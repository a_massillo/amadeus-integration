
package com.safarifone.waafitravels.amadeus.services.demo;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.amadeus.xml.fmptbr_14_3_1a.FareMasterPricerTravelBoardSearchReply.Recommendation;
import com.safarifone.waafitravels.amadeus.services.MPPBoardSearchClientService;
import com.waafi.amadeus.dto.PaxReference;
import com.waafi.amadeus.dto.Traveller;
import com.waafi.amadeus.enumerations.AmadeusEnumerations;
import com.waafi.amadeus.enumerations.AmadeusEnumerations.TYPE_OF_UNIT;

/**
 * Main class to perform request with soap header 4
 * @author amassillo
 *
 */
public final class MasterPricerTravelBoardSearchMainSH4 {

	private MasterPricerTravelBoardSearchMainSH4() {
	}

	public static void main(String args[]) throws java.lang.Exception {

		
		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

		//System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

		
		
		
		
		
		MPPBoardSearchClientService searchBoard = new MPPBoardSearchClientService();

		// Search for 3 seats
		BigInteger numberOfPassengerSeats = BigInteger.valueOf(3);

		// Setting passenger information for 3 adult travelers
		PaxReference paxReference = new PaxReference(AmadeusEnumerations.PNR_PassengerType.ADT);
		paxReference.addTravellerToList(new Traveller(BigInteger.valueOf(1)));
		paxReference.addTravellerToList(new Traveller(BigInteger.valueOf(2)));
		paxReference.addTravellerToList(new Traveller(BigInteger.valueOf(3)));

		List<PaxReference> paxReferences = new ArrayList<>();
		paxReferences.add(paxReference);

		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.MONTH, 10);

		java.util.List<Recommendation> result = searchBoard.searchSH4(numberOfPassengerSeats, paxReferences,
				TYPE_OF_UNIT.PX, "LON", "TYO", format.format(date.getTime()));

		// @TODO Parse response in a simplest way. Currently just printing the
		// object.

		for (Recommendation recomendation : result) {
			System.out.println("Print more info for item " +recomendation.getItemNumber().getItemNumberId().getNumber());
			// TODO: Process results
		}
		System.exit(0);
	}

}
