
package com.safarifone.waafitravels.amadeus.services;

import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.AddressingFeature;

import com.amadeus.utils.HeaderHandlerResolver;
import com.amadeus.utils.handlers.AddressingHandler;
import com.amadeus.utils.handlers.CustomSOAPHandler;
import com.amadeus.utils.handlers.SecurityHandler;
import com.amadeus.utils.handlers.SessionHandler;
import com.amadeus.utils.handlers.TransactionFlowLinkHandler;
import com.amadeus.utils.handlers.SessionHandler.TransactionStatusCode;
import com.amadeus.utils.handlers.TransactionFlowLinkHandler.TransactionFlowLinkAction;
import com.amadeus.xml.AmadeusWebServices;
import com.amadeus.xml.AmadeusWebServicesPT;
import com.amadeus.xml.SecurityAuthenticate_Client2;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser;
import com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser.UserID;
import com.amadeus.xml.fmptbr_14_3_1a.FareMasterPricerTravelBoardSearchReply;
import com.waafi.amadeus.builder.FareMasterPricerTravelBoardSearchRQBuilder;
import com.waafi.amadeus.builder.FareMasterPricerTravelBoardSearchRSBuilder;
import com.waafi.amadeus.dto.PaxReference;
import com.waafi.amadeus.enumerations.AmadeusEnumerations.FARE_TYPE;
import com.waafi.amadeus.enumerations.AmadeusEnumerations.PNR_PassengerType;
import com.waafi.amadeus.enumerations.AmadeusEnumerations.TYPE_OF_UNIT;


public class MPPBoardSearchClientService {

    private static final QName SERVICE_NAME = new QName("http://xml.amadeus.com", "AmadeusWebServices");
    
    public java.util.List<FareMasterPricerTravelBoardSearchReply.Recommendation> search(
    		BigInteger numberOfPassengerSeats,
    		List<PaxReference> paxReferences,
    		TYPE_OF_UNIT typeOfUnit,
    		String departureCity,
    		String arrivalCity,
    		String dateDeparture) {
        
    	URL wsdlURL = AmadeusWebServices.WSDL_LOCATION;      
        
        HeaderHandlerResolver headHandlerResolver = new HeaderHandlerResolver();
        AmadeusWebServices ss = new AmadeusWebServices(wsdlURL, SERVICE_NAME);
        ss.setHandlerResolver(headHandlerResolver);
        AmadeusWebServicesPT port = ss.getAmadeusWebServicesPort();  
                
        //TODO: IMPORTANT!!!! Remove sesisonId and securityToken from handler. Remove SecurityAuthenticate_Client2.getSession(port); too;
        SecurityAuthenticate_Client2.getSession(port);
        String sessionId=headHandlerResolver.getHeadHandler().sessionId;
        String securityToken=headHandlerResolver.getHeadHandler().securityToken;
        
        
        
        //Initializing FareMasterPriceTravelBoardSearch object.
        FareMasterPricerTravelBoardSearchRQBuilder fmpRqBuilder = new FareMasterPricerTravelBoardSearchRQBuilder();  
        //New unit information
        fmpRqBuilder.addUnit(numberOfPassengerSeats, typeOfUnit.name())
        //Adding Traveler Information
        .addTravelersInfo(paxReferences)
        //Itinerary Setup
        .addItemToItInerary(BigInteger.valueOf(1), departureCity, arrivalCity, dateDeparture)
        //Fare Options
        .addTicketingPriceType(FARE_TYPE.FARETYPE_PUBLISHED_FARES.getValue());

        
        //SESSION Not Needed in case of SoapHeader $ REMOVE IT OR REPLACE
        com.amadeus.xml._2010._06.session_v3.Session _fareMasterPricerTravelBoardSearch_session = new com.amadeus.xml._2010._06.session_v3.Session();
        _fareMasterPricerTravelBoardSearch_session.setSessionId(sessionId);
        _fareMasterPricerTravelBoardSearch_session.setSequenceNumber("1");
        _fareMasterPricerTravelBoardSearch_session.setSecurityToken(securityToken);
            
        AMASecurityHostedUser hostedSecurity = new AMASecurityHostedUser();

        FareMasterPricerTravelBoardSearchRSBuilder response = new FareMasterPricerTravelBoardSearchRSBuilder(); 
           
        port.fareMasterPricerTravelBoardSearch(
        		fmpRqBuilder.getNumberOfUnit(),
        		fmpRqBuilder.getPaxReferenceList(), 
        		fmpRqBuilder.getFareOptions(),
        		fmpRqBuilder.getItinerary(), 
        		_fareMasterPricerTravelBoardSearch_session, 
        		hostedSecurity,
        		response.replyStatus, 
        		response.errorMessage, 
        		response.conversionRate, 
        		response.solutionFamily,
        		response.familyInformation, 
        		response.amountInfoForAllPax, 
        		response.amountInfoPerPax, 
        		response.feeDetails, 
        		response.companyIdText, 
        		response.officeIdDetails, 
        		response.flightIndex, 
        		response.recommendation, 
        		response.otherSolutions, 
        		response.warningInfo, 
        		response.globalInformation, 
        		response.serviceFeesGrp, 
        		response.value, 
        		response.mnrGrp, 
        		response.session, 
        		response.link);

        
        return response.recommendation.value;
    }
    
    
    //Service search FareMasterPricerTravelBoardSearchRQBuilder using soap header4
    public java.util.List<FareMasterPricerTravelBoardSearchReply.Recommendation> searchSH4(
    		BigInteger numberOfPassengerSeats,
    		List<PaxReference> paxReferences,
    		TYPE_OF_UNIT typeOfUnit,
    		String departureCity,
    		String arrivalCity,
    		String dateDeparture) {
       
    	AmadeusWebServicesPT mPort;
    	SessionHandler mSession;
    	TransactionFlowLinkHandler mLink;
    	SecurityHandler mSecurity;
    	AddressingHandler mAddressing;
    	CustomSOAPHandler mSoapHandler = new CustomSOAPHandler();
    	
		AmadeusWebServices proxy = new AmadeusWebServices();
		mPort = proxy.getAmadeusWebServicesPort(new AddressingFeature());
		mSecurity = new SecurityHandler((BindingProvider) mPort, mSoapHandler);
		mSession = new SessionHandler((BindingProvider) mPort, mSecurity);
		mLink = new TransactionFlowLinkHandler((BindingProvider) mPort);
		mAddressing = new AddressingHandler((BindingProvider) mPort, "1ASIWWFIWAF");
		
		mSession.handleSessionStatus(TransactionStatusCode.START);
		mLink.handleLinkAction(TransactionFlowLinkAction.NONE);
		mAddressing.update();
		
		
		
		// Add a handler to manage security header and remove optional SOAP headers
		@SuppressWarnings("rawtypes")
		List<Handler> chain = new ArrayList<Handler>();
		chain.add(mSoapHandler);
		((BindingProvider) mPort).getBinding().setHandlerChain(chain);
    	
		
        
        
        //Initializing FareMasterPriceTravelBoardSearch object.
        FareMasterPricerTravelBoardSearchRQBuilder fmpRqBuilder = new FareMasterPricerTravelBoardSearchRQBuilder();  
        //New unit information
        fmpRqBuilder.addUnit(numberOfPassengerSeats, typeOfUnit.name())
        //Adding Traveler Information
        .addTravelersInfo(paxReferences)
        //Itinerary Setup
        .addItemToItInerary(BigInteger.valueOf(1), departureCity, arrivalCity, dateDeparture)
        //Fare Options
        .addTicketingPriceType(FARE_TYPE.FARETYPE_PUBLISHED_FARES.getValue());

        
        //SESSION Not Needed in case of SoapHeader $ REMOVE IT OR REPLACE
        com.amadeus.xml._2010._06.session_v3.Session _fareMasterPricerTravelBoardSearch_session = new com.amadeus.xml._2010._06.session_v3.Session();
        _fareMasterPricerTravelBoardSearch_session.setSessionId("");
        _fareMasterPricerTravelBoardSearch_session.setSequenceNumber("");
        _fareMasterPricerTravelBoardSearch_session.setSecurityToken("");
        _fareMasterPricerTravelBoardSearch_session.setTransactionStatusCode("Start");
        
        AMASecurityHostedUser hostedSecurity = new AMASecurityHostedUser();
        UserID userId = new UserID();
        userId.setAgentDutyCode("SU");
        userId.setPseudoCityCode("LONU128WH");
        userId.setRequestorType("U");
        userId.setPOSType("1");
        hostedSecurity.setUserID(userId);

        FareMasterPricerTravelBoardSearchRSBuilder response = new FareMasterPricerTravelBoardSearchRSBuilder(); 
           
        
        try {
        mPort.fareMasterPricerTravelBoardSearch(
        		fmpRqBuilder.getNumberOfUnit(),
        		fmpRqBuilder.getPaxReferenceList(), 
        		fmpRqBuilder.getFareOptions(),
        		fmpRqBuilder.getItinerary(), 
        		_fareMasterPricerTravelBoardSearch_session, 
        		hostedSecurity,
        		response.replyStatus, 
        		response.errorMessage, 
        		response.conversionRate, 
        		response.solutionFamily,
        		response.familyInformation, 
        		response.amountInfoForAllPax, 
        		response.amountInfoPerPax, 
        		response.feeDetails, 
        		response.companyIdText, 
        		response.officeIdDetails, 
        		response.flightIndex, 
        		response.recommendation, 
        		response.otherSolutions, 
        		response.warningInfo, 
        		response.globalInformation, 
        		response.serviceFeesGrp, 
        		response.value, 
        		response.mnrGrp, 
        		response.session, 
        		response.link);
        }catch (Exception e) {
        	System.out.println();
        }
        
        return response.recommendation.value;
    }

    

}
