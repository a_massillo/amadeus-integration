
package com.amadeus.xml.itares_05_2_ia;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Identification of facilities for a product or service by type or description.
 * 
 * <p>Java class for ProductFacilitiesTypeI complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductFacilitiesTypeI"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="code" type="{http://xml.amadeus.com/ITARES_05_2_IA}AlphaNumericString_Length1To3" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://xml.amadeus.com/ITARES_05_2_IA}AlphaNumericString_Length1To70" minOccurs="0"/&gt;
 *         &lt;element name="qualifier" type="{http://xml.amadeus.com/ITARES_05_2_IA}AlphaNumericString_Length1To3" minOccurs="0"/&gt;
 *         &lt;element name="extensionCode" type="{http://xml.amadeus.com/ITARES_05_2_IA}AlphaNumericString_Length1To17" maxOccurs="26" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductFacilitiesTypeI", propOrder = {
    "code",
    "description",
    "qualifier",
    "extensionCode"
})
public class ProductFacilitiesTypeI {

    protected String code;
    protected String description;
    protected String qualifier;
    protected List<String> extensionCode;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the qualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifier() {
        return qualifier;
    }

    /**
     * Sets the value of the qualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifier(String value) {
        this.qualifier = value;
    }

    /**
     * Gets the value of the extensionCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extensionCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtensionCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExtensionCode() {
        if (extensionCode == null) {
            extensionCode = new ArrayList<String>();
        }
        return this.extensionCode;
    }

}
