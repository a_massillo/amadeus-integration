
package com.amadeus.xml.trcanq_11_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.trcanq_11_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.trcanq_11_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TicketCancelDocument }
     * 
     */
    public TicketCancelDocument createTicketCancelDocument() {
        return new TicketCancelDocument();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

    /**
     * Create an instance of {@link ItemNumberTypeI }
     * 
     */
    public ItemNumberTypeI createItemNumberTypeI() {
        return new ItemNumberTypeI();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link OfficeSettingsDetailsType }
     * 
     */
    public OfficeSettingsDetailsType createOfficeSettingsDetailsType() {
        return new OfficeSettingsDetailsType();
    }

    /**
     * Create an instance of {@link AdditionalBusinessSourceInformationType }
     * 
     */
    public AdditionalBusinessSourceInformationType createAdditionalBusinessSourceInformationType() {
        return new AdditionalBusinessSourceInformationType();
    }

    /**
     * Create an instance of {@link DocumentInfoFromOfficeSettingType }
     * 
     */
    public DocumentInfoFromOfficeSettingType createDocumentInfoFromOfficeSettingType() {
        return new DocumentInfoFromOfficeSettingType();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationTypeI }
     * 
     */
    public ItemNumberIdentificationTypeI createItemNumberIdentificationTypeI() {
        return new ItemNumberIdentificationTypeI();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsType }
     * 
     */
    public OriginatorIdentificationDetailsType createOriginatorIdentificationDetailsType() {
        return new OriginatorIdentificationDetailsType();
    }

    /**
     * Create an instance of {@link StatusDetailsType }
     * 
     */
    public StatusDetailsType createStatusDetailsType() {
        return new StatusDetailsType();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

}
