
package com.amadeus.xml.itareq_05_2_ia;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.itareq_05_2_ia package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.itareq_05_2_ia
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AirSellFromRecommendation }
     * 
     */
    public AirSellFromRecommendation createAirSellFromRecommendation() {
        return new AirSellFromRecommendation();
    }

    /**
     * Create an instance of {@link AirSellFromRecommendation.ItineraryDetails }
     * 
     */
    public AirSellFromRecommendation.ItineraryDetails createAirSellFromRecommendationItineraryDetails() {
        return new AirSellFromRecommendation.ItineraryDetails();
    }

    /**
     * Create an instance of {@link MessageActionDetailsTypeI }
     * 
     */
    public MessageActionDetailsTypeI createMessageActionDetailsTypeI() {
        return new MessageActionDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReservationControlInformationTypeI }
     * 
     */
    public ReservationControlInformationTypeI createReservationControlInformationTypeI() {
        return new ReservationControlInformationTypeI();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link LocationTypeI }
     * 
     */
    public LocationTypeI createLocationTypeI() {
        return new LocationTypeI();
    }

    /**
     * Create an instance of {@link MarriageControlDetailsTypeI }
     * 
     */
    public MarriageControlDetailsTypeI createMarriageControlDetailsTypeI() {
        return new MarriageControlDetailsTypeI();
    }

    /**
     * Create an instance of {@link MessageFunctionBusinessDetailsTypeI }
     * 
     */
    public MessageFunctionBusinessDetailsTypeI createMessageFunctionBusinessDetailsTypeI() {
        return new MessageFunctionBusinessDetailsTypeI();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI }
     * 
     */
    public OriginAndDestinationDetailsTypeI createOriginAndDestinationDetailsTypeI() {
        return new OriginAndDestinationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductDateTimeTypeI }
     * 
     */
    public ProductDateTimeTypeI createProductDateTimeTypeI() {
        return new ProductDateTimeTypeI();
    }

    /**
     * Create an instance of {@link ProductIdentificationDetailsTypeI }
     * 
     */
    public ProductIdentificationDetailsTypeI createProductIdentificationDetailsTypeI() {
        return new ProductIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsTypeI }
     * 
     */
    public ProductTypeDetailsTypeI createProductTypeDetailsTypeI() {
        return new ProductTypeDetailsTypeI();
    }

    /**
     * Create an instance of {@link RelatedProductInformationTypeI }
     * 
     */
    public RelatedProductInformationTypeI createRelatedProductInformationTypeI() {
        return new RelatedProductInformationTypeI();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsTypeI }
     * 
     */
    public ReservationControlInformationDetailsTypeI createReservationControlInformationDetailsTypeI() {
        return new ReservationControlInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link TravelProductInformationTypeI }
     * 
     */
    public TravelProductInformationTypeI createTravelProductInformationTypeI() {
        return new TravelProductInformationTypeI();
    }

    /**
     * Create an instance of {@link AirSellFromRecommendation.ItineraryDetails.SegmentInformation }
     * 
     */
    public AirSellFromRecommendation.ItineraryDetails.SegmentInformation createAirSellFromRecommendationItineraryDetailsSegmentInformation() {
        return new AirSellFromRecommendation.ItineraryDetails.SegmentInformation();
    }

}
