
package com.amadeus.xml;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.1.7
 * 2018-03-17T14:21:19.432+03:00
 * Generated source version: 3.1.7
 * 
 */
public final class DocIssuanceIssueCombined_Client {

    private static final QName SERVICE_NAME = new QName("http://xml.amadeus.com", "AmadeusWebServices");

    private DocIssuanceIssueCombined_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = AmadeusWebServices.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        AmadeusWebServices ss = new AmadeusWebServices(wsdlURL, SERVICE_NAME);
        AmadeusWebServicesPT port = ss.getAmadeusWebServicesPort();  
        
        {
        System.out.println("Invoking docIssuanceIssueCombined...");
        com.amadeus.xml.tctmiq_15_1_1a.TicketAgentInfoTypeI _docIssuanceIssueCombined_agentInfo = new com.amadeus.xml.tctmiq_15_1_1a.TicketAgentInfoTypeI();
        com.amadeus.xml.tctmiq_15_1_1a.InternalIDDetailsTypeI _docIssuanceIssueCombined_agentInfoInternalIdDetails = new com.amadeus.xml.tctmiq_15_1_1a.InternalIDDetailsTypeI();
        _docIssuanceIssueCombined_agentInfoInternalIdDetails.setInhouseId("InhouseId-694297900");
        _docIssuanceIssueCombined_agentInfoInternalIdDetails.setType("Type237193894");
        _docIssuanceIssueCombined_agentInfo.setInternalIdDetails(_docIssuanceIssueCombined_agentInfoInternalIdDetails);
        com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeInformationType _docIssuanceIssueCombined_overrideDate = new com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeInformationType();
        _docIssuanceIssueCombined_overrideDate.setBusinessSemantic("BusinessSemantic-1125285697");
        com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeType _docIssuanceIssueCombined_overrideDateDateTime = new com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeType();
        _docIssuanceIssueCombined_overrideDateDateTime.setYear("Year-1538726601");
        _docIssuanceIssueCombined_overrideDateDateTime.setMonth("Month-77032296");
        _docIssuanceIssueCombined_overrideDateDateTime.setDay("Day-1556794691");
        _docIssuanceIssueCombined_overrideDate.setDateTime(_docIssuanceIssueCombined_overrideDateDateTime);
        java.util.List<com.amadeus.xml.tctmiq_15_1_1a.ReferenceInfoType> _docIssuanceIssueCombined_selection = new java.util.ArrayList<com.amadeus.xml.tctmiq_15_1_1a.ReferenceInfoType>();
        com.amadeus.xml.tctmiq_15_1_1a.ReferenceInfoType _docIssuanceIssueCombined_selectionVal1 = new com.amadeus.xml.tctmiq_15_1_1a.ReferenceInfoType();
        java.util.List<com.amadeus.xml.tctmiq_15_1_1a.ReferencingDetailsType270188C> _docIssuanceIssueCombined_selectionVal1ReferenceDetails = new java.util.ArrayList<com.amadeus.xml.tctmiq_15_1_1a.ReferencingDetailsType270188C>();
        com.amadeus.xml.tctmiq_15_1_1a.ReferencingDetailsType270188C _docIssuanceIssueCombined_selectionVal1ReferenceDetailsVal1 = new com.amadeus.xml.tctmiq_15_1_1a.ReferencingDetailsType270188C();
        _docIssuanceIssueCombined_selectionVal1ReferenceDetailsVal1.setType("Type145761267");
        _docIssuanceIssueCombined_selectionVal1ReferenceDetailsVal1.setValue("Value2096904212");
        _docIssuanceIssueCombined_selectionVal1ReferenceDetails.add(_docIssuanceIssueCombined_selectionVal1ReferenceDetailsVal1);
        _docIssuanceIssueCombined_selectionVal1.getReferenceDetails().addAll(_docIssuanceIssueCombined_selectionVal1ReferenceDetails);
        _docIssuanceIssueCombined_selection.add(_docIssuanceIssueCombined_selectionVal1);
        java.util.List<com.amadeus.xml.tctmiq_15_1_1a.ReferenceInformationType> _docIssuanceIssueCombined_paxSelection = new java.util.ArrayList<com.amadeus.xml.tctmiq_15_1_1a.ReferenceInformationType>();
        com.amadeus.xml.tctmiq_15_1_1a.ReferenceInformationType _docIssuanceIssueCombined_paxSelectionVal1 = new com.amadeus.xml.tctmiq_15_1_1a.ReferenceInformationType();
        com.amadeus.xml.tctmiq_15_1_1a.ReferencingDetailsType _docIssuanceIssueCombined_paxSelectionVal1PassengerReference = new com.amadeus.xml.tctmiq_15_1_1a.ReferencingDetailsType();
        _docIssuanceIssueCombined_paxSelectionVal1PassengerReference.setType("Type-1703233233");
        _docIssuanceIssueCombined_paxSelectionVal1PassengerReference.setValue("Value64557200");
        _docIssuanceIssueCombined_paxSelectionVal1.setPassengerReference(_docIssuanceIssueCombined_paxSelectionVal1PassengerReference);
        _docIssuanceIssueCombined_paxSelection.add(_docIssuanceIssueCombined_paxSelectionVal1);
        com.amadeus.xml.tctmiq_15_1_1a.StockInformationType _docIssuanceIssueCombined_stock = new com.amadeus.xml.tctmiq_15_1_1a.StockInformationType();
        com.amadeus.xml.tctmiq_15_1_1a.StockTicketNumberDetailsType _docIssuanceIssueCombined_stockStockTicketNumberDetails = new com.amadeus.xml.tctmiq_15_1_1a.StockTicketNumberDetailsType();
        _docIssuanceIssueCombined_stockStockTicketNumberDetails.setQualifier("Qualifier-1541242047");
        _docIssuanceIssueCombined_stockStockTicketNumberDetails.setControlNumber("ControlNumber1000149336");
        _docIssuanceIssueCombined_stock.setStockTicketNumberDetails(_docIssuanceIssueCombined_stockStockTicketNumberDetails);
        java.util.List<com.amadeus.xml.tctmiq_15_1_1a.DocIssuanceIssueCombined.OptionGroup> _docIssuanceIssueCombined_optionGroup = new java.util.ArrayList<com.amadeus.xml.tctmiq_15_1_1a.DocIssuanceIssueCombined.OptionGroup>();
        com.amadeus.xml.tctmiq_15_1_1a.DocIssuanceIssueCombined.OptionGroup _docIssuanceIssueCombined_optionGroupVal1 = new com.amadeus.xml.tctmiq_15_1_1a.DocIssuanceIssueCombined.OptionGroup();
        com.amadeus.xml.tctmiq_15_1_1a.StatusTypeI _docIssuanceIssueCombined_optionGroupVal1Switches = new com.amadeus.xml.tctmiq_15_1_1a.StatusTypeI();
        com.amadeus.xml.tctmiq_15_1_1a.StatusDetailsTypeI _docIssuanceIssueCombined_optionGroupVal1SwitchesStatusDetails = new com.amadeus.xml.tctmiq_15_1_1a.StatusDetailsTypeI();
        _docIssuanceIssueCombined_optionGroupVal1SwitchesStatusDetails.setIndicator("Indicator-1569881488");
        _docIssuanceIssueCombined_optionGroupVal1Switches.setStatusDetails(_docIssuanceIssueCombined_optionGroupVal1SwitchesStatusDetails);
        _docIssuanceIssueCombined_optionGroupVal1.setSwitches(_docIssuanceIssueCombined_optionGroupVal1Switches);
        java.util.List<com.amadeus.xml.tctmiq_15_1_1a.AttributeType> _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptions = new java.util.ArrayList<com.amadeus.xml.tctmiq_15_1_1a.AttributeType>();
        com.amadeus.xml.tctmiq_15_1_1a.AttributeType _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1 = new com.amadeus.xml.tctmiq_15_1_1a.AttributeType();
        com.amadeus.xml.tctmiq_15_1_1a.AttributeInformationTypeU _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1CriteriaDetails = new com.amadeus.xml.tctmiq_15_1_1a.AttributeInformationTypeU();
        _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1CriteriaDetails.setAttributeType("AttributeType891496222");
        _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1CriteriaDetails.setAttributeDescription("AttributeDescription-130855021");
        _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1.setCriteriaDetails(_docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1CriteriaDetails);
        _docIssuanceIssueCombined_optionGroupVal1SubCompoundOptions.add(_docIssuanceIssueCombined_optionGroupVal1SubCompoundOptionsVal1);
        _docIssuanceIssueCombined_optionGroupVal1.getSubCompoundOptions().addAll(_docIssuanceIssueCombined_optionGroupVal1SubCompoundOptions);
        com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeInformationType _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDate = new com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeInformationType();
        _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDate.setBusinessSemantic("BusinessSemantic36063498");
        com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeType _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDateDateTime = new com.amadeus.xml.tctmiq_15_1_1a.StructuredDateTimeType();
        _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDateDateTime.setYear("Year-1328536948");
        _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDateDateTime.setMonth("Month-1846552055");
        _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDateDateTime.setDay("Day1176469020");
        _docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDate.setDateTime(_docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDateDateTime);
        _docIssuanceIssueCombined_optionGroupVal1.setOverrideAlternativeDate(_docIssuanceIssueCombined_optionGroupVal1OverrideAlternativeDate);
        _docIssuanceIssueCombined_optionGroup.add(_docIssuanceIssueCombined_optionGroupVal1);
        com.amadeus.xml.tctmiq_15_1_1a.TravellerInformationType _docIssuanceIssueCombined_infantOrAdultAssociation = new com.amadeus.xml.tctmiq_15_1_1a.TravellerInformationType();
        com.amadeus.xml.tctmiq_15_1_1a.TravellerSurnameInformationType _docIssuanceIssueCombined_infantOrAdultAssociationPaxDetails = new com.amadeus.xml.tctmiq_15_1_1a.TravellerSurnameInformationType();
        _docIssuanceIssueCombined_infantOrAdultAssociationPaxDetails.setType("Type373725665");
        _docIssuanceIssueCombined_infantOrAdultAssociation.setPaxDetails(_docIssuanceIssueCombined_infantOrAdultAssociationPaxDetails);
        java.util.List<com.amadeus.xml.tctmiq_15_1_1a.CodedAttributeType> _docIssuanceIssueCombined_otherCompoundOptions = new java.util.ArrayList<com.amadeus.xml.tctmiq_15_1_1a.CodedAttributeType>();
        com.amadeus.xml.tctmiq_15_1_1a.CodedAttributeType _docIssuanceIssueCombined_otherCompoundOptionsVal1 = new com.amadeus.xml.tctmiq_15_1_1a.CodedAttributeType();
        com.amadeus.xml.tctmiq_15_1_1a.CodedAttributeInformationType _docIssuanceIssueCombined_otherCompoundOptionsVal1AttributeDetails = new com.amadeus.xml.tctmiq_15_1_1a.CodedAttributeInformationType();
        _docIssuanceIssueCombined_otherCompoundOptionsVal1AttributeDetails.setAttributeType("AttributeType1181657662");
        _docIssuanceIssueCombined_otherCompoundOptionsVal1AttributeDetails.setAttributeDescription("AttributeDescription265710013");
        _docIssuanceIssueCombined_otherCompoundOptionsVal1.setAttributeDetails(_docIssuanceIssueCombined_otherCompoundOptionsVal1AttributeDetails);
        _docIssuanceIssueCombined_otherCompoundOptions.add(_docIssuanceIssueCombined_otherCompoundOptionsVal1);
        com.amadeus.xml._2010._06.session_v3.Session _docIssuanceIssueCombined_session = new com.amadeus.xml._2010._06.session_v3.Session();
        _docIssuanceIssueCombined_session.setSessionId("SessionId-1056654420");
        _docIssuanceIssueCombined_session.setSequenceNumber("SequenceNumber-2117520375");
        _docIssuanceIssueCombined_session.setSecurityToken("SecurityToken1281662528");
        _docIssuanceIssueCombined_session.setTransactionStatusCode("TransactionStatusCode-112669225");
        com.amadeus.wsdl._2010._06.ws.link_v1.TransactionFlowLinkType _docIssuanceIssueCombined_link = new com.amadeus.wsdl._2010._06.ws.link_v1.TransactionFlowLinkType();
        com.amadeus.wsdl._2010._06.ws.link_v1.ConsumerType _docIssuanceIssueCombined_linkConsumer = new com.amadeus.wsdl._2010._06.ws.link_v1.ConsumerType();
        _docIssuanceIssueCombined_linkConsumer.setUniqueID("UniqueID2010595653");
        _docIssuanceIssueCombined_link.setConsumer(_docIssuanceIssueCombined_linkConsumer);
        com.amadeus.wsdl._2010._06.ws.link_v1.ReceiverType _docIssuanceIssueCombined_linkReceiver = new com.amadeus.wsdl._2010._06.ws.link_v1.ReceiverType();
        _docIssuanceIssueCombined_linkReceiver.setServerID("ServerID-575784922");
        _docIssuanceIssueCombined_link.setReceiver(_docIssuanceIssueCombined_linkReceiver);
        com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser _docIssuanceIssueCombined_security = new com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser();
        com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser.UserID _docIssuanceIssueCombined_securityUserID = new com.amadeus.xml._2010._06.security_v1.AMASecurityHostedUser.UserID();
        org.iata.iata._2007._00.iata2010.UniqueIDType _docIssuanceIssueCombined_securityUserIDRequestorID = new org.iata.iata._2007._00.iata2010.UniqueIDType();
        org.iata.iata._2007._00.iata2010.CompanyNameType _docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName = new org.iata.iata._2007._00.iata2010.CompanyNameType();
        _docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName.setValue("Value1807675392");
        _docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName.setCompanyShortName("CompanyShortName552358339");
        _docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName.setTravelSector("TravelSector1846009327");
        _docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName.setCode("Code-450797927");
        _docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName.setCodeContext("CodeContext1702382995");
        _docIssuanceIssueCombined_securityUserIDRequestorID.setCompanyName(_docIssuanceIssueCombined_securityUserIDRequestorIDCompanyName);
        _docIssuanceIssueCombined_securityUserIDRequestorID.setURL("URL1422710695");
        _docIssuanceIssueCombined_securityUserIDRequestorID.setType("Type413157832");
        _docIssuanceIssueCombined_securityUserIDRequestorID.setInstance("Instance463294209");
        _docIssuanceIssueCombined_securityUserIDRequestorID.setIDContext("IDContext-1244267287");
        _docIssuanceIssueCombined_securityUserIDRequestorID.setID("ID-2050562689");
        _docIssuanceIssueCombined_securityUserID.setRequestorID(_docIssuanceIssueCombined_securityUserIDRequestorID);
        com.amadeus.xml._2010._06.types_v1.PointOfSaleType.BookingChannel _docIssuanceIssueCombined_securityUserIDBookingChannel = new com.amadeus.xml._2010._06.types_v1.PointOfSaleType.BookingChannel();
        org.opentravel.ota._2003._05.ota2010b.CompanyNameType _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName = new org.opentravel.ota._2003._05.ota2010b.CompanyNameType();
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setValue("Value-1016523170");
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setDivision("Division1500408836");
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setDepartment("Department-1143294398");
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setCompanyShortName("CompanyShortName-1096621944");
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setTravelSector("TravelSector-1223831487");
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setCode("Code-877652146");
        _docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName.setCodeContext("CodeContext1860710881");
        _docIssuanceIssueCombined_securityUserIDBookingChannel.setCompanyName(_docIssuanceIssueCombined_securityUserIDBookingChannelCompanyName);
        _docIssuanceIssueCombined_securityUserIDBookingChannel.setType("Type1495145186");
        _docIssuanceIssueCombined_securityUserID.setBookingChannel(_docIssuanceIssueCombined_securityUserIDBookingChannel);
        _docIssuanceIssueCombined_securityUserID.setPOSType("POSType-1150736942");
        _docIssuanceIssueCombined_securityUserID.setPseudoCityCode("PseudoCityCode2097918425");
        _docIssuanceIssueCombined_securityUserID.setAgentSign("AgentSign1761565950");
        _docIssuanceIssueCombined_securityUserID.setAgentDutyCode("AgentDutyCode1942263664");
        _docIssuanceIssueCombined_securityUserID.setERSPUserID("ERSPUserID963389281");
        _docIssuanceIssueCombined_securityUserID.setFirstDepartPoint("FirstDepartPoint-214901649");
        _docIssuanceIssueCombined_securityUserID.setTrueCityCode("TrueCityCode-291870208");
        _docIssuanceIssueCombined_securityUserID.setISOCountry("ISOCountry1011394562");
        _docIssuanceIssueCombined_securityUserID.setISOCurrency("ISOCurrency-1682901897");
        _docIssuanceIssueCombined_securityUserID.setLanguage("Language-1233994644");
        _docIssuanceIssueCombined_securityUserID.setDate("Date1427845099");
        _docIssuanceIssueCombined_securityUserID.setTime("Time1295239606");
        org.iata.iata._2007._00.iata2010.TimeType _docIssuanceIssueCombined_securityUserIDTimeZone = org.iata.iata._2007._00.iata2010.TimeType.LOCAL;
        _docIssuanceIssueCombined_securityUserID.setTimeZone(_docIssuanceIssueCombined_securityUserIDTimeZone);
        _docIssuanceIssueCombined_securityUserID.setRequestorType("RequestorType-1653716792");
        _docIssuanceIssueCombined_security.setUserID(_docIssuanceIssueCombined_securityUserID);
        com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType _docIssuanceIssueCombined_securityFullLocation = new com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType();
        com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType.Location _docIssuanceIssueCombined_securityFullLocationLocation = new com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType.Location();
        com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType.Location.Airport _docIssuanceIssueCombined_securityFullLocationLocationAirport = new com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType.Location.Airport();
        _docIssuanceIssueCombined_securityFullLocationLocationAirport.setBuildingCode("BuildingCode427527447");
        _docIssuanceIssueCombined_securityFullLocationLocationAirport.setLocationCode("LocationCode-442604646");
        _docIssuanceIssueCombined_securityFullLocationLocationAirport.setCodeContext("CodeContext-303585205");
        _docIssuanceIssueCombined_securityFullLocationLocationAirport.setTerminal("Terminal-1571167403");
        _docIssuanceIssueCombined_securityFullLocationLocationAirport.setGate("Gate2039791906");
        _docIssuanceIssueCombined_securityFullLocationLocation.setAirport(_docIssuanceIssueCombined_securityFullLocationLocationAirport);
        com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType.Location.City _docIssuanceIssueCombined_securityFullLocationLocationCity = new com.amadeus.xml._2010._06.appmdw_commontypes_v3.LocationType.Location.City();
        _docIssuanceIssueCombined_securityFullLocationLocationCity.setBuildingCode("BuildingCode-867021930");
        _docIssuanceIssueCombined_securityFullLocationLocationCity.setLocationCode("LocationCode1748728772");
        _docIssuanceIssueCombined_securityFullLocationLocationCity.setCodeContext("CodeContext-1965112764");
        _docIssuanceIssueCombined_securityFullLocationLocation.setCity(_docIssuanceIssueCombined_securityFullLocationLocationCity);
        _docIssuanceIssueCombined_securityFullLocation.setLocation(_docIssuanceIssueCombined_securityFullLocationLocation);
        com.amadeus.xml._2010._06.appmdw_commontypes_v3.CategoryType _docIssuanceIssueCombined_securityFullLocationLocationCategory = new com.amadeus.xml._2010._06.appmdw_commontypes_v3.CategoryType();
        _docIssuanceIssueCombined_securityFullLocationLocationCategory.setIndexNumber("IndexNumber-30543321");
        _docIssuanceIssueCombined_securityFullLocationLocationCategory.setCode("Code127284617");
        _docIssuanceIssueCombined_securityFullLocationLocationCategory.setOwner("Owner-2088432734");
        _docIssuanceIssueCombined_securityFullLocationLocationCategory.setListName("ListName979430893");
        _docIssuanceIssueCombined_securityFullLocationLocationCategory.setListCode("ListCode441536815");
        _docIssuanceIssueCombined_securityFullLocationLocationCategory.setDesc("Desc1713270961");
        _docIssuanceIssueCombined_securityFullLocation.setLocationCategory(_docIssuanceIssueCombined_securityFullLocationLocationCategory);
        _docIssuanceIssueCombined_security.setFullLocation(_docIssuanceIssueCombined_securityFullLocation);
        _docIssuanceIssueCombined_security.setWorkstationID("WorkstationID1576555342");
        com.amadeus.xml._2010._06.appmdw_commontypes_v3.ApplicationType _docIssuanceIssueCombined_securityApplication = new com.amadeus.xml._2010._06.appmdw_commontypes_v3.ApplicationType();
        _docIssuanceIssueCombined_securityApplication.setApplicationLabel("ApplicationLabel-1717381426");
        _docIssuanceIssueCombined_securityApplication.setIndexNumber("IndexNumber-2006281753");
        _docIssuanceIssueCombined_security.setApplication(_docIssuanceIssueCombined_securityApplication);
        javax.xml.ws.Holder<com.amadeus.xml.tctmir_15_1_1a.ResponseAnalysisDetailsType> _docIssuanceIssueCombined_processingStatus = new javax.xml.ws.Holder<com.amadeus.xml.tctmir_15_1_1a.ResponseAnalysisDetailsType>();
        javax.xml.ws.Holder<com.amadeus.xml.tctmir_15_1_1a.ErrorGroupType> _docIssuanceIssueCombined_errorGroup = new javax.xml.ws.Holder<com.amadeus.xml.tctmir_15_1_1a.ErrorGroupType>();
        javax.xml.ws.Holder<com.amadeus.xml._2010._06.session_v3.Session> _docIssuanceIssueCombined_session1 = new javax.xml.ws.Holder<com.amadeus.xml._2010._06.session_v3.Session>();
        javax.xml.ws.Holder<com.amadeus.wsdl._2010._06.ws.link_v1.TransactionFlowLinkType> _docIssuanceIssueCombined_link1 = new javax.xml.ws.Holder<com.amadeus.wsdl._2010._06.ws.link_v1.TransactionFlowLinkType>();
        port.docIssuanceIssueCombined(_docIssuanceIssueCombined_agentInfo, _docIssuanceIssueCombined_overrideDate, _docIssuanceIssueCombined_selection, _docIssuanceIssueCombined_paxSelection, _docIssuanceIssueCombined_stock, _docIssuanceIssueCombined_optionGroup, _docIssuanceIssueCombined_infantOrAdultAssociation, _docIssuanceIssueCombined_otherCompoundOptions, _docIssuanceIssueCombined_session, _docIssuanceIssueCombined_link, _docIssuanceIssueCombined_security, _docIssuanceIssueCombined_processingStatus, _docIssuanceIssueCombined_errorGroup, _docIssuanceIssueCombined_session1, _docIssuanceIssueCombined_link1);

        System.out.println("docIssuanceIssueCombined._docIssuanceIssueCombined_processingStatus=" + _docIssuanceIssueCombined_processingStatus.value);
        System.out.println("docIssuanceIssueCombined._docIssuanceIssueCombined_errorGroup=" + _docIssuanceIssueCombined_errorGroup.value);
        System.out.println("docIssuanceIssueCombined._docIssuanceIssueCombined_session1=" + _docIssuanceIssueCombined_session1.value);
        System.out.println("docIssuanceIssueCombined._docIssuanceIssueCombined_link1=" + _docIssuanceIssueCombined_link1.value);

        }
   System.exit(0);
    }

}
