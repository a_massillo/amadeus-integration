
package com.amadeus.xml.quqpcr_03_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.quqpcr_03_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.quqpcr_03_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueuePlacePNRReply }
     * 
     */
    public QueuePlacePNRReply createQueuePlacePNRReply() {
        return new QueuePlacePNRReply();
    }

    /**
     * Create an instance of {@link QueuePlacePNRReply.ErrorReturn }
     * 
     */
    public QueuePlacePNRReply.ErrorReturn createQueuePlacePNRReplyErrorReturn() {
        return new QueuePlacePNRReply.ErrorReturn();
    }

    /**
     * Create an instance of {@link ReservationControlInformationTypeI }
     * 
     */
    public ReservationControlInformationTypeI createReservationControlInformationTypeI() {
        return new ReservationControlInformationTypeI();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailTypeI }
     * 
     */
    public ApplicationErrorDetailTypeI createApplicationErrorDetailTypeI() {
        return new ApplicationErrorDetailTypeI();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationTypeI }
     * 
     */
    public ApplicationErrorInformationTypeI createApplicationErrorInformationTypeI() {
        return new ApplicationErrorInformationTypeI();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsTypeI }
     * 
     */
    public ReservationControlInformationDetailsTypeI createReservationControlInformationDetailsTypeI() {
        return new ReservationControlInformationDetailsTypeI();
    }

}
