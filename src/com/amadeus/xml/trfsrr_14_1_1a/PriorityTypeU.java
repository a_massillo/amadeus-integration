
package com.amadeus.xml.trfsrr_14_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * The segment is used to communicate priority information.
 * 
 * <p>Java class for PriorityTypeU complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriorityTypeU"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="priorityDetails" type="{http://xml.amadeus.com/TRFSRR_14_1_1A}PriorityDetailsTypeU" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriorityTypeU", propOrder = {
    "priorityDetails"
})
public class PriorityTypeU {

    protected PriorityDetailsTypeU priorityDetails;

    /**
     * Gets the value of the priorityDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PriorityDetailsTypeU }
     *     
     */
    public PriorityDetailsTypeU getPriorityDetails() {
        return priorityDetails;
    }

    /**
     * Sets the value of the priorityDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorityDetailsTypeU }
     *     
     */
    public void setPriorityDetails(PriorityDetailsTypeU value) {
        this.priorityDetails = value;
    }

}
