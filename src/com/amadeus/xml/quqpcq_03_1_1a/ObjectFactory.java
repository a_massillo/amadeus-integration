
package com.amadeus.xml.quqpcq_03_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.quqpcq_03_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.quqpcq_03_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueuePlacePNR }
     * 
     */
    public QueuePlacePNR createQueuePlacePNR() {
        return new QueuePlacePNR();
    }

    /**
     * Create an instance of {@link SelectionDetailsTypeI }
     * 
     */
    public SelectionDetailsTypeI createSelectionDetailsTypeI() {
        return new SelectionDetailsTypeI();
    }

    /**
     * Create an instance of {@link QueuePlacePNR.TargetDetails }
     * 
     */
    public QueuePlacePNR.TargetDetails createQueuePlacePNRTargetDetails() {
        return new QueuePlacePNR.TargetDetails();
    }

    /**
     * Create an instance of {@link ReservationControlInformationTypeI }
     * 
     */
    public ReservationControlInformationTypeI createReservationControlInformationTypeI() {
        return new ReservationControlInformationTypeI();
    }

    /**
     * Create an instance of {@link AdditionalBusinessSourceInformationType }
     * 
     */
    public AdditionalBusinessSourceInformationType createAdditionalBusinessSourceInformationType() {
        return new AdditionalBusinessSourceInformationType();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsTypeI }
     * 
     */
    public OriginatorIdentificationDetailsTypeI createOriginatorIdentificationDetailsTypeI() {
        return new OriginatorIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link QueueInformationDetailsTypeI }
     * 
     */
    public QueueInformationDetailsTypeI createQueueInformationDetailsTypeI() {
        return new QueueInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link QueueInformationTypeI }
     * 
     */
    public QueueInformationTypeI createQueueInformationTypeI() {
        return new QueueInformationTypeI();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsTypeI }
     * 
     */
    public ReservationControlInformationDetailsTypeI createReservationControlInformationDetailsTypeI() {
        return new ReservationControlInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsInformationTypeI }
     * 
     */
    public SelectionDetailsInformationTypeI createSelectionDetailsInformationTypeI() {
        return new SelectionDetailsInformationTypeI();
    }

    /**
     * Create an instance of {@link SourceTypeDetailsTypeI }
     * 
     */
    public SourceTypeDetailsTypeI createSourceTypeDetailsTypeI() {
        return new SourceTypeDetailsTypeI();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType }
     * 
     */
    public StructuredDateTimeInformationType createStructuredDateTimeInformationType() {
        return new StructuredDateTimeInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link SubQueueInformationDetailsTypeI }
     * 
     */
    public SubQueueInformationDetailsTypeI createSubQueueInformationDetailsTypeI() {
        return new SubQueueInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link SubQueueInformationTypeI }
     * 
     */
    public SubQueueInformationTypeI createSubQueueInformationTypeI() {
        return new SubQueueInformationTypeI();
    }

}
