
package com.amadeus.xml.tmrcrq_11_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fareRecommendationId" type="{http://xml.amadeus.com/TMRCRQ_11_1_1A}ItemReferencesAndVersionsType" maxOccurs="99"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareRecommendationId"
})
@XmlRootElement(name = "MiniRule_GetFromPricing")
public class MiniRuleGetFromPricing {

    @XmlElement(required = true)
    protected List<ItemReferencesAndVersionsType> fareRecommendationId;

    /**
     * Gets the value of the fareRecommendationId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fareRecommendationId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFareRecommendationId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemReferencesAndVersionsType }
     * 
     * 
     */
    public List<ItemReferencesAndVersionsType> getFareRecommendationId() {
        if (fareRecommendationId == null) {
            fareRecommendationId = new ArrayList<ItemReferencesAndVersionsType>();
        }
        return this.fareRecommendationId;
    }

}
