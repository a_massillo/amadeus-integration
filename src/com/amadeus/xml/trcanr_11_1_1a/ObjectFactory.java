
package com.amadeus.xml.trcanr_11_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.trcanr_11_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.trcanr_11_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TicketCancelDocumentReply }
     * 
     */
    public TicketCancelDocumentReply createTicketCancelDocumentReply() {
        return new TicketCancelDocumentReply();
    }

    /**
     * Create an instance of {@link TicketCancelDocumentReply.TransactionResults }
     * 
     */
    public TicketCancelDocumentReply.TransactionResults createTicketCancelDocumentReplyTransactionResults() {
        return new TicketCancelDocumentReply.TransactionResults();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailType }
     * 
     */
    public ApplicationErrorDetailType createApplicationErrorDetailType() {
        return new ApplicationErrorDetailType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType }
     * 
     */
    public ApplicationErrorInformationType createApplicationErrorInformationType() {
        return new ApplicationErrorInformationType();
    }

    /**
     * Create an instance of {@link ErrorGroupType }
     * 
     */
    public ErrorGroupType createErrorGroupType() {
        return new ErrorGroupType();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationTypeI }
     * 
     */
    public ItemNumberIdentificationTypeI createItemNumberIdentificationTypeI() {
        return new ItemNumberIdentificationTypeI();
    }

    /**
     * Create an instance of {@link ItemNumberTypeI }
     * 
     */
    public ItemNumberTypeI createItemNumberTypeI() {
        return new ItemNumberTypeI();
    }

    /**
     * Create an instance of {@link ReferenceInformationTypeI }
     * 
     */
    public ReferenceInformationTypeI createReferenceInformationTypeI() {
        return new ReferenceInformationTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI }
     * 
     */
    public ReferencingDetailsTypeI createReferencingDetailsTypeI() {
        return new ReferencingDetailsTypeI();
    }

    /**
     * Create an instance of {@link ResponseAnalysisDetailsTypeI }
     * 
     */
    public ResponseAnalysisDetailsTypeI createResponseAnalysisDetailsTypeI() {
        return new ResponseAnalysisDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

}
