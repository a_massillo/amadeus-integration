
package com.amadeus.xml.trcanr_11_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="transactionResults" maxOccurs="20"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="responseDetails" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ResponseAnalysisDetailsTypeI"/&gt;
 *                   &lt;element name="sequenceNumberDetails" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ItemNumberTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="ticketNumbers" type="{http://xml.amadeus.com/TRCANR_11_1_1A}TicketNumberTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="errorGroup" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ErrorGroupType" minOccurs="0"/&gt;
 *                   &lt;element name="sacNumber" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ReferenceInformationTypeI" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionResults"
})
@XmlRootElement(name = "Ticket_CancelDocumentReply")
public class TicketCancelDocumentReply {

    @XmlElement(required = true)
    protected List<TicketCancelDocumentReply.TransactionResults> transactionResults;

    /**
     * Gets the value of the transactionResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TicketCancelDocumentReply.TransactionResults }
     * 
     * 
     */
    public List<TicketCancelDocumentReply.TransactionResults> getTransactionResults() {
        if (transactionResults == null) {
            transactionResults = new ArrayList<TicketCancelDocumentReply.TransactionResults>();
        }
        return this.transactionResults;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="responseDetails" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ResponseAnalysisDetailsTypeI"/&gt;
     *         &lt;element name="sequenceNumberDetails" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ItemNumberTypeI" minOccurs="0"/&gt;
     *         &lt;element name="ticketNumbers" type="{http://xml.amadeus.com/TRCANR_11_1_1A}TicketNumberTypeI" minOccurs="0"/&gt;
     *         &lt;element name="errorGroup" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ErrorGroupType" minOccurs="0"/&gt;
     *         &lt;element name="sacNumber" type="{http://xml.amadeus.com/TRCANR_11_1_1A}ReferenceInformationTypeI" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "responseDetails",
        "sequenceNumberDetails",
        "ticketNumbers",
        "errorGroup",
        "sacNumber"
    })
    public static class TransactionResults {

        @XmlElement(required = true)
        protected ResponseAnalysisDetailsTypeI responseDetails;
        protected ItemNumberTypeI sequenceNumberDetails;
        protected TicketNumberTypeI ticketNumbers;
        protected ErrorGroupType errorGroup;
        protected ReferenceInformationTypeI sacNumber;

        /**
         * Gets the value of the responseDetails property.
         * 
         * @return
         *     possible object is
         *     {@link ResponseAnalysisDetailsTypeI }
         *     
         */
        public ResponseAnalysisDetailsTypeI getResponseDetails() {
            return responseDetails;
        }

        /**
         * Sets the value of the responseDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link ResponseAnalysisDetailsTypeI }
         *     
         */
        public void setResponseDetails(ResponseAnalysisDetailsTypeI value) {
            this.responseDetails = value;
        }

        /**
         * Gets the value of the sequenceNumberDetails property.
         * 
         * @return
         *     possible object is
         *     {@link ItemNumberTypeI }
         *     
         */
        public ItemNumberTypeI getSequenceNumberDetails() {
            return sequenceNumberDetails;
        }

        /**
         * Sets the value of the sequenceNumberDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemNumberTypeI }
         *     
         */
        public void setSequenceNumberDetails(ItemNumberTypeI value) {
            this.sequenceNumberDetails = value;
        }

        /**
         * Gets the value of the ticketNumbers property.
         * 
         * @return
         *     possible object is
         *     {@link TicketNumberTypeI }
         *     
         */
        public TicketNumberTypeI getTicketNumbers() {
            return ticketNumbers;
        }

        /**
         * Sets the value of the ticketNumbers property.
         * 
         * @param value
         *     allowed object is
         *     {@link TicketNumberTypeI }
         *     
         */
        public void setTicketNumbers(TicketNumberTypeI value) {
            this.ticketNumbers = value;
        }

        /**
         * Gets the value of the errorGroup property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorGroupType }
         *     
         */
        public ErrorGroupType getErrorGroup() {
            return errorGroup;
        }

        /**
         * Sets the value of the errorGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorGroupType }
         *     
         */
        public void setErrorGroup(ErrorGroupType value) {
            this.errorGroup = value;
        }

        /**
         * Gets the value of the sacNumber property.
         * 
         * @return
         *     possible object is
         *     {@link ReferenceInformationTypeI }
         *     
         */
        public ReferenceInformationTypeI getSacNumber() {
            return sacNumber;
        }

        /**
         * Sets the value of the sacNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReferenceInformationTypeI }
         *     
         */
        public void setSacNumber(ReferenceInformationTypeI value) {
            this.sacNumber = value;
        }

    }

}
