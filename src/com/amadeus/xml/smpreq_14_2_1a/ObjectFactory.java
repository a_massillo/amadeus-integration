
package com.amadeus.xml.smpreq_14_2_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.smpreq_14_2_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.smpreq_14_2_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMap }
     * 
     */
    public AirRetrieveSeatMap createAirRetrieveSeatMap() {
        return new AirRetrieveSeatMap();
    }

    /**
     * Create an instance of {@link TravelProductInformationTypeI }
     * 
     */
    public TravelProductInformationTypeI createTravelProductInformationTypeI() {
        return new TravelProductInformationTypeI();
    }

    /**
     * Create an instance of {@link SeatRequestParametersTypeI }
     * 
     */
    public SeatRequestParametersTypeI createSeatRequestParametersTypeI() {
        return new SeatRequestParametersTypeI();
    }

    /**
     * Create an instance of {@link RelatedProductInformationTypeI }
     * 
     */
    public RelatedProductInformationTypeI createRelatedProductInformationTypeI() {
        return new RelatedProductInformationTypeI();
    }

    /**
     * Create an instance of {@link FrequentTravellerInformationTypeI }
     * 
     */
    public FrequentTravellerInformationTypeI createFrequentTravellerInformationTypeI() {
        return new FrequentTravellerInformationTypeI();
    }

    /**
     * Create an instance of {@link ReservationControlInformationTypeI }
     * 
     */
    public ReservationControlInformationTypeI createReservationControlInformationTypeI() {
        return new ReservationControlInformationTypeI();
    }

    /**
     * Create an instance of {@link EquipmentInfoTypeI }
     * 
     */
    public EquipmentInfoTypeI createEquipmentInfoTypeI() {
        return new EquipmentInfoTypeI();
    }

    /**
     * Create an instance of {@link AdditionalBusinessSourceInformationTypeI }
     * 
     */
    public AdditionalBusinessSourceInformationTypeI createAdditionalBusinessSourceInformationTypeI() {
        return new AdditionalBusinessSourceInformationTypeI();
    }

    /**
     * Create an instance of {@link ConversionRateTypeI }
     * 
     */
    public ConversionRateTypeI createConversionRateTypeI() {
        return new ConversionRateTypeI();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMap.Traveler }
     * 
     */
    public AirRetrieveSeatMap.Traveler createAirRetrieveSeatMapTraveler() {
        return new AirRetrieveSeatMap.Traveler();
    }

    /**
     * Create an instance of {@link ReferenceInformationType }
     * 
     */
    public ReferenceInformationType createReferenceInformationType() {
        return new ReferenceInformationType();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link AdditionalEquipmentInformationTypeI }
     * 
     */
    public AdditionalEquipmentInformationTypeI createAdditionalEquipmentInformationTypeI() {
        return new AdditionalEquipmentInformationTypeI();
    }

    /**
     * Create an instance of {@link AdditionalFareQualifierDetailsTypeI }
     * 
     */
    public AdditionalFareQualifierDetailsTypeI createAdditionalFareQualifierDetailsTypeI() {
        return new AdditionalFareQualifierDetailsTypeI();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link ConfigurationDetailsTypeI }
     * 
     */
    public ConfigurationDetailsTypeI createConfigurationDetailsTypeI() {
        return new ConfigurationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ConversionRateDetailsTypeI }
     * 
     */
    public ConversionRateDetailsTypeI createConversionRateDetailsTypeI() {
        return new ConversionRateDetailsTypeI();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsTypeI }
     * 
     */
    public DateAndTimeDetailsTypeI createDateAndTimeDetailsTypeI() {
        return new DateAndTimeDetailsTypeI();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationTypeI }
     * 
     */
    public DateAndTimeInformationTypeI createDateAndTimeInformationTypeI() {
        return new DateAndTimeInformationTypeI();
    }

    /**
     * Create an instance of {@link FareInformationTypeI }
     * 
     */
    public FareInformationTypeI createFareInformationTypeI() {
        return new FareInformationTypeI();
    }

    /**
     * Create an instance of {@link FareQualifierDetailsTypeI }
     * 
     */
    public FareQualifierDetailsTypeI createFareQualifierDetailsTypeI() {
        return new FareQualifierDetailsTypeI();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationTypeI }
     * 
     */
    public FrequentTravellerIdentificationTypeI createFrequentTravellerIdentificationTypeI() {
        return new FrequentTravellerIdentificationTypeI();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationTypeI262024C }
     * 
     */
    public FrequentTravellerIdentificationTypeI262024C createFrequentTravellerIdentificationTypeI262024C() {
        return new FrequentTravellerIdentificationTypeI262024C();
    }

    /**
     * Create an instance of {@link FrequentTravellerInformationTypeI187045S }
     * 
     */
    public FrequentTravellerInformationTypeI187045S createFrequentTravellerInformationTypeI187045S() {
        return new FrequentTravellerInformationTypeI187045S();
    }

    /**
     * Create an instance of {@link GenericDetailsTypeI }
     * 
     */
    public GenericDetailsTypeI createGenericDetailsTypeI() {
        return new GenericDetailsTypeI();
    }

    /**
     * Create an instance of {@link LocationTypeI }
     * 
     */
    public LocationTypeI createLocationTypeI() {
        return new LocationTypeI();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsTypeI }
     * 
     */
    public OriginatorIdentificationDetailsTypeI createOriginatorIdentificationDetailsTypeI() {
        return new OriginatorIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductDateTimeTypeI }
     * 
     */
    public ProductDateTimeTypeI createProductDateTimeTypeI() {
        return new ProductDateTimeTypeI();
    }

    /**
     * Create an instance of {@link ProductIdentificationDetailsTypeI }
     * 
     */
    public ProductIdentificationDetailsTypeI createProductIdentificationDetailsTypeI() {
        return new ProductIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsTypeI }
     * 
     */
    public ProductTypeDetailsTypeI createProductTypeDetailsTypeI() {
        return new ProductTypeDetailsTypeI();
    }

    /**
     * Create an instance of {@link RangeOfRowsDetailsTypeI }
     * 
     */
    public RangeOfRowsDetailsTypeI createRangeOfRowsDetailsTypeI() {
        return new RangeOfRowsDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsTypeI }
     * 
     */
    public ReservationControlInformationDetailsTypeI createReservationControlInformationDetailsTypeI() {
        return new ReservationControlInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link SourceTypeDetailsTypeI }
     * 
     */
    public SourceTypeDetailsTypeI createSourceTypeDetailsTypeI() {
        return new SourceTypeDetailsTypeI();
    }

    /**
     * Create an instance of {@link SpecialRequirementsDetailsType }
     * 
     */
    public SpecialRequirementsDetailsType createSpecialRequirementsDetailsType() {
        return new SpecialRequirementsDetailsType();
    }

    /**
     * Create an instance of {@link SpecialRequirementsTypeDetailsType }
     * 
     */
    public SpecialRequirementsTypeDetailsType createSpecialRequirementsTypeDetailsType() {
        return new SpecialRequirementsTypeDetailsType();
    }

    /**
     * Create an instance of {@link StatusDetailsType }
     * 
     */
    public StatusDetailsType createStatusDetailsType() {
        return new StatusDetailsType();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

    /**
     * Create an instance of {@link TravellerDetailsType }
     * 
     */
    public TravellerDetailsType createTravellerDetailsType() {
        return new TravellerDetailsType();
    }

    /**
     * Create an instance of {@link TravellerInformationType }
     * 
     */
    public TravellerInformationType createTravellerInformationType() {
        return new TravellerInformationType();
    }

    /**
     * Create an instance of {@link TravellerSurnameInformationType }
     * 
     */
    public TravellerSurnameInformationType createTravellerSurnameInformationType() {
        return new TravellerSurnameInformationType();
    }

}
