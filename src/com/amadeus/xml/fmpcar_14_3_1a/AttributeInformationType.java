
package com.amadeus.xml.fmpcar_14_3_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * To identify the type of attribute and the attribute
 * 
 * <p>Java class for AttributeInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttributeInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="feeParameterType" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}AlphaNumericString_Length3To3" minOccurs="0"/&gt;
 *         &lt;element name="feeParameterDescription" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}AlphaNumericString_Length1To15" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttributeInformationType", propOrder = {
    "feeParameterType",
    "feeParameterDescription"
})
public class AttributeInformationType {

    protected String feeParameterType;
    protected String feeParameterDescription;

    /**
     * Gets the value of the feeParameterType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeParameterType() {
        return feeParameterType;
    }

    /**
     * Sets the value of the feeParameterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeParameterType(String value) {
        this.feeParameterType = value;
    }

    /**
     * Gets the value of the feeParameterDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeeParameterDescription() {
        return feeParameterDescription;
    }

    /**
     * Sets the value of the feeParameterDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeeParameterDescription(String value) {
        this.feeParameterDescription = value;
    }

}
