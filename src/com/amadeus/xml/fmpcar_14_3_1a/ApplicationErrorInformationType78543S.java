
package com.amadeus.xml.fmpcar_14_3_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To identify the type of application error within a message
 * 
 * <p>Java class for ApplicationErrorInformationType_78543S complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationErrorInformationType_78543S"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="applicationErrorDetail" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}ApplicationErrorInformationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationErrorInformationType_78543S", propOrder = {
    "applicationErrorDetail"
})
public class ApplicationErrorInformationType78543S {

    @XmlElement(required = true)
    protected ApplicationErrorInformationType applicationErrorDetail;

    /**
     * Gets the value of the applicationErrorDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationErrorInformationType }
     *     
     */
    public ApplicationErrorInformationType getApplicationErrorDetail() {
        return applicationErrorDetail;
    }

    /**
     * Sets the value of the applicationErrorDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationErrorInformationType }
     *     
     */
    public void setApplicationErrorDetail(ApplicationErrorInformationType value) {
        this.applicationErrorDetail = value;
    }

}
