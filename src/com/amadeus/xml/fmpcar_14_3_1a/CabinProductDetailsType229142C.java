
package com.amadeus.xml.fmpcar_14_3_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CabinProductDetailsType_229142C complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CabinProductDetailsType_229142C"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rbd" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}AlphaString_Length1To1"/&gt;
 *         &lt;element name="cabin" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}AlphaString_Length1To1" minOccurs="0"/&gt;
 *         &lt;element name="avlStatus" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}AMA_EDICodesetType_Length1to3" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CabinProductDetailsType_229142C", propOrder = {
    "rbd",
    "cabin",
    "avlStatus"
})
public class CabinProductDetailsType229142C {

    @XmlElement(required = true)
    protected String rbd;
    protected String cabin;
    protected String avlStatus;

    /**
     * Gets the value of the rbd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRbd() {
        return rbd;
    }

    /**
     * Sets the value of the rbd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRbd(String value) {
        this.rbd = value;
    }

    /**
     * Gets the value of the cabin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabin() {
        return cabin;
    }

    /**
     * Sets the value of the cabin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabin(String value) {
        this.cabin = value;
    }

    /**
     * Gets the value of the avlStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAvlStatus() {
        return avlStatus;
    }

    /**
     * Sets the value of the avlStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAvlStatus(String value) {
        this.avlStatus = value;
    }

}
