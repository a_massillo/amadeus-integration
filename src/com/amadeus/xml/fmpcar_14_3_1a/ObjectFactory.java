
package com.amadeus.xml.fmpcar_14_3_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.fmpcar_14_3_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.fmpcar_14_3_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply }
     * 
     */
    public FareMasterPricerCalendarReply createFareMasterPricerCalendarReply() {
        return new FareMasterPricerCalendarReply();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.MnrGrp }
     * 
     */
    public FareMasterPricerCalendarReply.MnrGrp createFareMasterPricerCalendarReplyMnrGrp() {
        return new FareMasterPricerCalendarReply.MnrGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.MnrGrp.MnrDetails }
     * 
     */
    public FareMasterPricerCalendarReply.MnrGrp.MnrDetails createFareMasterPricerCalendarReplyMnrGrpMnrDetails() {
        return new FareMasterPricerCalendarReply.MnrGrp.MnrDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp createFareMasterPricerCalendarReplyServiceFeesGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceDetailsGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceDetailsGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceDetailsGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceDetailsGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceFeeInfoGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp.ServiceDetailsGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp.ServiceDetailsGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceFeeInfoGrpServiceDetailsGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp.ServiceDetailsGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceCoverageInfoGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceCoverageInfoGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceCoverageInfoGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceCoverageInfoGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.OtherSolutions }
     * 
     */
    public FareMasterPricerCalendarReply.OtherSolutions createFareMasterPricerCalendarReplyOtherSolutions() {
        return new FareMasterPricerCalendarReply.OtherSolutions();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation createFareMasterPricerCalendarReplyRecommendation() {
        return new FareMasterPricerCalendarReply.Recommendation();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails createFareMasterPricerCalendarReplyRecommendationSpecificRecDetails() {
        return new FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails createFareMasterPricerCalendarReplyRecommendationSpecificRecDetailsSpecificProductDetails() {
        return new FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails.FareContextDetails }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails.FareContextDetails createFareMasterPricerCalendarReplyRecommendationSpecificRecDetailsSpecificProductDetailsFareContextDetails() {
        return new FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails.FareContextDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.PaxFareProduct }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.PaxFareProduct createFareMasterPricerCalendarReplyRecommendationPaxFareProduct() {
        return new FareMasterPricerCalendarReply.Recommendation.PaxFareProduct();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.FareDetails }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.FareDetails createFareMasterPricerCalendarReplyRecommendationPaxFareProductFareDetails() {
        return new FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.FareDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.FlightIndex }
     * 
     */
    public FareMasterPricerCalendarReply.FlightIndex createFareMasterPricerCalendarReplyFlightIndex() {
        return new FareMasterPricerCalendarReply.FlightIndex();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.FlightIndex.GroupOfFlights }
     * 
     */
    public FareMasterPricerCalendarReply.FlightIndex.GroupOfFlights createFareMasterPricerCalendarReplyFlightIndexGroupOfFlights() {
        return new FareMasterPricerCalendarReply.FlightIndex.GroupOfFlights();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.AmountInfoPerPax }
     * 
     */
    public FareMasterPricerCalendarReply.AmountInfoPerPax createFareMasterPricerCalendarReplyAmountInfoPerPax() {
        return new FareMasterPricerCalendarReply.AmountInfoPerPax();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.AmountInfoForAllPax }
     * 
     */
    public FareMasterPricerCalendarReply.AmountInfoForAllPax createFareMasterPricerCalendarReplyAmountInfoForAllPax() {
        return new FareMasterPricerCalendarReply.AmountInfoForAllPax();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ErrorMessage }
     * 
     */
    public FareMasterPricerCalendarReply.ErrorMessage createFareMasterPricerCalendarReplyErrorMessage() {
        return new FareMasterPricerCalendarReply.ErrorMessage();
    }

    /**
     * Create an instance of {@link ConversionRateTypeI }
     * 
     */
    public ConversionRateTypeI createConversionRateTypeI() {
        return new ConversionRateTypeI();
    }

    /**
     * Create an instance of {@link FareInformationType }
     * 
     */
    public FareInformationType createFareInformationType() {
        return new FareInformationType();
    }

    /**
     * Create an instance of {@link FareFamilyType }
     * 
     */
    public FareFamilyType createFareFamilyType() {
        return new FareFamilyType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.FeeDetails }
     * 
     */
    public FareMasterPricerCalendarReply.FeeDetails createFareMasterPricerCalendarReplyFeeDetails() {
        return new FareMasterPricerCalendarReply.FeeDetails();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTextType }
     * 
     */
    public CompanyIdentificationTextType createCompanyIdentificationTextType() {
        return new CompanyIdentificationTextType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.OfficeIdDetails }
     * 
     */
    public FareMasterPricerCalendarReply.OfficeIdDetails createFareMasterPricerCalendarReplyOfficeIdDetails() {
        return new FareMasterPricerCalendarReply.OfficeIdDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.WarningInfo }
     * 
     */
    public FareMasterPricerCalendarReply.WarningInfo createFareMasterPricerCalendarReplyWarningInfo() {
        return new FareMasterPricerCalendarReply.WarningInfo();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.GlobalInformation }
     * 
     */
    public FareMasterPricerCalendarReply.GlobalInformation createFareMasterPricerCalendarReplyGlobalInformation() {
        return new FareMasterPricerCalendarReply.GlobalInformation();
    }

    /**
     * Create an instance of {@link ValueSearchCriteriaType }
     * 
     */
    public ValueSearchCriteriaType createValueSearchCriteriaType() {
        return new ValueSearchCriteriaType();
    }

    /**
     * Create an instance of {@link ActionDetailsType }
     * 
     */
    public ActionDetailsType createActionDetailsType() {
        return new ActionDetailsType();
    }

    /**
     * Create an instance of {@link AdditionalFareQualifierDetailsTypeI }
     * 
     */
    public AdditionalFareQualifierDetailsTypeI createAdditionalFareQualifierDetailsTypeI() {
        return new AdditionalFareQualifierDetailsTypeI();
    }

    /**
     * Create an instance of {@link AdditionalProductDetailsType }
     * 
     */
    public AdditionalProductDetailsType createAdditionalProductDetailsType() {
        return new AdditionalProductDetailsType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType }
     * 
     */
    public ApplicationErrorInformationType createApplicationErrorInformationType() {
        return new ApplicationErrorInformationType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType78543S }
     * 
     */
    public ApplicationErrorInformationType78543S createApplicationErrorInformationType78543S() {
        return new ApplicationErrorInformationType78543S();
    }

    /**
     * Create an instance of {@link AttributeInformationTypeU }
     * 
     */
    public AttributeInformationTypeU createAttributeInformationTypeU() {
        return new AttributeInformationTypeU();
    }

    /**
     * Create an instance of {@link AttributeInformationType }
     * 
     */
    public AttributeInformationType createAttributeInformationType() {
        return new AttributeInformationType();
    }

    /**
     * Create an instance of {@link AttributeInformationType97181C }
     * 
     */
    public AttributeInformationType97181C createAttributeInformationType97181C() {
        return new AttributeInformationType97181C();
    }

    /**
     * Create an instance of {@link AttributeTypeU }
     * 
     */
    public AttributeTypeU createAttributeTypeU() {
        return new AttributeTypeU();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link AttributeType78561S }
     * 
     */
    public AttributeType78561S createAttributeType78561S() {
        return new AttributeType78561S();
    }

    /**
     * Create an instance of {@link BaggageDetailsType }
     * 
     */
    public BaggageDetailsType createBaggageDetailsType() {
        return new BaggageDetailsType();
    }

    /**
     * Create an instance of {@link BagtagDetailsType }
     * 
     */
    public BagtagDetailsType createBagtagDetailsType() {
        return new BagtagDetailsType();
    }

    /**
     * Create an instance of {@link CabinInformationType }
     * 
     */
    public CabinInformationType createCabinInformationType() {
        return new CabinInformationType();
    }

    /**
     * Create an instance of {@link CabinProductDetailsType }
     * 
     */
    public CabinProductDetailsType createCabinProductDetailsType() {
        return new CabinProductDetailsType();
    }

    /**
     * Create an instance of {@link CabinProductDetailsType195516C }
     * 
     */
    public CabinProductDetailsType195516C createCabinProductDetailsType195516C() {
        return new CabinProductDetailsType195516C();
    }

    /**
     * Create an instance of {@link CabinProductDetailsType205138C }
     * 
     */
    public CabinProductDetailsType205138C createCabinProductDetailsType205138C() {
        return new CabinProductDetailsType205138C();
    }

    /**
     * Create an instance of {@link CabinProductDetailsType229142C }
     * 
     */
    public CabinProductDetailsType229142C createCabinProductDetailsType229142C() {
        return new CabinProductDetailsType229142C();
    }

    /**
     * Create an instance of {@link CategDescrType }
     * 
     */
    public CategDescrType createCategDescrType() {
        return new CategDescrType();
    }

    /**
     * Create an instance of {@link CategoryDescriptionType }
     * 
     */
    public CategoryDescriptionType createCategoryDescriptionType() {
        return new CategoryDescriptionType();
    }

    /**
     * Create an instance of {@link ClassInformationType }
     * 
     */
    public ClassInformationType createClassInformationType() {
        return new ClassInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType }
     * 
     */
    public CodedAttributeInformationType createCodedAttributeInformationType() {
        return new CodedAttributeInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType260992C }
     * 
     */
    public CodedAttributeInformationType260992C createCodedAttributeInformationType260992C() {
        return new CodedAttributeInformationType260992C();
    }

    /**
     * Create an instance of {@link CodedAttributeType }
     * 
     */
    public CodedAttributeType createCodedAttributeType() {
        return new CodedAttributeType();
    }

    /**
     * Create an instance of {@link CommercialAgreementsType }
     * 
     */
    public CommercialAgreementsType createCommercialAgreementsType() {
        return new CommercialAgreementsType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link CompanyIdentificationType }
     * 
     */
    public CompanyIdentificationType createCompanyIdentificationType() {
        return new CompanyIdentificationType();
    }

    /**
     * Create an instance of {@link CompanyRoleIdentificationType }
     * 
     */
    public CompanyRoleIdentificationType createCompanyRoleIdentificationType() {
        return new CompanyRoleIdentificationType();
    }

    /**
     * Create an instance of {@link CompanyRoleIdentificationType120771C }
     * 
     */
    public CompanyRoleIdentificationType120771C createCompanyRoleIdentificationType120771C() {
        return new CompanyRoleIdentificationType120771C();
    }

    /**
     * Create an instance of {@link ConversionRateDetailsTypeI }
     * 
     */
    public ConversionRateDetailsTypeI createConversionRateDetailsTypeI() {
        return new ConversionRateDetailsTypeI();
    }

    /**
     * Create an instance of {@link ConversionRateDetailsTypeI179848C }
     * 
     */
    public ConversionRateDetailsTypeI179848C createConversionRateDetailsTypeI179848C() {
        return new ConversionRateDetailsTypeI179848C();
    }

    /**
     * Create an instance of {@link ConversionRateTypeI78562S }
     * 
     */
    public ConversionRateTypeI78562S createConversionRateTypeI78562S() {
        return new ConversionRateTypeI78562S();
    }

    /**
     * Create an instance of {@link CriteriaiDetaislType }
     * 
     */
    public CriteriaiDetaislType createCriteriaiDetaislType() {
        return new CriteriaiDetaislType();
    }

    /**
     * Create an instance of {@link DataInformationType }
     * 
     */
    public DataInformationType createDataInformationType() {
        return new DataInformationType();
    }

    /**
     * Create an instance of {@link DataTypeInformationType }
     * 
     */
    public DataTypeInformationType createDataTypeInformationType() {
        return new DataTypeInformationType();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsType }
     * 
     */
    public DateAndTimeDetailsType createDateAndTimeDetailsType() {
        return new DateAndTimeDetailsType();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsType256192C }
     * 
     */
    public DateAndTimeDetailsType256192C createDateAndTimeDetailsType256192C() {
        return new DateAndTimeDetailsType256192C();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationType }
     * 
     */
    public DateAndTimeInformationType createDateAndTimeInformationType() {
        return new DateAndTimeInformationType();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationType182345S }
     * 
     */
    public DateAndTimeInformationType182345S createDateAndTimeInformationType182345S() {
        return new DateAndTimeInformationType182345S();
    }

    /**
     * Create an instance of {@link DateTimePeriodDetailsTypeI }
     * 
     */
    public DateTimePeriodDetailsTypeI createDateTimePeriodDetailsTypeI() {
        return new DateTimePeriodDetailsTypeI();
    }

    /**
     * Create an instance of {@link DiscountAndPenaltyInformationType }
     * 
     */
    public DiscountAndPenaltyInformationType createDiscountAndPenaltyInformationType() {
        return new DiscountAndPenaltyInformationType();
    }

    /**
     * Create an instance of {@link DiscountPenaltyInformationType }
     * 
     */
    public DiscountPenaltyInformationType createDiscountPenaltyInformationType() {
        return new DiscountPenaltyInformationType();
    }

    /**
     * Create an instance of {@link DiscountPenaltyMonetaryInformationType }
     * 
     */
    public DiscountPenaltyMonetaryInformationType createDiscountPenaltyMonetaryInformationType() {
        return new DiscountPenaltyMonetaryInformationType();
    }

    /**
     * Create an instance of {@link DummySegmentTypeI }
     * 
     */
    public DummySegmentTypeI createDummySegmentTypeI() {
        return new DummySegmentTypeI();
    }

    /**
     * Create an instance of {@link ExcessBaggageType }
     * 
     */
    public ExcessBaggageType createExcessBaggageType() {
        return new ExcessBaggageType();
    }

    /**
     * Create an instance of {@link FareCalculationCodeDetailsType }
     * 
     */
    public FareCalculationCodeDetailsType createFareCalculationCodeDetailsType() {
        return new FareCalculationCodeDetailsType();
    }

    /**
     * Create an instance of {@link FareCategoryCodesTypeI }
     * 
     */
    public FareCategoryCodesTypeI createFareCategoryCodesTypeI() {
        return new FareCategoryCodesTypeI();
    }

    /**
     * Create an instance of {@link FareDetailsTypeI }
     * 
     */
    public FareDetailsTypeI createFareDetailsTypeI() {
        return new FareDetailsTypeI();
    }

    /**
     * Create an instance of {@link FareDetailsType }
     * 
     */
    public FareDetailsType createFareDetailsType() {
        return new FareDetailsType();
    }

    /**
     * Create an instance of {@link FareDetailsType193037C }
     * 
     */
    public FareDetailsType193037C createFareDetailsType193037C() {
        return new FareDetailsType193037C();
    }

    /**
     * Create an instance of {@link FareFamilyDetailsType }
     * 
     */
    public FareFamilyDetailsType createFareFamilyDetailsType() {
        return new FareFamilyDetailsType();
    }

    /**
     * Create an instance of {@link FareInformationTypeI }
     * 
     */
    public FareInformationTypeI createFareInformationTypeI() {
        return new FareInformationTypeI();
    }

    /**
     * Create an instance of {@link FareInformationType80868S }
     * 
     */
    public FareInformationType80868S createFareInformationType80868S() {
        return new FareInformationType80868S();
    }

    /**
     * Create an instance of {@link FareProductDetailsType }
     * 
     */
    public FareProductDetailsType createFareProductDetailsType() {
        return new FareProductDetailsType();
    }

    /**
     * Create an instance of {@link FareProductDetailsType248552C }
     * 
     */
    public FareProductDetailsType248552C createFareProductDetailsType248552C() {
        return new FareProductDetailsType248552C();
    }

    /**
     * Create an instance of {@link FareQualifierDetailsType }
     * 
     */
    public FareQualifierDetailsType createFareQualifierDetailsType() {
        return new FareQualifierDetailsType();
    }

    /**
     * Create an instance of {@link FareTypeGroupingInformationType }
     * 
     */
    public FareTypeGroupingInformationType createFareTypeGroupingInformationType() {
        return new FareTypeGroupingInformationType();
    }

    /**
     * Create an instance of {@link FlightCharacteristicsType }
     * 
     */
    public FlightCharacteristicsType createFlightCharacteristicsType() {
        return new FlightCharacteristicsType();
    }

    /**
     * Create an instance of {@link FlightProductInformationType }
     * 
     */
    public FlightProductInformationType createFlightProductInformationType() {
        return new FlightProductInformationType();
    }

    /**
     * Create an instance of {@link FlightProductInformationType141442S }
     * 
     */
    public FlightProductInformationType141442S createFlightProductInformationType141442S() {
        return new FlightProductInformationType141442S();
    }

    /**
     * Create an instance of {@link FlightProductInformationType161491S }
     * 
     */
    public FlightProductInformationType161491S createFlightProductInformationType161491S() {
        return new FlightProductInformationType161491S();
    }

    /**
     * Create an instance of {@link FlightProductInformationType176659S }
     * 
     */
    public FlightProductInformationType176659S createFlightProductInformationType176659S() {
        return new FlightProductInformationType176659S();
    }

    /**
     * Create an instance of {@link FlightServicesType }
     * 
     */
    public FlightServicesType createFlightServicesType() {
        return new FlightServicesType();
    }

    /**
     * Create an instance of {@link FreeTextQualificationTypeI }
     * 
     */
    public FreeTextQualificationTypeI createFreeTextQualificationTypeI() {
        return new FreeTextQualificationTypeI();
    }

    /**
     * Create an instance of {@link FreeTextQualificationType }
     * 
     */
    public FreeTextQualificationType createFreeTextQualificationType() {
        return new FreeTextQualificationType();
    }

    /**
     * Create an instance of {@link FreeTextQualificationType120769C }
     * 
     */
    public FreeTextQualificationType120769C createFreeTextQualificationType120769C() {
        return new FreeTextQualificationType120769C();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationCodeType }
     * 
     */
    public FrequentTravellerIdentificationCodeType createFrequentTravellerIdentificationCodeType() {
        return new FrequentTravellerIdentificationCodeType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationType }
     * 
     */
    public FrequentTravellerIdentificationType createFrequentTravellerIdentificationType() {
        return new FrequentTravellerIdentificationType();
    }

    /**
     * Create an instance of {@link HeaderInformationTypeI }
     * 
     */
    public HeaderInformationTypeI createHeaderInformationTypeI() {
        return new HeaderInformationTypeI();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextType }
     * 
     */
    public InteractiveFreeTextType createInteractiveFreeTextType() {
        return new InteractiveFreeTextType();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextType78534S }
     * 
     */
    public InteractiveFreeTextType78534S createInteractiveFreeTextType78534S() {
        return new InteractiveFreeTextType78534S();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextType78544S }
     * 
     */
    public InteractiveFreeTextType78544S createInteractiveFreeTextType78544S() {
        return new InteractiveFreeTextType78544S();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextType78559S }
     * 
     */
    public InteractiveFreeTextType78559S createInteractiveFreeTextType78559S() {
        return new InteractiveFreeTextType78559S();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType }
     * 
     */
    public ItemNumberIdentificationType createItemNumberIdentificationType() {
        return new ItemNumberIdentificationType();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType191597C }
     * 
     */
    public ItemNumberIdentificationType191597C createItemNumberIdentificationType191597C() {
        return new ItemNumberIdentificationType191597C();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType192331C }
     * 
     */
    public ItemNumberIdentificationType192331C createItemNumberIdentificationType192331C() {
        return new ItemNumberIdentificationType192331C();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType234878C }
     * 
     */
    public ItemNumberIdentificationType234878C createItemNumberIdentificationType234878C() {
        return new ItemNumberIdentificationType234878C();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType248537C }
     * 
     */
    public ItemNumberIdentificationType248537C createItemNumberIdentificationType248537C() {
        return new ItemNumberIdentificationType248537C();
    }

    /**
     * Create an instance of {@link ItemNumberType }
     * 
     */
    public ItemNumberType createItemNumberType() {
        return new ItemNumberType();
    }

    /**
     * Create an instance of {@link ItemNumberType161497S }
     * 
     */
    public ItemNumberType161497S createItemNumberType161497S() {
        return new ItemNumberType161497S();
    }

    /**
     * Create an instance of {@link ItemNumberType166130S }
     * 
     */
    public ItemNumberType166130S createItemNumberType166130S() {
        return new ItemNumberType166130S();
    }

    /**
     * Create an instance of {@link ItemNumberType176648S }
     * 
     */
    public ItemNumberType176648S createItemNumberType176648S() {
        return new ItemNumberType176648S();
    }

    /**
     * Create an instance of {@link ItemNumberType80866S }
     * 
     */
    public ItemNumberType80866S createItemNumberType80866S() {
        return new ItemNumberType80866S();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType }
     * 
     */
    public ItemReferencesAndVersionsType createItemReferencesAndVersionsType() {
        return new ItemReferencesAndVersionsType();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType78536S }
     * 
     */
    public ItemReferencesAndVersionsType78536S createItemReferencesAndVersionsType78536S() {
        return new ItemReferencesAndVersionsType78536S();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType78564S }
     * 
     */
    public ItemReferencesAndVersionsType78564S createItemReferencesAndVersionsType78564S() {
        return new ItemReferencesAndVersionsType78564S();
    }

    /**
     * Create an instance of {@link ItineraryDetailsType }
     * 
     */
    public ItineraryDetailsType createItineraryDetailsType() {
        return new ItineraryDetailsType();
    }

    /**
     * Create an instance of {@link LocationIdentificationDetailsType }
     * 
     */
    public LocationIdentificationDetailsType createLocationIdentificationDetailsType() {
        return new LocationIdentificationDetailsType();
    }

    /**
     * Create an instance of {@link MiniRulesDetailsType }
     * 
     */
    public MiniRulesDetailsType createMiniRulesDetailsType() {
        return new MiniRulesDetailsType();
    }

    /**
     * Create an instance of {@link MiniRulesIndicatorType }
     * 
     */
    public MiniRulesIndicatorType createMiniRulesIndicatorType() {
        return new MiniRulesIndicatorType();
    }

    /**
     * Create an instance of {@link MiniRulesType }
     * 
     */
    public MiniRulesType createMiniRulesType() {
        return new MiniRulesType();
    }

    /**
     * Create an instance of {@link MiniRulesType78547S }
     * 
     */
    public MiniRulesType78547S createMiniRulesType78547S() {
        return new MiniRulesType78547S();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI }
     * 
     */
    public MonetaryInformationDetailsTypeI createMonetaryInformationDetailsTypeI() {
        return new MonetaryInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType }
     * 
     */
    public MonetaryInformationDetailsType createMonetaryInformationDetailsType() {
        return new MonetaryInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType245528C }
     * 
     */
    public MonetaryInformationDetailsType245528C createMonetaryInformationDetailsType245528C() {
        return new MonetaryInformationDetailsType245528C();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI }
     * 
     */
    public MonetaryInformationTypeI createMonetaryInformationTypeI() {
        return new MonetaryInformationTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link MonetaryInformationType137835S }
     * 
     */
    public MonetaryInformationType137835S createMonetaryInformationType137835S() {
        return new MonetaryInformationType137835S();
    }

    /**
     * Create an instance of {@link MonetaryInformationType174241S }
     * 
     */
    public MonetaryInformationType174241S createMonetaryInformationType174241S() {
        return new MonetaryInformationType174241S();
    }

    /**
     * Create an instance of {@link MonetaryInformationType185955S }
     * 
     */
    public MonetaryInformationType185955S createMonetaryInformationType185955S() {
        return new MonetaryInformationType185955S();
    }

    /**
     * Create an instance of {@link OnTimePerformanceType }
     * 
     */
    public OnTimePerformanceType createOnTimePerformanceType() {
        return new OnTimePerformanceType();
    }

    /**
     * Create an instance of {@link OriginAndDestinationRequestType }
     * 
     */
    public OriginAndDestinationRequestType createOriginAndDestinationRequestType() {
        return new OriginAndDestinationRequestType();
    }

    /**
     * Create an instance of {@link OriginAndDestinationRequestType134833S }
     * 
     */
    public OriginAndDestinationRequestType134833S createOriginAndDestinationRequestType134833S() {
        return new OriginAndDestinationRequestType134833S();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsTypeI }
     * 
     */
    public OriginatorIdentificationDetailsTypeI createOriginatorIdentificationDetailsTypeI() {
        return new OriginatorIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link PricingTicketingInformationType }
     * 
     */
    public PricingTicketingInformationType createPricingTicketingInformationType() {
        return new PricingTicketingInformationType();
    }

    /**
     * Create an instance of {@link PricingTicketingSubsequentType }
     * 
     */
    public PricingTicketingSubsequentType createPricingTicketingSubsequentType() {
        return new PricingTicketingSubsequentType();
    }

    /**
     * Create an instance of {@link PricingTicketingSubsequentType144401S }
     * 
     */
    public PricingTicketingSubsequentType144401S createPricingTicketingSubsequentType144401S() {
        return new PricingTicketingSubsequentType144401S();
    }

    /**
     * Create an instance of {@link ProcessingInformationType }
     * 
     */
    public ProcessingInformationType createProcessingInformationType() {
        return new ProcessingInformationType();
    }

    /**
     * Create an instance of {@link ProductDateTimeType }
     * 
     */
    public ProductDateTimeType createProductDateTimeType() {
        return new ProductDateTimeType();
    }

    /**
     * Create an instance of {@link ProductDetailsType }
     * 
     */
    public ProductDetailsType createProductDetailsType() {
        return new ProductDetailsType();
    }

    /**
     * Create an instance of {@link ProductFacilitiesType }
     * 
     */
    public ProductFacilitiesType createProductFacilitiesType() {
        return new ProductFacilitiesType();
    }

    /**
     * Create an instance of {@link ProductInformationType }
     * 
     */
    public ProductInformationType createProductInformationType() {
        return new ProductInformationType();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsType }
     * 
     */
    public ProductTypeDetailsType createProductTypeDetailsType() {
        return new ProductTypeDetailsType();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsType205137C }
     * 
     */
    public ProductTypeDetailsType205137C createProductTypeDetailsType205137C() {
        return new ProductTypeDetailsType205137C();
    }

    /**
     * Create an instance of {@link ProposedSegmentDetailsType }
     * 
     */
    public ProposedSegmentDetailsType createProposedSegmentDetailsType() {
        return new ProposedSegmentDetailsType();
    }

    /**
     * Create an instance of {@link ProposedSegmentType }
     * 
     */
    public ProposedSegmentType createProposedSegmentType() {
        return new ProposedSegmentType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType133176S }
     * 
     */
    public ReferenceInfoType133176S createReferenceInfoType133176S() {
        return new ReferenceInfoType133176S();
    }

    /**
     * Create an instance of {@link ReferenceInfoType134839S }
     * 
     */
    public ReferenceInfoType134839S createReferenceInfoType134839S() {
        return new ReferenceInfoType134839S();
    }

    /**
     * Create an instance of {@link ReferenceInfoType134840S }
     * 
     */
    public ReferenceInfoType134840S createReferenceInfoType134840S() {
        return new ReferenceInfoType134840S();
    }

    /**
     * Create an instance of {@link ReferenceInfoType165972S }
     * 
     */
    public ReferenceInfoType165972S createReferenceInfoType165972S() {
        return new ReferenceInfoType165972S();
    }

    /**
     * Create an instance of {@link ReferenceInfoType176658S }
     * 
     */
    public ReferenceInfoType176658S createReferenceInfoType176658S() {
        return new ReferenceInfoType176658S();
    }

    /**
     * Create an instance of {@link ReferenceType }
     * 
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType191583C }
     * 
     */
    public ReferencingDetailsType191583C createReferencingDetailsType191583C() {
        return new ReferencingDetailsType191583C();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType195561C }
     * 
     */
    public ReferencingDetailsType195561C createReferencingDetailsType195561C() {
        return new ReferencingDetailsType195561C();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType234704C }
     * 
     */
    public ReferencingDetailsType234704C createReferencingDetailsType234704C() {
        return new ReferencingDetailsType234704C();
    }

    /**
     * Create an instance of {@link SegmentRepetitionControlDetailsTypeI }
     * 
     */
    public SegmentRepetitionControlDetailsTypeI createSegmentRepetitionControlDetailsTypeI() {
        return new SegmentRepetitionControlDetailsTypeI();
    }

    /**
     * Create an instance of {@link SegmentRepetitionControlTypeI }
     * 
     */
    public SegmentRepetitionControlTypeI createSegmentRepetitionControlTypeI() {
        return new SegmentRepetitionControlTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsInformationType }
     * 
     */
    public SelectionDetailsInformationType createSelectionDetailsInformationType() {
        return new SelectionDetailsInformationType();
    }

    /**
     * Create an instance of {@link SelectionDetailsType }
     * 
     */
    public SelectionDetailsType createSelectionDetailsType() {
        return new SelectionDetailsType();
    }

    /**
     * Create an instance of {@link SequenceDetailsTypeU }
     * 
     */
    public SequenceDetailsTypeU createSequenceDetailsTypeU() {
        return new SequenceDetailsTypeU();
    }

    /**
     * Create an instance of {@link SequenceInformationTypeU }
     * 
     */
    public SequenceInformationTypeU createSequenceInformationTypeU() {
        return new SequenceInformationTypeU();
    }

    /**
     * Create an instance of {@link ServicesReferences }
     * 
     */
    public ServicesReferences createServicesReferences() {
        return new ServicesReferences();
    }

    /**
     * Create an instance of {@link SpecialRequirementsDataDetailsType }
     * 
     */
    public SpecialRequirementsDataDetailsType createSpecialRequirementsDataDetailsType() {
        return new SpecialRequirementsDataDetailsType();
    }

    /**
     * Create an instance of {@link SpecialRequirementsDetailsType }
     * 
     */
    public SpecialRequirementsDetailsType createSpecialRequirementsDetailsType() {
        return new SpecialRequirementsDetailsType();
    }

    /**
     * Create an instance of {@link SpecialRequirementsTypeDetailsType }
     * 
     */
    public SpecialRequirementsTypeDetailsType createSpecialRequirementsTypeDetailsType() {
        return new SpecialRequirementsTypeDetailsType();
    }

    /**
     * Create an instance of {@link SpecificDataInformationType }
     * 
     */
    public SpecificDataInformationType createSpecificDataInformationType() {
        return new SpecificDataInformationType();
    }

    /**
     * Create an instance of {@link SpecificTravellerDetailsType }
     * 
     */
    public SpecificTravellerDetailsType createSpecificTravellerDetailsType() {
        return new SpecificTravellerDetailsType();
    }

    /**
     * Create an instance of {@link SpecificTravellerType }
     * 
     */
    public SpecificTravellerType createSpecificTravellerType() {
        return new SpecificTravellerType();
    }

    /**
     * Create an instance of {@link StatusDetailsType }
     * 
     */
    public StatusDetailsType createStatusDetailsType() {
        return new StatusDetailsType();
    }

    /**
     * Create an instance of {@link StatusDetailsType256255C }
     * 
     */
    public StatusDetailsType256255C createStatusDetailsType256255C() {
        return new StatusDetailsType256255C();
    }

    /**
     * Create an instance of {@link StatusType182386S }
     * 
     */
    public StatusType182386S createStatusType182386S() {
        return new StatusType182386S();
    }

    /**
     * Create an instance of {@link TaxDetailsType }
     * 
     */
    public TaxDetailsType createTaxDetailsType() {
        return new TaxDetailsType();
    }

    /**
     * Create an instance of {@link TaxType }
     * 
     */
    public TaxType createTaxType() {
        return new TaxType();
    }

    /**
     * Create an instance of {@link TransportIdentifierType }
     * 
     */
    public TransportIdentifierType createTransportIdentifierType() {
        return new TransportIdentifierType();
    }

    /**
     * Create an instance of {@link TravelProductType }
     * 
     */
    public TravelProductType createTravelProductType() {
        return new TravelProductType();
    }

    /**
     * Create an instance of {@link TravellerDetailsType }
     * 
     */
    public TravellerDetailsType createTravellerDetailsType() {
        return new TravellerDetailsType();
    }

    /**
     * Create an instance of {@link TravellerReferenceInformationType }
     * 
     */
    public TravellerReferenceInformationType createTravellerReferenceInformationType() {
        return new TravellerReferenceInformationType();
    }

    /**
     * Create an instance of {@link UserIdentificationType }
     * 
     */
    public UserIdentificationType createUserIdentificationType() {
        return new UserIdentificationType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.MnrGrp.MnrDetails.CatGrp }
     * 
     */
    public FareMasterPricerCalendarReply.MnrGrp.MnrDetails.CatGrp createFareMasterPricerCalendarReplyMnrGrpMnrDetailsCatGrp() {
        return new FareMasterPricerCalendarReply.MnrGrp.MnrDetails.CatGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeRefGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeRefGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceFeeRefGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeRefGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.FreeBagAllowanceGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.FreeBagAllowanceGrp createFareMasterPricerCalendarReplyServiceFeesGrpFreeBagAllowanceGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.FreeBagAllowanceGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceDetailsGrp.FeeDescriptionGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceDetailsGrp.FeeDescriptionGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceDetailsGrpFeeDescriptionGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceDetailsGrp.FeeDescriptionGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp.ServiceDetailsGrp.ServiceMatchedInfoGroup }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp.ServiceDetailsGrp.ServiceMatchedInfoGroup createFareMasterPricerCalendarReplyServiceFeesGrpServiceFeeInfoGrpServiceDetailsGrpServiceMatchedInfoGroup() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceFeeInfoGrp.ServiceDetailsGrp.ServiceMatchedInfoGroup();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceCoverageInfoGrp.ServiceCovInfoGrp }
     * 
     */
    public FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceCoverageInfoGrp.ServiceCovInfoGrp createFareMasterPricerCalendarReplyServiceFeesGrpServiceCoverageInfoGrpServiceCovInfoGrp() {
        return new FareMasterPricerCalendarReply.ServiceFeesGrp.ServiceCoverageInfoGrp.ServiceCovInfoGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.OtherSolutions.AmtGroup }
     * 
     */
    public FareMasterPricerCalendarReply.OtherSolutions.AmtGroup createFareMasterPricerCalendarReplyOtherSolutionsAmtGroup() {
        return new FareMasterPricerCalendarReply.OtherSolutions.AmtGroup();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.OtherSolutions.PsgInfo }
     * 
     */
    public FareMasterPricerCalendarReply.OtherSolutions.PsgInfo createFareMasterPricerCalendarReplyOtherSolutionsPsgInfo() {
        return new FareMasterPricerCalendarReply.OtherSolutions.PsgInfo();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.RecommandationSegmentsFareDetails }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.RecommandationSegmentsFareDetails createFareMasterPricerCalendarReplyRecommendationRecommandationSegmentsFareDetails() {
        return new FareMasterPricerCalendarReply.Recommendation.RecommandationSegmentsFareDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails.FareContextDetails.CnxContextDetails }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails.FareContextDetails.CnxContextDetails createFareMasterPricerCalendarReplyRecommendationSpecificRecDetailsSpecificProductDetailsFareContextDetailsCnxContextDetails() {
        return new FareMasterPricerCalendarReply.Recommendation.SpecificRecDetails.SpecificProductDetails.FareContextDetails.CnxContextDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.Fare }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.Fare createFareMasterPricerCalendarReplyRecommendationPaxFareProductFare() {
        return new FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.Fare();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.FareDetails.GroupOfFares }
     * 
     */
    public FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.FareDetails.GroupOfFares createFareMasterPricerCalendarReplyRecommendationPaxFareProductFareDetailsGroupOfFares() {
        return new FareMasterPricerCalendarReply.Recommendation.PaxFareProduct.FareDetails.GroupOfFares();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.FlightIndex.GroupOfFlights.FlightDetails }
     * 
     */
    public FareMasterPricerCalendarReply.FlightIndex.GroupOfFlights.FlightDetails createFareMasterPricerCalendarReplyFlightIndexGroupOfFlightsFlightDetails() {
        return new FareMasterPricerCalendarReply.FlightIndex.GroupOfFlights.FlightDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.AmountInfoPerPax.AmountsPerSgt }
     * 
     */
    public FareMasterPricerCalendarReply.AmountInfoPerPax.AmountsPerSgt createFareMasterPricerCalendarReplyAmountInfoPerPaxAmountsPerSgt() {
        return new FareMasterPricerCalendarReply.AmountInfoPerPax.AmountsPerSgt();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendarReply.AmountInfoForAllPax.AmountsPerSgt }
     * 
     */
    public FareMasterPricerCalendarReply.AmountInfoForAllPax.AmountsPerSgt createFareMasterPricerCalendarReplyAmountInfoForAllPaxAmountsPerSgt() {
        return new FareMasterPricerCalendarReply.AmountInfoForAllPax.AmountsPerSgt();
    }

}
