
package com.amadeus.xml.fmpcar_14_3_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify the details for making a selection.
 * 
 * <p>Java class for SelectionDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelectionDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="carrierFeeDetails" type="{http://xml.amadeus.com/FMPCAR_14_3_1A}SelectionDetailsInformationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelectionDetailsType", propOrder = {
    "carrierFeeDetails"
})
public class SelectionDetailsType {

    @XmlElement(required = true)
    protected SelectionDetailsInformationType carrierFeeDetails;

    /**
     * Gets the value of the carrierFeeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SelectionDetailsInformationType }
     *     
     */
    public SelectionDetailsInformationType getCarrierFeeDetails() {
        return carrierFeeDetails;
    }

    /**
     * Sets the value of the carrierFeeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectionDetailsInformationType }
     *     
     */
    public void setCarrierFeeDetails(SelectionDetailsInformationType value) {
        this.carrierFeeDetails = value;
    }

}
