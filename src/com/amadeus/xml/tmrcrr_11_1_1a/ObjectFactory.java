
package com.amadeus.xml.tmrcrr_11_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.tmrcrr_11_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.tmrcrr_11_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MiniRuleGetFromPricingReply }
     * 
     */
    public MiniRuleGetFromPricingReply createMiniRuleGetFromPricingReply() {
        return new MiniRuleGetFromPricingReply();
    }

    /**
     * Create an instance of {@link MiniRulesRegulPropertiesType }
     * 
     */
    public MiniRulesRegulPropertiesType createMiniRulesRegulPropertiesType() {
        return new MiniRulesRegulPropertiesType();
    }

    /**
     * Create an instance of {@link MiniRuleGetFromPricingReply.MnrByFareRecommendation }
     * 
     */
    public MiniRuleGetFromPricingReply.MnrByFareRecommendation createMiniRuleGetFromPricingReplyMnrByFareRecommendation() {
        return new MiniRuleGetFromPricingReply.MnrByFareRecommendation();
    }

    /**
     * Create an instance of {@link ResponseAnalysisDetailsType }
     * 
     */
    public ResponseAnalysisDetailsType createResponseAnalysisDetailsType() {
        return new ResponseAnalysisDetailsType();
    }

    /**
     * Create an instance of {@link ErrorGroupType }
     * 
     */
    public ErrorGroupType createErrorGroupType() {
        return new ErrorGroupType();
    }

    /**
     * Create an instance of {@link AdditionalFareQualifierDetailsType }
     * 
     */
    public AdditionalFareQualifierDetailsType createAdditionalFareQualifierDetailsType() {
        return new AdditionalFareQualifierDetailsType();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailType }
     * 
     */
    public ApplicationErrorDetailType createApplicationErrorDetailType() {
        return new ApplicationErrorDetailType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType }
     * 
     */
    public ApplicationErrorInformationType createApplicationErrorInformationType() {
        return new ApplicationErrorInformationType();
    }

    /**
     * Create an instance of {@link CategDescrType }
     * 
     */
    public CategDescrType createCategDescrType() {
        return new CategDescrType();
    }

    /**
     * Create an instance of {@link CategoryDescriptionType }
     * 
     */
    public CategoryDescriptionType createCategoryDescriptionType() {
        return new CategoryDescriptionType();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsType }
     * 
     */
    public DateAndTimeDetailsType createDateAndTimeDetailsType() {
        return new DateAndTimeDetailsType();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationType }
     * 
     */
    public DateAndTimeInformationType createDateAndTimeInformationType() {
        return new DateAndTimeInformationType();
    }

    /**
     * Create an instance of {@link ElementManagementSegmentType }
     * 
     */
    public ElementManagementSegmentType createElementManagementSegmentType() {
        return new ElementManagementSegmentType();
    }

    /**
     * Create an instance of {@link FareQualifierDetailsType }
     * 
     */
    public FareQualifierDetailsType createFareQualifierDetailsType() {
        return new FareQualifierDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType }
     * 
     */
    public ItemReferencesAndVersionsType createItemReferencesAndVersionsType() {
        return new ItemReferencesAndVersionsType();
    }

    /**
     * Create an instance of {@link LocationIdentificationBatchType }
     * 
     */
    public LocationIdentificationBatchType createLocationIdentificationBatchType() {
        return new LocationIdentificationBatchType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType }
     * 
     */
    public MonetaryInformationDetailsType createMonetaryInformationDetailsType() {
        return new MonetaryInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link NumberOfUnitDetailsType }
     * 
     */
    public NumberOfUnitDetailsType createNumberOfUnitDetailsType() {
        return new NumberOfUnitDetailsType();
    }

    /**
     * Create an instance of {@link NumberOfUnitsType }
     * 
     */
    public NumberOfUnitsType createNumberOfUnitsType() {
        return new NumberOfUnitsType();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI }
     * 
     */
    public OriginAndDestinationDetailsTypeI createOriginAndDestinationDetailsTypeI() {
        return new OriginAndDestinationDetailsTypeI();
    }

    /**
     * Create an instance of {@link PlaceLocationIdentificationType }
     * 
     */
    public PlaceLocationIdentificationType createPlaceLocationIdentificationType() {
        return new PlaceLocationIdentificationType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType98124S }
     * 
     */
    public ReferenceInfoType98124S createReferenceInfoType98124S() {
        return new ReferenceInfoType98124S();
    }

    /**
     * Create an instance of {@link ReferenceInformationType }
     * 
     */
    public ReferenceInformationType createReferenceInformationType() {
        return new ReferenceInformationType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType152449C }
     * 
     */
    public ReferencingDetailsType152449C createReferencingDetailsType152449C() {
        return new ReferencingDetailsType152449C();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType153016C }
     * 
     */
    public ReferencingDetailsType153016C createReferencingDetailsType153016C() {
        return new ReferencingDetailsType153016C();
    }

    /**
     * Create an instance of {@link StatusDetailsType }
     * 
     */
    public StatusDetailsType createStatusDetailsType() {
        return new StatusDetailsType();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link MiniRulesRegulPropertiesType.MnrFCInfoGrp }
     * 
     */
    public MiniRulesRegulPropertiesType.MnrFCInfoGrp createMiniRulesRegulPropertiesTypeMnrFCInfoGrp() {
        return new MiniRulesRegulPropertiesType.MnrFCInfoGrp();
    }

    /**
     * Create an instance of {@link MiniRulesRegulPropertiesType.MnrDateInfoGrp }
     * 
     */
    public MiniRulesRegulPropertiesType.MnrDateInfoGrp createMiniRulesRegulPropertiesTypeMnrDateInfoGrp() {
        return new MiniRulesRegulPropertiesType.MnrDateInfoGrp();
    }

    /**
     * Create an instance of {@link MiniRulesRegulPropertiesType.MnrMonInfoGrp }
     * 
     */
    public MiniRulesRegulPropertiesType.MnrMonInfoGrp createMiniRulesRegulPropertiesTypeMnrMonInfoGrp() {
        return new MiniRulesRegulPropertiesType.MnrMonInfoGrp();
    }

    /**
     * Create an instance of {@link MiniRulesRegulPropertiesType.MnrRestriAppInfoGrp }
     * 
     */
    public MiniRulesRegulPropertiesType.MnrRestriAppInfoGrp createMiniRulesRegulPropertiesTypeMnrRestriAppInfoGrp() {
        return new MiniRulesRegulPropertiesType.MnrRestriAppInfoGrp();
    }

    /**
     * Create an instance of {@link MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo }
     * 
     */
    public MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo createMiniRuleGetFromPricingReplyMnrByFareRecommendationFareComponentInfo() {
        return new MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo();
    }

}
