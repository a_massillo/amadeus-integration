
package com.amadeus.xml.tmrcrr_11_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseDetails" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ResponseAnalysisDetailsType"/&gt;
 *         &lt;element name="errorWarningGroup" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ErrorGroupType" minOccurs="0"/&gt;
 *         &lt;element name="mnrByFareRecommendation" maxOccurs="99" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="fareRecommendation" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ItemReferencesAndVersionsType"/&gt;
 *                   &lt;element name="paxRef" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ReferenceInformationType" minOccurs="0"/&gt;
 *                   &lt;element name="fareComponentInfo" maxOccurs="16" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="fareQualifierDetails" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}FareQualifierDetailsType"/&gt;
 *                             &lt;element name="fareComponentRef" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ReferenceInfoType"/&gt;
 *                             &lt;element name="originAndDestination" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}OriginAndDestinationDetailsTypeI"/&gt;
 *                             &lt;element name="segmentRefernce" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ElementManagementSegmentType" maxOccurs="99"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="mnrRulesInfoGrp" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}MiniRulesRegulPropertiesType" maxOccurs="600" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseDetails",
    "errorWarningGroup",
    "mnrByFareRecommendation"
})
@XmlRootElement(name = "MiniRule_GetFromPricingReply")
public class MiniRuleGetFromPricingReply {

    @XmlElement(required = true)
    protected ResponseAnalysisDetailsType responseDetails;
    protected ErrorGroupType errorWarningGroup;
    protected List<MiniRuleGetFromPricingReply.MnrByFareRecommendation> mnrByFareRecommendation;

    /**
     * Gets the value of the responseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAnalysisDetailsType }
     *     
     */
    public ResponseAnalysisDetailsType getResponseDetails() {
        return responseDetails;
    }

    /**
     * Sets the value of the responseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAnalysisDetailsType }
     *     
     */
    public void setResponseDetails(ResponseAnalysisDetailsType value) {
        this.responseDetails = value;
    }

    /**
     * Gets the value of the errorWarningGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorGroupType }
     *     
     */
    public ErrorGroupType getErrorWarningGroup() {
        return errorWarningGroup;
    }

    /**
     * Sets the value of the errorWarningGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorGroupType }
     *     
     */
    public void setErrorWarningGroup(ErrorGroupType value) {
        this.errorWarningGroup = value;
    }

    /**
     * Gets the value of the mnrByFareRecommendation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mnrByFareRecommendation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMnrByFareRecommendation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MiniRuleGetFromPricingReply.MnrByFareRecommendation }
     * 
     * 
     */
    public List<MiniRuleGetFromPricingReply.MnrByFareRecommendation> getMnrByFareRecommendation() {
        if (mnrByFareRecommendation == null) {
            mnrByFareRecommendation = new ArrayList<MiniRuleGetFromPricingReply.MnrByFareRecommendation>();
        }
        return this.mnrByFareRecommendation;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="fareRecommendation" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ItemReferencesAndVersionsType"/&gt;
     *         &lt;element name="paxRef" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ReferenceInformationType" minOccurs="0"/&gt;
     *         &lt;element name="fareComponentInfo" maxOccurs="16" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="fareQualifierDetails" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}FareQualifierDetailsType"/&gt;
     *                   &lt;element name="fareComponentRef" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ReferenceInfoType"/&gt;
     *                   &lt;element name="originAndDestination" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}OriginAndDestinationDetailsTypeI"/&gt;
     *                   &lt;element name="segmentRefernce" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ElementManagementSegmentType" maxOccurs="99"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="mnrRulesInfoGrp" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}MiniRulesRegulPropertiesType" maxOccurs="600" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fareRecommendation",
        "paxRef",
        "fareComponentInfo",
        "mnrRulesInfoGrp"
    })
    public static class MnrByFareRecommendation {

        @XmlElement(required = true)
        protected ItemReferencesAndVersionsType fareRecommendation;
        protected ReferenceInformationType paxRef;
        protected List<MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo> fareComponentInfo;
        protected List<MiniRulesRegulPropertiesType> mnrRulesInfoGrp;

        /**
         * Gets the value of the fareRecommendation property.
         * 
         * @return
         *     possible object is
         *     {@link ItemReferencesAndVersionsType }
         *     
         */
        public ItemReferencesAndVersionsType getFareRecommendation() {
            return fareRecommendation;
        }

        /**
         * Sets the value of the fareRecommendation property.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemReferencesAndVersionsType }
         *     
         */
        public void setFareRecommendation(ItemReferencesAndVersionsType value) {
            this.fareRecommendation = value;
        }

        /**
         * Gets the value of the paxRef property.
         * 
         * @return
         *     possible object is
         *     {@link ReferenceInformationType }
         *     
         */
        public ReferenceInformationType getPaxRef() {
            return paxRef;
        }

        /**
         * Sets the value of the paxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReferenceInformationType }
         *     
         */
        public void setPaxRef(ReferenceInformationType value) {
            this.paxRef = value;
        }

        /**
         * Gets the value of the fareComponentInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fareComponentInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFareComponentInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo }
         * 
         * 
         */
        public List<MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo> getFareComponentInfo() {
            if (fareComponentInfo == null) {
                fareComponentInfo = new ArrayList<MiniRuleGetFromPricingReply.MnrByFareRecommendation.FareComponentInfo>();
            }
            return this.fareComponentInfo;
        }

        /**
         * Gets the value of the mnrRulesInfoGrp property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the mnrRulesInfoGrp property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMnrRulesInfoGrp().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MiniRulesRegulPropertiesType }
         * 
         * 
         */
        public List<MiniRulesRegulPropertiesType> getMnrRulesInfoGrp() {
            if (mnrRulesInfoGrp == null) {
                mnrRulesInfoGrp = new ArrayList<MiniRulesRegulPropertiesType>();
            }
            return this.mnrRulesInfoGrp;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="fareQualifierDetails" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}FareQualifierDetailsType"/&gt;
         *         &lt;element name="fareComponentRef" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ReferenceInfoType"/&gt;
         *         &lt;element name="originAndDestination" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}OriginAndDestinationDetailsTypeI"/&gt;
         *         &lt;element name="segmentRefernce" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}ElementManagementSegmentType" maxOccurs="99"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fareQualifierDetails",
            "fareComponentRef",
            "originAndDestination",
            "segmentRefernce"
        })
        public static class FareComponentInfo {

            @XmlElement(required = true)
            protected FareQualifierDetailsType fareQualifierDetails;
            @XmlElement(required = true)
            protected ReferenceInfoType fareComponentRef;
            @XmlElement(required = true)
            protected OriginAndDestinationDetailsTypeI originAndDestination;
            @XmlElement(required = true)
            protected List<ElementManagementSegmentType> segmentRefernce;

            /**
             * Gets the value of the fareQualifierDetails property.
             * 
             * @return
             *     possible object is
             *     {@link FareQualifierDetailsType }
             *     
             */
            public FareQualifierDetailsType getFareQualifierDetails() {
                return fareQualifierDetails;
            }

            /**
             * Sets the value of the fareQualifierDetails property.
             * 
             * @param value
             *     allowed object is
             *     {@link FareQualifierDetailsType }
             *     
             */
            public void setFareQualifierDetails(FareQualifierDetailsType value) {
                this.fareQualifierDetails = value;
            }

            /**
             * Gets the value of the fareComponentRef property.
             * 
             * @return
             *     possible object is
             *     {@link ReferenceInfoType }
             *     
             */
            public ReferenceInfoType getFareComponentRef() {
                return fareComponentRef;
            }

            /**
             * Sets the value of the fareComponentRef property.
             * 
             * @param value
             *     allowed object is
             *     {@link ReferenceInfoType }
             *     
             */
            public void setFareComponentRef(ReferenceInfoType value) {
                this.fareComponentRef = value;
            }

            /**
             * Gets the value of the originAndDestination property.
             * 
             * @return
             *     possible object is
             *     {@link OriginAndDestinationDetailsTypeI }
             *     
             */
            public OriginAndDestinationDetailsTypeI getOriginAndDestination() {
                return originAndDestination;
            }

            /**
             * Sets the value of the originAndDestination property.
             * 
             * @param value
             *     allowed object is
             *     {@link OriginAndDestinationDetailsTypeI }
             *     
             */
            public void setOriginAndDestination(OriginAndDestinationDetailsTypeI value) {
                this.originAndDestination = value;
            }

            /**
             * Gets the value of the segmentRefernce property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the segmentRefernce property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSegmentRefernce().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ElementManagementSegmentType }
             * 
             * 
             */
            public List<ElementManagementSegmentType> getSegmentRefernce() {
                if (segmentRefernce == null) {
                    segmentRefernce = new ArrayList<ElementManagementSegmentType>();
                }
                return this.segmentRefernce;
            }

        }

    }

}
