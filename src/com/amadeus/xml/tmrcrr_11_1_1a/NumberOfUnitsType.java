
package com.amadeus.xml.tmrcrr_11_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify the number of units required.
 * 
 * <p>Java class for NumberOfUnitsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NumberOfUnitsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="quantityDetails" type="{http://xml.amadeus.com/TMRCRR_11_1_1A}NumberOfUnitDetailsType" maxOccurs="9"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NumberOfUnitsType", propOrder = {
    "quantityDetails"
})
public class NumberOfUnitsType {

    @XmlElement(required = true)
    protected List<NumberOfUnitDetailsType> quantityDetails;

    /**
     * Gets the value of the quantityDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quantityDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuantityDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NumberOfUnitDetailsType }
     * 
     * 
     */
    public List<NumberOfUnitDetailsType> getQuantityDetails() {
        if (quantityDetails == null) {
            quantityDetails = new ArrayList<NumberOfUnitDetailsType>();
        }
        return this.quantityDetails;
    }

}
