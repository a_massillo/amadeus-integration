
package com.amadeus.xml.tpcbrr_16_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.tpcbrr_16_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.tpcbrr_16_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply }
     * 
     */
    public FarePricePNRWithBookingClassReply createFarePricePNRWithBookingClassReply() {
        return new FarePricePNRWithBookingClassReply();
    }

    /**
     * Create an instance of {@link CouponDetailsType }
     * 
     */
    public CouponDetailsType createCouponDetailsType() {
        return new CouponDetailsType();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList createFarePricePNRWithBookingClassReplyFareList() {
        return new FarePricePNRWithBookingClassReply.FareList();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.FeeBreakdown }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.FeeBreakdown createFarePricePNRWithBookingClassReplyFareListFeeBreakdown() {
        return new FarePricePNRWithBookingClassReply.FareList.FeeBreakdown();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo createFarePricePNRWithBookingClassReplyFareListAutomaticReissueInfo() {
        return new FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.SegmentInformation }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.SegmentInformation createFarePricePNRWithBookingClassReplyFareListSegmentInformation() {
        return new FarePricePNRWithBookingClassReply.FareList.SegmentInformation();
    }

    /**
     * Create an instance of {@link ErrorGroupType }
     * 
     */
    public ErrorGroupType createErrorGroupType() {
        return new ErrorGroupType();
    }

    /**
     * Create an instance of {@link ReservationControlInformationTypeI }
     * 
     */
    public ReservationControlInformationTypeI createReservationControlInformationTypeI() {
        return new ReservationControlInformationTypeI();
    }

    /**
     * Create an instance of {@link AdditionalFareQualifierDetailsTypeI }
     * 
     */
    public AdditionalFareQualifierDetailsTypeI createAdditionalFareQualifierDetailsTypeI() {
        return new AdditionalFareQualifierDetailsTypeI();
    }

    /**
     * Create an instance of {@link AdditionalProductDetailsTypeI }
     * 
     */
    public AdditionalProductDetailsTypeI createAdditionalProductDetailsTypeI() {
        return new AdditionalProductDetailsTypeI();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailType }
     * 
     */
    public ApplicationErrorDetailType createApplicationErrorDetailType() {
        return new ApplicationErrorDetailType();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailType48648C }
     * 
     */
    public ApplicationErrorDetailType48648C createApplicationErrorDetailType48648C() {
        return new ApplicationErrorDetailType48648C();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType }
     * 
     */
    public ApplicationErrorInformationType createApplicationErrorInformationType() {
        return new ApplicationErrorInformationType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType84497S }
     * 
     */
    public ApplicationErrorInformationType84497S createApplicationErrorInformationType84497S() {
        return new ApplicationErrorInformationType84497S();
    }

    /**
     * Create an instance of {@link BaggageDetailsTypeI }
     * 
     */
    public BaggageDetailsTypeI createBaggageDetailsTypeI() {
        return new BaggageDetailsTypeI();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType }
     * 
     */
    public CodedAttributeInformationType createCodedAttributeInformationType() {
        return new CodedAttributeInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType66047C }
     * 
     */
    public CodedAttributeInformationType66047C createCodedAttributeInformationType66047C() {
        return new CodedAttributeInformationType66047C();
    }

    /**
     * Create an instance of {@link CodedAttributeType }
     * 
     */
    public CodedAttributeType createCodedAttributeType() {
        return new CodedAttributeType();
    }

    /**
     * Create an instance of {@link CodedAttributeType39223S }
     * 
     */
    public CodedAttributeType39223S createCodedAttributeType39223S() {
        return new CodedAttributeType39223S();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI222513C }
     * 
     */
    public CompanyIdentificationTypeI222513C createCompanyIdentificationTypeI222513C() {
        return new CompanyIdentificationTypeI222513C();
    }

    /**
     * Create an instance of {@link ConnectionDetailsTypeI }
     * 
     */
    public ConnectionDetailsTypeI createConnectionDetailsTypeI() {
        return new ConnectionDetailsTypeI();
    }

    /**
     * Create an instance of {@link ConnectionTypeI }
     * 
     */
    public ConnectionTypeI createConnectionTypeI() {
        return new ConnectionTypeI();
    }

    /**
     * Create an instance of {@link ConversionRateDetailsTypeI }
     * 
     */
    public ConversionRateDetailsTypeI createConversionRateDetailsTypeI() {
        return new ConversionRateDetailsTypeI();
    }

    /**
     * Create an instance of {@link ConversionRateTypeI }
     * 
     */
    public ConversionRateTypeI createConversionRateTypeI() {
        return new ConversionRateTypeI();
    }

    /**
     * Create an instance of {@link CorporateFareIdentifiersTypeI }
     * 
     */
    public CorporateFareIdentifiersTypeI createCorporateFareIdentifiersTypeI() {
        return new CorporateFareIdentifiersTypeI();
    }

    /**
     * Create an instance of {@link CorporateFareInformationType }
     * 
     */
    public CorporateFareInformationType createCorporateFareInformationType() {
        return new CorporateFareInformationType();
    }

    /**
     * Create an instance of {@link CouponInformationDetailsTypeI }
     * 
     */
    public CouponInformationDetailsTypeI createCouponInformationDetailsTypeI() {
        return new CouponInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link CouponInformationTypeI }
     * 
     */
    public CouponInformationTypeI createCouponInformationTypeI() {
        return new CouponInformationTypeI();
    }

    /**
     * Create an instance of {@link DataInformationTypeI }
     * 
     */
    public DataInformationTypeI createDataInformationTypeI() {
        return new DataInformationTypeI();
    }

    /**
     * Create an instance of {@link DataTypeInformationTypeI }
     * 
     */
    public DataTypeInformationTypeI createDataTypeInformationTypeI() {
        return new DataTypeInformationTypeI();
    }

    /**
     * Create an instance of {@link DiscountAndPenaltyInformationTypeI }
     * 
     */
    public DiscountAndPenaltyInformationTypeI createDiscountAndPenaltyInformationTypeI() {
        return new DiscountAndPenaltyInformationTypeI();
    }

    /**
     * Create an instance of {@link DiscountAndPenaltyInformationTypeI6128S }
     * 
     */
    public DiscountAndPenaltyInformationTypeI6128S createDiscountAndPenaltyInformationTypeI6128S() {
        return new DiscountAndPenaltyInformationTypeI6128S();
    }

    /**
     * Create an instance of {@link DiscountPenaltyInformationTypeI }
     * 
     */
    public DiscountPenaltyInformationTypeI createDiscountPenaltyInformationTypeI() {
        return new DiscountPenaltyInformationTypeI();
    }

    /**
     * Create an instance of {@link DiscountPenaltyInformationType }
     * 
     */
    public DiscountPenaltyInformationType createDiscountPenaltyInformationType() {
        return new DiscountPenaltyInformationType();
    }

    /**
     * Create an instance of {@link DiscountPenaltyMonetaryInformationTypeI }
     * 
     */
    public DiscountPenaltyMonetaryInformationTypeI createDiscountPenaltyMonetaryInformationTypeI() {
        return new DiscountPenaltyMonetaryInformationTypeI();
    }

    /**
     * Create an instance of {@link DiscountPenaltyMonetaryInformationTypeI29792C }
     * 
     */
    public DiscountPenaltyMonetaryInformationTypeI29792C createDiscountPenaltyMonetaryInformationTypeI29792C() {
        return new DiscountPenaltyMonetaryInformationTypeI29792C();
    }

    /**
     * Create an instance of {@link DummySegmentTypeI }
     * 
     */
    public DummySegmentTypeI createDummySegmentTypeI() {
        return new DummySegmentTypeI();
    }

    /**
     * Create an instance of {@link DutyTaxFeeAccountDetailTypeU }
     * 
     */
    public DutyTaxFeeAccountDetailTypeU createDutyTaxFeeAccountDetailTypeU() {
        return new DutyTaxFeeAccountDetailTypeU();
    }

    /**
     * Create an instance of {@link DutyTaxFeeDetailsTypeU }
     * 
     */
    public DutyTaxFeeDetailsTypeU createDutyTaxFeeDetailsTypeU() {
        return new DutyTaxFeeDetailsTypeU();
    }

    /**
     * Create an instance of {@link DutyTaxFeeDetailsType }
     * 
     */
    public DutyTaxFeeDetailsType createDutyTaxFeeDetailsType() {
        return new DutyTaxFeeDetailsType();
    }

    /**
     * Create an instance of {@link DutyTaxFeeTypeDetailsTypeU }
     * 
     */
    public DutyTaxFeeTypeDetailsTypeU createDutyTaxFeeTypeDetailsTypeU() {
        return new DutyTaxFeeTypeDetailsTypeU();
    }

    /**
     * Create an instance of {@link ExcessBaggageTypeI }
     * 
     */
    public ExcessBaggageTypeI createExcessBaggageTypeI() {
        return new ExcessBaggageTypeI();
    }

    /**
     * Create an instance of {@link FareComponentDetailsType }
     * 
     */
    public FareComponentDetailsType createFareComponentDetailsType() {
        return new FareComponentDetailsType();
    }

    /**
     * Create an instance of {@link FareDetailsType }
     * 
     */
    public FareDetailsType createFareDetailsType() {
        return new FareDetailsType();
    }

    /**
     * Create an instance of {@link FareFamilyDetailsType }
     * 
     */
    public FareFamilyDetailsType createFareFamilyDetailsType() {
        return new FareFamilyDetailsType();
    }

    /**
     * Create an instance of {@link FareFamilyType }
     * 
     */
    public FareFamilyType createFareFamilyType() {
        return new FareFamilyType();
    }

    /**
     * Create an instance of {@link FareInformationType }
     * 
     */
    public FareInformationType createFareInformationType() {
        return new FareInformationType();
    }

    /**
     * Create an instance of {@link FareQualifierDetailsTypeI }
     * 
     */
    public FareQualifierDetailsTypeI createFareQualifierDetailsTypeI() {
        return new FareQualifierDetailsTypeI();
    }

    /**
     * Create an instance of {@link FareQualifierDetailsType }
     * 
     */
    public FareQualifierDetailsType createFareQualifierDetailsType() {
        return new FareQualifierDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link FreeTextQualificationTypeI }
     * 
     */
    public FreeTextQualificationTypeI createFreeTextQualificationTypeI() {
        return new FreeTextQualificationTypeI();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextTypeI }
     * 
     */
    public InteractiveFreeTextTypeI createInteractiveFreeTextTypeI() {
        return new InteractiveFreeTextTypeI();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextTypeI6759S }
     * 
     */
    public InteractiveFreeTextTypeI6759S createInteractiveFreeTextTypeI6759S() {
        return new InteractiveFreeTextTypeI6759S();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType }
     * 
     */
    public ItemNumberIdentificationType createItemNumberIdentificationType() {
        return new ItemNumberIdentificationType();
    }

    /**
     * Create an instance of {@link ItemNumberType }
     * 
     */
    public ItemNumberType createItemNumberType() {
        return new ItemNumberType();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType }
     * 
     */
    public ItemReferencesAndVersionsType createItemReferencesAndVersionsType() {
        return new ItemReferencesAndVersionsType();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType94584S }
     * 
     */
    public ItemReferencesAndVersionsType94584S createItemReferencesAndVersionsType94584S() {
        return new ItemReferencesAndVersionsType94584S();
    }

    /**
     * Create an instance of {@link LocationIdentificationBatchType }
     * 
     */
    public LocationIdentificationBatchType createLocationIdentificationBatchType() {
        return new LocationIdentificationBatchType();
    }

    /**
     * Create an instance of {@link LocationTypeI }
     * 
     */
    public LocationTypeI createLocationTypeI() {
        return new LocationTypeI();
    }

    /**
     * Create an instance of {@link LocationTypeI47688C }
     * 
     */
    public LocationTypeI47688C createLocationTypeI47688C() {
        return new LocationTypeI47688C();
    }

    /**
     * Create an instance of {@link MileageTimeDetailsTypeI }
     * 
     */
    public MileageTimeDetailsTypeI createMileageTimeDetailsTypeI() {
        return new MileageTimeDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI }
     * 
     */
    public MonetaryInformationDetailsTypeI createMonetaryInformationDetailsTypeI() {
        return new MonetaryInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI37257C }
     * 
     */
    public MonetaryInformationDetailsTypeI37257C createMonetaryInformationDetailsTypeI37257C() {
        return new MonetaryInformationDetailsTypeI37257C();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI63727C }
     * 
     */
    public MonetaryInformationDetailsTypeI63727C createMonetaryInformationDetailsTypeI63727C() {
        return new MonetaryInformationDetailsTypeI63727C();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType }
     * 
     */
    public MonetaryInformationDetailsType createMonetaryInformationDetailsType() {
        return new MonetaryInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType262564C }
     * 
     */
    public MonetaryInformationDetailsType262564C createMonetaryInformationDetailsType262564C() {
        return new MonetaryInformationDetailsType262564C();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType270392C }
     * 
     */
    public MonetaryInformationDetailsType270392C createMonetaryInformationDetailsType270392C() {
        return new MonetaryInformationDetailsType270392C();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI }
     * 
     */
    public MonetaryInformationTypeI createMonetaryInformationTypeI() {
        return new MonetaryInformationTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI20897S }
     * 
     */
    public MonetaryInformationTypeI20897S createMonetaryInformationTypeI20897S() {
        return new MonetaryInformationTypeI20897S();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI39230S }
     * 
     */
    public MonetaryInformationTypeI39230S createMonetaryInformationTypeI39230S() {
        return new MonetaryInformationTypeI39230S();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link MonetaryInformationType198917S }
     * 
     */
    public MonetaryInformationType198917S createMonetaryInformationType198917S() {
        return new MonetaryInformationType198917S();
    }

    /**
     * Create an instance of {@link MonetaryInformationType198918S }
     * 
     */
    public MonetaryInformationType198918S createMonetaryInformationType198918S() {
        return new MonetaryInformationType198918S();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI }
     * 
     */
    public OriginAndDestinationDetailsTypeI createOriginAndDestinationDetailsTypeI() {
        return new OriginAndDestinationDetailsTypeI();
    }

    /**
     * Create an instance of {@link PlaceLocationIdentificationType }
     * 
     */
    public PlaceLocationIdentificationType createPlaceLocationIdentificationType() {
        return new PlaceLocationIdentificationType();
    }

    /**
     * Create an instance of {@link PricingOrTicketingSubsequentType }
     * 
     */
    public PricingOrTicketingSubsequentType createPricingOrTicketingSubsequentType() {
        return new PricingOrTicketingSubsequentType();
    }

    /**
     * Create an instance of {@link PricingTicketingSubsequentTypeI }
     * 
     */
    public PricingTicketingSubsequentTypeI createPricingTicketingSubsequentTypeI() {
        return new PricingTicketingSubsequentTypeI();
    }

    /**
     * Create an instance of {@link ProductDetailsTypeI }
     * 
     */
    public ProductDetailsTypeI createProductDetailsTypeI() {
        return new ProductDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductIdentificationDetailsTypeI }
     * 
     */
    public ProductIdentificationDetailsTypeI createProductIdentificationDetailsTypeI() {
        return new ProductIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductInformationTypeI }
     * 
     */
    public ProductInformationTypeI createProductInformationTypeI() {
        return new ProductInformationTypeI();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsType }
     * 
     */
    public ProductTypeDetailsType createProductTypeDetailsType() {
        return new ProductTypeDetailsType();
    }

    /**
     * Create an instance of {@link RateTariffClassInformationTypeI }
     * 
     */
    public RateTariffClassInformationTypeI createRateTariffClassInformationTypeI() {
        return new RateTariffClassInformationTypeI();
    }

    /**
     * Create an instance of {@link RateTariffClassInformationType }
     * 
     */
    public RateTariffClassInformationType createRateTariffClassInformationType() {
        return new RateTariffClassInformationType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferenceInformationTypeI }
     * 
     */
    public ReferenceInformationTypeI createReferenceInformationTypeI() {
        return new ReferenceInformationTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI }
     * 
     */
    public ReferencingDetailsTypeI createReferencingDetailsTypeI() {
        return new ReferencingDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsTypeI }
     * 
     */
    public ReservationControlInformationDetailsTypeI createReservationControlInformationDetailsTypeI() {
        return new ReservationControlInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsInformationTypeI }
     * 
     */
    public SelectionDetailsInformationTypeI createSelectionDetailsInformationTypeI() {
        return new SelectionDetailsInformationTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsTypeI }
     * 
     */
    public SelectionDetailsTypeI createSelectionDetailsTypeI() {
        return new SelectionDetailsTypeI();
    }

    /**
     * Create an instance of {@link SpecificDataInformationTypeI }
     * 
     */
    public SpecificDataInformationTypeI createSpecificDataInformationTypeI() {
        return new SpecificDataInformationTypeI();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType }
     * 
     */
    public StructuredDateTimeInformationType createStructuredDateTimeInformationType() {
        return new StructuredDateTimeInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType199533S }
     * 
     */
    public StructuredDateTimeInformationType199533S createStructuredDateTimeInformationType199533S() {
        return new StructuredDateTimeInformationType199533S();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType277474C }
     * 
     */
    public StructuredDateTimeType277474C createStructuredDateTimeType277474C() {
        return new StructuredDateTimeType277474C();
    }

    /**
     * Create an instance of {@link TaxDetailsTypeI }
     * 
     */
    public TaxDetailsTypeI createTaxDetailsTypeI() {
        return new TaxDetailsTypeI();
    }

    /**
     * Create an instance of {@link TaxDetailsType }
     * 
     */
    public TaxDetailsType createTaxDetailsType() {
        return new TaxDetailsType();
    }

    /**
     * Create an instance of {@link TaxTypeI }
     * 
     */
    public TaxTypeI createTaxTypeI() {
        return new TaxTypeI();
    }

    /**
     * Create an instance of {@link TaxType }
     * 
     */
    public TaxType createTaxType() {
        return new TaxType();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

    /**
     * Create an instance of {@link TransportIdentifierType }
     * 
     */
    public TransportIdentifierType createTransportIdentifierType() {
        return new TransportIdentifierType();
    }

    /**
     * Create an instance of {@link TransportIdentifierType156079S }
     * 
     */
    public TransportIdentifierType156079S createTransportIdentifierType156079S() {
        return new TransportIdentifierType156079S();
    }

    /**
     * Create an instance of {@link TravelProductInformationTypeI }
     * 
     */
    public TravelProductInformationTypeI createTravelProductInformationTypeI() {
        return new TravelProductInformationTypeI();
    }

    /**
     * Create an instance of {@link TravelProductInformationTypeI26322S }
     * 
     */
    public TravelProductInformationTypeI26322S createTravelProductInformationTypeI26322S() {
        return new TravelProductInformationTypeI26322S();
    }

    /**
     * Create an instance of {@link TravelProductInformationType }
     * 
     */
    public TravelProductInformationType createTravelProductInformationType() {
        return new TravelProductInformationType();
    }

    /**
     * Create an instance of {@link UniqueIdDescriptionType }
     * 
     */
    public UniqueIdDescriptionType createUniqueIdDescriptionType() {
        return new UniqueIdDescriptionType();
    }

    /**
     * Create an instance of {@link CouponDetailsType.CouponTaxDetailsGroup }
     * 
     */
    public CouponDetailsType.CouponTaxDetailsGroup createCouponDetailsTypeCouponTaxDetailsGroup() {
        return new CouponDetailsType.CouponTaxDetailsGroup();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.TaxInformation }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.TaxInformation createFarePricePNRWithBookingClassReplyFareListTaxInformation() {
        return new FarePricePNRWithBookingClassReply.FareList.TaxInformation();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.PassengerInformation }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.PassengerInformation createFarePricePNRWithBookingClassReplyFareListPassengerInformation() {
        return new FarePricePNRWithBookingClassReply.FareList.PassengerInformation();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.WarningInformation }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.WarningInformation createFarePricePNRWithBookingClassReplyFareListWarningInformation() {
        return new FarePricePNRWithBookingClassReply.FareList.WarningInformation();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.FeeBreakdown.FeeDetails }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.FeeBreakdown.FeeDetails createFarePricePNRWithBookingClassReplyFareListFeeBreakdownFeeDetails() {
        return new FarePricePNRWithBookingClassReply.FareList.FeeBreakdown.FeeDetails();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.PaperCouponRange }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.PaperCouponRange createFarePricePNRWithBookingClassReplyFareListAutomaticReissueInfoPaperCouponRange() {
        return new FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.PaperCouponRange();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.FirstDpiGroup }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.FirstDpiGroup createFarePricePNRWithBookingClassReplyFareListAutomaticReissueInfoFirstDpiGroup() {
        return new FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.FirstDpiGroup();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.SecondDpiGroup }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.SecondDpiGroup createFarePricePNRWithBookingClassReplyFareListAutomaticReissueInfoSecondDpiGroup() {
        return new FarePricePNRWithBookingClassReply.FareList.AutomaticReissueInfo.SecondDpiGroup();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClassReply.FareList.SegmentInformation.CabinGroup }
     * 
     */
    public FarePricePNRWithBookingClassReply.FareList.SegmentInformation.CabinGroup createFarePricePNRWithBookingClassReplyFareListSegmentInformationCabinGroup() {
        return new FarePricePNRWithBookingClassReply.FareList.SegmentInformation.CabinGroup();
    }

}
