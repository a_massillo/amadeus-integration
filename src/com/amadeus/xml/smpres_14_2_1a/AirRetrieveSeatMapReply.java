
package com.amadeus.xml.smpres_14_2_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ResponseAnalysisDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="errorInformation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ErrorInformationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="warningInformation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}WarningInformationType" minOccurs="0"/&gt;
 *         &lt;element name="seatRequestParameters" type="{http://xml.amadeus.com/SMPRES_14_2_1A}SeatRequestParametersTypeI" minOccurs="0"/&gt;
 *         &lt;element name="seatmapInformation" maxOccurs="5" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="flightDateInformation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TravelProductInformationTypeI"/&gt;
 *                   &lt;element name="seatmapErrorInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ErrorInformationTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="seatmapWarningInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}WarningInformationTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="additionalProductInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}AdditionalProductDetailsTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="equipmentInformation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}EquipmentInfoTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="cabin" maxOccurs="9" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="compartmentDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinDetailsTypeI"/&gt;
 *                             &lt;element name="cabinFacilities" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinFacilitiesTypeI" maxOccurs="9" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="row" maxOccurs="999" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsType"/&gt;
 *                             &lt;element name="cabinFacility" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinFacilitiesTypeI" maxOccurs="9" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="freeText" type="{http://xml.amadeus.com/SMPRES_14_2_1A}InteractiveFreeTextTypeI" maxOccurs="2" minOccurs="0"/&gt;
 *                   &lt;element name="commercialDescription" type="{http://xml.amadeus.com/SMPRES_14_2_1A}FreeTextInformationType" minOccurs="0"/&gt;
 *                   &lt;element name="customerCentricData" maxOccurs="99" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="dummy" type="{http://xml.amadeus.com/SMPRES_14_2_1A}DummySegmentTypeI"/&gt;
 *                             &lt;element name="travellerDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TravellerInformationTypeI" maxOccurs="99" minOccurs="0"/&gt;
 *                             &lt;element name="seatPrice" maxOccurs="999"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="seatPrice" type="{http://xml.amadeus.com/SMPRES_14_2_1A}MonetaryInformationTypeI"/&gt;
 *                                       &lt;element name="taxDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TaxTypeI" minOccurs="0"/&gt;
 *                                       &lt;element name="priceRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType" minOccurs="0"/&gt;
 *                                       &lt;element name="additionalInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CodedAttributeType" minOccurs="0"/&gt;
 *                                       &lt;element name="enrichedContent" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CommunicationContactType" maxOccurs="5" minOccurs="0"/&gt;
 *                                       &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsTypeI" maxOccurs="999"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="catalogData" maxOccurs="999" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="catalogueRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType"/&gt;
 *                             &lt;element name="catalogueDescription" type="{http://xml.amadeus.com/SMPRES_14_2_1A}FreeTextInformationType" maxOccurs="10" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseDetails",
    "errorInformation",
    "warningInformation",
    "seatRequestParameters",
    "seatmapInformation"
})
@XmlRootElement(name = "Air_RetrieveSeatMapReply")
public class AirRetrieveSeatMapReply {

    protected ResponseAnalysisDetailsTypeI responseDetails;
    protected ErrorInformationTypeI errorInformation;
    protected WarningInformationType warningInformation;
    protected SeatRequestParametersTypeI seatRequestParameters;
    protected List<AirRetrieveSeatMapReply.SeatmapInformation> seatmapInformation;

    /**
     * Gets the value of the responseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAnalysisDetailsTypeI }
     *     
     */
    public ResponseAnalysisDetailsTypeI getResponseDetails() {
        return responseDetails;
    }

    /**
     * Sets the value of the responseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAnalysisDetailsTypeI }
     *     
     */
    public void setResponseDetails(ResponseAnalysisDetailsTypeI value) {
        this.responseDetails = value;
    }

    /**
     * Gets the value of the errorInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorInformationTypeI }
     *     
     */
    public ErrorInformationTypeI getErrorInformation() {
        return errorInformation;
    }

    /**
     * Sets the value of the errorInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorInformationTypeI }
     *     
     */
    public void setErrorInformation(ErrorInformationTypeI value) {
        this.errorInformation = value;
    }

    /**
     * Gets the value of the warningInformation property.
     * 
     * @return
     *     possible object is
     *     {@link WarningInformationType }
     *     
     */
    public WarningInformationType getWarningInformation() {
        return warningInformation;
    }

    /**
     * Sets the value of the warningInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningInformationType }
     *     
     */
    public void setWarningInformation(WarningInformationType value) {
        this.warningInformation = value;
    }

    /**
     * Gets the value of the seatRequestParameters property.
     * 
     * @return
     *     possible object is
     *     {@link SeatRequestParametersTypeI }
     *     
     */
    public SeatRequestParametersTypeI getSeatRequestParameters() {
        return seatRequestParameters;
    }

    /**
     * Sets the value of the seatRequestParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatRequestParametersTypeI }
     *     
     */
    public void setSeatRequestParameters(SeatRequestParametersTypeI value) {
        this.seatRequestParameters = value;
    }

    /**
     * Gets the value of the seatmapInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seatmapInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeatmapInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirRetrieveSeatMapReply.SeatmapInformation }
     * 
     * 
     */
    public List<AirRetrieveSeatMapReply.SeatmapInformation> getSeatmapInformation() {
        if (seatmapInformation == null) {
            seatmapInformation = new ArrayList<AirRetrieveSeatMapReply.SeatmapInformation>();
        }
        return this.seatmapInformation;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="flightDateInformation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TravelProductInformationTypeI"/&gt;
     *         &lt;element name="seatmapErrorInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ErrorInformationTypeI" minOccurs="0"/&gt;
     *         &lt;element name="seatmapWarningInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}WarningInformationTypeI" minOccurs="0"/&gt;
     *         &lt;element name="additionalProductInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}AdditionalProductDetailsTypeI" minOccurs="0"/&gt;
     *         &lt;element name="equipmentInformation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}EquipmentInfoTypeI" minOccurs="0"/&gt;
     *         &lt;element name="cabin" maxOccurs="9" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="compartmentDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinDetailsTypeI"/&gt;
     *                   &lt;element name="cabinFacilities" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinFacilitiesTypeI" maxOccurs="9" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="row" maxOccurs="999" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsType"/&gt;
     *                   &lt;element name="cabinFacility" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinFacilitiesTypeI" maxOccurs="9" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="freeText" type="{http://xml.amadeus.com/SMPRES_14_2_1A}InteractiveFreeTextTypeI" maxOccurs="2" minOccurs="0"/&gt;
     *         &lt;element name="commercialDescription" type="{http://xml.amadeus.com/SMPRES_14_2_1A}FreeTextInformationType" minOccurs="0"/&gt;
     *         &lt;element name="customerCentricData" maxOccurs="99" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="dummy" type="{http://xml.amadeus.com/SMPRES_14_2_1A}DummySegmentTypeI"/&gt;
     *                   &lt;element name="travellerDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TravellerInformationTypeI" maxOccurs="99" minOccurs="0"/&gt;
     *                   &lt;element name="seatPrice" maxOccurs="999"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="seatPrice" type="{http://xml.amadeus.com/SMPRES_14_2_1A}MonetaryInformationTypeI"/&gt;
     *                             &lt;element name="taxDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TaxTypeI" minOccurs="0"/&gt;
     *                             &lt;element name="priceRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType" minOccurs="0"/&gt;
     *                             &lt;element name="additionalInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CodedAttributeType" minOccurs="0"/&gt;
     *                             &lt;element name="enrichedContent" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CommunicationContactType" maxOccurs="5" minOccurs="0"/&gt;
     *                             &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsTypeI" maxOccurs="999"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="catalogData" maxOccurs="999" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="catalogueRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType"/&gt;
     *                   &lt;element name="catalogueDescription" type="{http://xml.amadeus.com/SMPRES_14_2_1A}FreeTextInformationType" maxOccurs="10" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightDateInformation",
        "seatmapErrorInfo",
        "seatmapWarningInfo",
        "additionalProductInfo",
        "equipmentInformation",
        "cabin",
        "row",
        "freeText",
        "commercialDescription",
        "customerCentricData",
        "catalogData"
    })
    public static class SeatmapInformation {

        @XmlElement(required = true)
        protected TravelProductInformationTypeI flightDateInformation;
        protected ErrorInformationTypeI seatmapErrorInfo;
        protected WarningInformationTypeI seatmapWarningInfo;
        protected AdditionalProductDetailsTypeI additionalProductInfo;
        protected EquipmentInfoTypeI equipmentInformation;
        protected List<AirRetrieveSeatMapReply.SeatmapInformation.Cabin> cabin;
        protected List<AirRetrieveSeatMapReply.SeatmapInformation.Row> row;
        protected List<InteractiveFreeTextTypeI> freeText;
        protected FreeTextInformationType commercialDescription;
        protected List<AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData> customerCentricData;
        protected List<AirRetrieveSeatMapReply.SeatmapInformation.CatalogData> catalogData;

        /**
         * Gets the value of the flightDateInformation property.
         * 
         * @return
         *     possible object is
         *     {@link TravelProductInformationTypeI }
         *     
         */
        public TravelProductInformationTypeI getFlightDateInformation() {
            return flightDateInformation;
        }

        /**
         * Sets the value of the flightDateInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link TravelProductInformationTypeI }
         *     
         */
        public void setFlightDateInformation(TravelProductInformationTypeI value) {
            this.flightDateInformation = value;
        }

        /**
         * Gets the value of the seatmapErrorInfo property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorInformationTypeI }
         *     
         */
        public ErrorInformationTypeI getSeatmapErrorInfo() {
            return seatmapErrorInfo;
        }

        /**
         * Sets the value of the seatmapErrorInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorInformationTypeI }
         *     
         */
        public void setSeatmapErrorInfo(ErrorInformationTypeI value) {
            this.seatmapErrorInfo = value;
        }

        /**
         * Gets the value of the seatmapWarningInfo property.
         * 
         * @return
         *     possible object is
         *     {@link WarningInformationTypeI }
         *     
         */
        public WarningInformationTypeI getSeatmapWarningInfo() {
            return seatmapWarningInfo;
        }

        /**
         * Sets the value of the seatmapWarningInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link WarningInformationTypeI }
         *     
         */
        public void setSeatmapWarningInfo(WarningInformationTypeI value) {
            this.seatmapWarningInfo = value;
        }

        /**
         * Gets the value of the additionalProductInfo property.
         * 
         * @return
         *     possible object is
         *     {@link AdditionalProductDetailsTypeI }
         *     
         */
        public AdditionalProductDetailsTypeI getAdditionalProductInfo() {
            return additionalProductInfo;
        }

        /**
         * Sets the value of the additionalProductInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link AdditionalProductDetailsTypeI }
         *     
         */
        public void setAdditionalProductInfo(AdditionalProductDetailsTypeI value) {
            this.additionalProductInfo = value;
        }

        /**
         * Gets the value of the equipmentInformation property.
         * 
         * @return
         *     possible object is
         *     {@link EquipmentInfoTypeI }
         *     
         */
        public EquipmentInfoTypeI getEquipmentInformation() {
            return equipmentInformation;
        }

        /**
         * Sets the value of the equipmentInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link EquipmentInfoTypeI }
         *     
         */
        public void setEquipmentInformation(EquipmentInfoTypeI value) {
            this.equipmentInformation = value;
        }

        /**
         * Gets the value of the cabin property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cabin property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCabin().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AirRetrieveSeatMapReply.SeatmapInformation.Cabin }
         * 
         * 
         */
        public List<AirRetrieveSeatMapReply.SeatmapInformation.Cabin> getCabin() {
            if (cabin == null) {
                cabin = new ArrayList<AirRetrieveSeatMapReply.SeatmapInformation.Cabin>();
            }
            return this.cabin;
        }

        /**
         * Gets the value of the row property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the row property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRow().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AirRetrieveSeatMapReply.SeatmapInformation.Row }
         * 
         * 
         */
        public List<AirRetrieveSeatMapReply.SeatmapInformation.Row> getRow() {
            if (row == null) {
                row = new ArrayList<AirRetrieveSeatMapReply.SeatmapInformation.Row>();
            }
            return this.row;
        }

        /**
         * Gets the value of the freeText property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeText property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeText().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link InteractiveFreeTextTypeI }
         * 
         * 
         */
        public List<InteractiveFreeTextTypeI> getFreeText() {
            if (freeText == null) {
                freeText = new ArrayList<InteractiveFreeTextTypeI>();
            }
            return this.freeText;
        }

        /**
         * Gets the value of the commercialDescription property.
         * 
         * @return
         *     possible object is
         *     {@link FreeTextInformationType }
         *     
         */
        public FreeTextInformationType getCommercialDescription() {
            return commercialDescription;
        }

        /**
         * Sets the value of the commercialDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link FreeTextInformationType }
         *     
         */
        public void setCommercialDescription(FreeTextInformationType value) {
            this.commercialDescription = value;
        }

        /**
         * Gets the value of the customerCentricData property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the customerCentricData property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustomerCentricData().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData }
         * 
         * 
         */
        public List<AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData> getCustomerCentricData() {
            if (customerCentricData == null) {
                customerCentricData = new ArrayList<AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData>();
            }
            return this.customerCentricData;
        }

        /**
         * Gets the value of the catalogData property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the catalogData property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCatalogData().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AirRetrieveSeatMapReply.SeatmapInformation.CatalogData }
         * 
         * 
         */
        public List<AirRetrieveSeatMapReply.SeatmapInformation.CatalogData> getCatalogData() {
            if (catalogData == null) {
                catalogData = new ArrayList<AirRetrieveSeatMapReply.SeatmapInformation.CatalogData>();
            }
            return this.catalogData;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="compartmentDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinDetailsTypeI"/&gt;
         *         &lt;element name="cabinFacilities" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinFacilitiesTypeI" maxOccurs="9" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "compartmentDetails",
            "cabinFacilities"
        })
        public static class Cabin {

            @XmlElement(required = true)
            protected CabinDetailsTypeI compartmentDetails;
            protected List<CabinFacilitiesTypeI> cabinFacilities;

            /**
             * Gets the value of the compartmentDetails property.
             * 
             * @return
             *     possible object is
             *     {@link CabinDetailsTypeI }
             *     
             */
            public CabinDetailsTypeI getCompartmentDetails() {
                return compartmentDetails;
            }

            /**
             * Sets the value of the compartmentDetails property.
             * 
             * @param value
             *     allowed object is
             *     {@link CabinDetailsTypeI }
             *     
             */
            public void setCompartmentDetails(CabinDetailsTypeI value) {
                this.compartmentDetails = value;
            }

            /**
             * Gets the value of the cabinFacilities property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the cabinFacilities property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCabinFacilities().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CabinFacilitiesTypeI }
             * 
             * 
             */
            public List<CabinFacilitiesTypeI> getCabinFacilities() {
                if (cabinFacilities == null) {
                    cabinFacilities = new ArrayList<CabinFacilitiesTypeI>();
                }
                return this.cabinFacilities;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="catalogueRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType"/&gt;
         *         &lt;element name="catalogueDescription" type="{http://xml.amadeus.com/SMPRES_14_2_1A}FreeTextInformationType" maxOccurs="10" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "catalogueRef",
            "catalogueDescription"
        })
        public static class CatalogData {

            @XmlElement(required = true)
            protected ReferenceInfoType catalogueRef;
            protected List<FreeTextInformationType> catalogueDescription;

            /**
             * Gets the value of the catalogueRef property.
             * 
             * @return
             *     possible object is
             *     {@link ReferenceInfoType }
             *     
             */
            public ReferenceInfoType getCatalogueRef() {
                return catalogueRef;
            }

            /**
             * Sets the value of the catalogueRef property.
             * 
             * @param value
             *     allowed object is
             *     {@link ReferenceInfoType }
             *     
             */
            public void setCatalogueRef(ReferenceInfoType value) {
                this.catalogueRef = value;
            }

            /**
             * Gets the value of the catalogueDescription property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the catalogueDescription property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCatalogueDescription().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link FreeTextInformationType }
             * 
             * 
             */
            public List<FreeTextInformationType> getCatalogueDescription() {
                if (catalogueDescription == null) {
                    catalogueDescription = new ArrayList<FreeTextInformationType>();
                }
                return this.catalogueDescription;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="dummy" type="{http://xml.amadeus.com/SMPRES_14_2_1A}DummySegmentTypeI"/&gt;
         *         &lt;element name="travellerDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TravellerInformationTypeI" maxOccurs="99" minOccurs="0"/&gt;
         *         &lt;element name="seatPrice" maxOccurs="999"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="seatPrice" type="{http://xml.amadeus.com/SMPRES_14_2_1A}MonetaryInformationTypeI"/&gt;
         *                   &lt;element name="taxDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TaxTypeI" minOccurs="0"/&gt;
         *                   &lt;element name="priceRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType" minOccurs="0"/&gt;
         *                   &lt;element name="additionalInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CodedAttributeType" minOccurs="0"/&gt;
         *                   &lt;element name="enrichedContent" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CommunicationContactType" maxOccurs="5" minOccurs="0"/&gt;
         *                   &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsTypeI" maxOccurs="999"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "dummy",
            "travellerDetails",
            "seatPrice"
        })
        public static class CustomerCentricData {

            @XmlElement(required = true)
            protected DummySegmentTypeI dummy;
            protected List<TravellerInformationTypeI> travellerDetails;
            @XmlElement(required = true)
            protected List<AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice> seatPrice;

            /**
             * Gets the value of the dummy property.
             * 
             * @return
             *     possible object is
             *     {@link DummySegmentTypeI }
             *     
             */
            public DummySegmentTypeI getDummy() {
                return dummy;
            }

            /**
             * Sets the value of the dummy property.
             * 
             * @param value
             *     allowed object is
             *     {@link DummySegmentTypeI }
             *     
             */
            public void setDummy(DummySegmentTypeI value) {
                this.dummy = value;
            }

            /**
             * Gets the value of the travellerDetails property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the travellerDetails property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTravellerDetails().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TravellerInformationTypeI }
             * 
             * 
             */
            public List<TravellerInformationTypeI> getTravellerDetails() {
                if (travellerDetails == null) {
                    travellerDetails = new ArrayList<TravellerInformationTypeI>();
                }
                return this.travellerDetails;
            }

            /**
             * Gets the value of the seatPrice property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the seatPrice property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSeatPrice().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice }
             * 
             * 
             */
            public List<AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice> getSeatPrice() {
                if (seatPrice == null) {
                    seatPrice = new ArrayList<AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice>();
                }
                return this.seatPrice;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="seatPrice" type="{http://xml.amadeus.com/SMPRES_14_2_1A}MonetaryInformationTypeI"/&gt;
             *         &lt;element name="taxDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}TaxTypeI" minOccurs="0"/&gt;
             *         &lt;element name="priceRef" type="{http://xml.amadeus.com/SMPRES_14_2_1A}ReferenceInfoType" minOccurs="0"/&gt;
             *         &lt;element name="additionalInfo" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CodedAttributeType" minOccurs="0"/&gt;
             *         &lt;element name="enrichedContent" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CommunicationContactType" maxOccurs="5" minOccurs="0"/&gt;
             *         &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsTypeI" maxOccurs="999"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "seatPrice",
                "taxDetails",
                "priceRef",
                "additionalInfo",
                "enrichedContent",
                "rowDetails"
            })
            public static class SeatPrice {

                @XmlElement(required = true)
                protected MonetaryInformationTypeI seatPrice;
                protected TaxTypeI taxDetails;
                protected ReferenceInfoType priceRef;
                protected CodedAttributeType additionalInfo;
                protected List<CommunicationContactType> enrichedContent;
                @XmlElement(required = true)
                protected List<RowDetailsTypeI> rowDetails;

                /**
                 * Gets the value of the seatPrice property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link MonetaryInformationTypeI }
                 *     
                 */
                public MonetaryInformationTypeI getSeatPrice() {
                    return seatPrice;
                }

                /**
                 * Sets the value of the seatPrice property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link MonetaryInformationTypeI }
                 *     
                 */
                public void setSeatPrice(MonetaryInformationTypeI value) {
                    this.seatPrice = value;
                }

                /**
                 * Gets the value of the taxDetails property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link TaxTypeI }
                 *     
                 */
                public TaxTypeI getTaxDetails() {
                    return taxDetails;
                }

                /**
                 * Sets the value of the taxDetails property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TaxTypeI }
                 *     
                 */
                public void setTaxDetails(TaxTypeI value) {
                    this.taxDetails = value;
                }

                /**
                 * Gets the value of the priceRef property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ReferenceInfoType }
                 *     
                 */
                public ReferenceInfoType getPriceRef() {
                    return priceRef;
                }

                /**
                 * Sets the value of the priceRef property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ReferenceInfoType }
                 *     
                 */
                public void setPriceRef(ReferenceInfoType value) {
                    this.priceRef = value;
                }

                /**
                 * Gets the value of the additionalInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodedAttributeType }
                 *     
                 */
                public CodedAttributeType getAdditionalInfo() {
                    return additionalInfo;
                }

                /**
                 * Sets the value of the additionalInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodedAttributeType }
                 *     
                 */
                public void setAdditionalInfo(CodedAttributeType value) {
                    this.additionalInfo = value;
                }

                /**
                 * Gets the value of the enrichedContent property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the enrichedContent property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getEnrichedContent().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link CommunicationContactType }
                 * 
                 * 
                 */
                public List<CommunicationContactType> getEnrichedContent() {
                    if (enrichedContent == null) {
                        enrichedContent = new ArrayList<CommunicationContactType>();
                    }
                    return this.enrichedContent;
                }

                /**
                 * Gets the value of the rowDetails property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the rowDetails property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getRowDetails().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link RowDetailsTypeI }
                 * 
                 * 
                 */
                public List<RowDetailsTypeI> getRowDetails() {
                    if (rowDetails == null) {
                        rowDetails = new ArrayList<RowDetailsTypeI>();
                    }
                    return this.rowDetails;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="rowDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowDetailsType"/&gt;
         *         &lt;element name="cabinFacility" type="{http://xml.amadeus.com/SMPRES_14_2_1A}CabinFacilitiesTypeI" maxOccurs="9" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rowDetails",
            "cabinFacility"
        })
        public static class Row {

            @XmlElement(required = true)
            protected RowDetailsType rowDetails;
            protected List<CabinFacilitiesTypeI> cabinFacility;

            /**
             * Gets the value of the rowDetails property.
             * 
             * @return
             *     possible object is
             *     {@link RowDetailsType }
             *     
             */
            public RowDetailsType getRowDetails() {
                return rowDetails;
            }

            /**
             * Sets the value of the rowDetails property.
             * 
             * @param value
             *     allowed object is
             *     {@link RowDetailsType }
             *     
             */
            public void setRowDetails(RowDetailsType value) {
                this.rowDetails = value;
            }

            /**
             * Gets the value of the cabinFacility property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the cabinFacility property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCabinFacility().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CabinFacilitiesTypeI }
             * 
             * 
             */
            public List<CabinFacilitiesTypeI> getCabinFacility() {
                if (cabinFacility == null) {
                    cabinFacility = new ArrayList<CabinFacilitiesTypeI>();
                }
                return this.cabinFacility;
            }

        }

    }

}
