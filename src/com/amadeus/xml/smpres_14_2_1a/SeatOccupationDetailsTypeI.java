
package com.amadeus.xml.smpres_14_2_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To identify the status of a specific seat.
 * 
 * <p>Java class for SeatOccupationDetailsTypeI complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SeatOccupationDetailsTypeI"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="seatColumn" type="{http://xml.amadeus.com/SMPRES_14_2_1A}AlphaNumericString_Length1To1"/&gt;
 *         &lt;element name="seatOccupation" type="{http://xml.amadeus.com/SMPRES_14_2_1A}AlphaString_Length1To1" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeatOccupationDetailsTypeI", propOrder = {
    "seatColumn",
    "seatOccupation"
})
public class SeatOccupationDetailsTypeI {

    @XmlElement(required = true)
    protected String seatColumn;
    protected String seatOccupation;

    /**
     * Gets the value of the seatColumn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatColumn() {
        return seatColumn;
    }

    /**
     * Sets the value of the seatColumn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatColumn(String value) {
        this.seatColumn = value;
    }

    /**
     * Gets the value of the seatOccupation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatOccupation() {
        return seatOccupation;
    }

    /**
     * Sets the value of the seatOccupation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatOccupation(String value) {
        this.seatOccupation = value;
    }

}
