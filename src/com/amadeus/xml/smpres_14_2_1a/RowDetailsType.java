
package com.amadeus.xml.smpres_14_2_1a;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify the individual seat information for seats in a particular row.
 * 
 * <p>Java class for RowDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RowDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="seatRowNumber" type="{http://xml.amadeus.com/SMPRES_14_2_1A}NumericDecimal_Length1To3"/&gt;
 *         &lt;element name="rowCharacteristicDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}RowCharacteristicsDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="seatOccupationDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}SeatOccupationDetailsTypeI_262401C" maxOccurs="12" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RowDetailsType", propOrder = {
    "seatRowNumber",
    "rowCharacteristicDetails",
    "seatOccupationDetails"
})
public class RowDetailsType {

    @XmlElement(required = true)
    protected BigDecimal seatRowNumber;
    protected RowCharacteristicsDetailsTypeI rowCharacteristicDetails;
    protected List<SeatOccupationDetailsTypeI262401C> seatOccupationDetails;

    /**
     * Gets the value of the seatRowNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeatRowNumber() {
        return seatRowNumber;
    }

    /**
     * Sets the value of the seatRowNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeatRowNumber(BigDecimal value) {
        this.seatRowNumber = value;
    }

    /**
     * Gets the value of the rowCharacteristicDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RowCharacteristicsDetailsTypeI }
     *     
     */
    public RowCharacteristicsDetailsTypeI getRowCharacteristicDetails() {
        return rowCharacteristicDetails;
    }

    /**
     * Sets the value of the rowCharacteristicDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RowCharacteristicsDetailsTypeI }
     *     
     */
    public void setRowCharacteristicDetails(RowCharacteristicsDetailsTypeI value) {
        this.rowCharacteristicDetails = value;
    }

    /**
     * Gets the value of the seatOccupationDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seatOccupationDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeatOccupationDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SeatOccupationDetailsTypeI262401C }
     * 
     * 
     */
    public List<SeatOccupationDetailsTypeI262401C> getSeatOccupationDetails() {
        if (seatOccupationDetails == null) {
            seatOccupationDetails = new ArrayList<SeatOccupationDetailsTypeI262401C>();
        }
        return this.seatOccupationDetails;
    }

}
