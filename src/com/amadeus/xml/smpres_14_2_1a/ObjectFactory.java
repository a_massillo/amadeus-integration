
package com.amadeus.xml.smpres_14_2_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.smpres_14_2_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.smpres_14_2_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply }
     * 
     */
    public AirRetrieveSeatMapReply createAirRetrieveSeatMapReply() {
        return new AirRetrieveSeatMapReply();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply.SeatmapInformation }
     * 
     */
    public AirRetrieveSeatMapReply.SeatmapInformation createAirRetrieveSeatMapReplySeatmapInformation() {
        return new AirRetrieveSeatMapReply.SeatmapInformation();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData }
     * 
     */
    public AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData createAirRetrieveSeatMapReplySeatmapInformationCustomerCentricData() {
        return new AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData();
    }

    /**
     * Create an instance of {@link ResponseAnalysisDetailsTypeI }
     * 
     */
    public ResponseAnalysisDetailsTypeI createResponseAnalysisDetailsTypeI() {
        return new ResponseAnalysisDetailsTypeI();
    }

    /**
     * Create an instance of {@link ErrorInformationTypeI }
     * 
     */
    public ErrorInformationTypeI createErrorInformationTypeI() {
        return new ErrorInformationTypeI();
    }

    /**
     * Create an instance of {@link WarningInformationType }
     * 
     */
    public WarningInformationType createWarningInformationType() {
        return new WarningInformationType();
    }

    /**
     * Create an instance of {@link SeatRequestParametersTypeI }
     * 
     */
    public SeatRequestParametersTypeI createSeatRequestParametersTypeI() {
        return new SeatRequestParametersTypeI();
    }

    /**
     * Create an instance of {@link AdditionalEquipmentInformationTypeI }
     * 
     */
    public AdditionalEquipmentInformationTypeI createAdditionalEquipmentInformationTypeI() {
        return new AdditionalEquipmentInformationTypeI();
    }

    /**
     * Create an instance of {@link AdditionalProductDetailsTypeI }
     * 
     */
    public AdditionalProductDetailsTypeI createAdditionalProductDetailsTypeI() {
        return new AdditionalProductDetailsTypeI();
    }

    /**
     * Create an instance of {@link AdditionalProductTypeI }
     * 
     */
    public AdditionalProductTypeI createAdditionalProductTypeI() {
        return new AdditionalProductTypeI();
    }

    /**
     * Create an instance of {@link CabinClassDesignationTypeI }
     * 
     */
    public CabinClassDesignationTypeI createCabinClassDesignationTypeI() {
        return new CabinClassDesignationTypeI();
    }

    /**
     * Create an instance of {@link CabinClassSeatRowRangeDetailsTypeI }
     * 
     */
    public CabinClassSeatRowRangeDetailsTypeI createCabinClassSeatRowRangeDetailsTypeI() {
        return new CabinClassSeatRowRangeDetailsTypeI();
    }

    /**
     * Create an instance of {@link CabinDetailsTypeI }
     * 
     */
    public CabinDetailsTypeI createCabinDetailsTypeI() {
        return new CabinDetailsTypeI();
    }

    /**
     * Create an instance of {@link CabinFacilitiesDetailsTypeI }
     * 
     */
    public CabinFacilitiesDetailsTypeI createCabinFacilitiesDetailsTypeI() {
        return new CabinFacilitiesDetailsTypeI();
    }

    /**
     * Create an instance of {@link CabinFacilitiesTypeI }
     * 
     */
    public CabinFacilitiesTypeI createCabinFacilitiesTypeI() {
        return new CabinFacilitiesTypeI();
    }

    /**
     * Create an instance of {@link CabinWidthAndColumnDetailsTypeI }
     * 
     */
    public CabinWidthAndColumnDetailsTypeI createCabinWidthAndColumnDetailsTypeI() {
        return new CabinWidthAndColumnDetailsTypeI();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType }
     * 
     */
    public CodedAttributeInformationType createCodedAttributeInformationType() {
        return new CodedAttributeInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeType }
     * 
     */
    public CodedAttributeType createCodedAttributeType() {
        return new CodedAttributeType();
    }

    /**
     * Create an instance of {@link CommunicationContactDetailsType }
     * 
     */
    public CommunicationContactDetailsType createCommunicationContactDetailsType() {
        return new CommunicationContactDetailsType();
    }

    /**
     * Create an instance of {@link CommunicationContactType }
     * 
     */
    public CommunicationContactType createCommunicationContactType() {
        return new CommunicationContactType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link ConfigurationDetailsTypeI }
     * 
     */
    public ConfigurationDetailsTypeI createConfigurationDetailsTypeI() {
        return new ConfigurationDetailsTypeI();
    }

    /**
     * Create an instance of {@link DummySegmentTypeI }
     * 
     */
    public DummySegmentTypeI createDummySegmentTypeI() {
        return new DummySegmentTypeI();
    }

    /**
     * Create an instance of {@link EquipmentInfoTypeI }
     * 
     */
    public EquipmentInfoTypeI createEquipmentInfoTypeI() {
        return new EquipmentInfoTypeI();
    }

    /**
     * Create an instance of {@link ErrorInformationDetailsTypeI }
     * 
     */
    public ErrorInformationDetailsTypeI createErrorInformationDetailsTypeI() {
        return new ErrorInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link FreeTextQualificationTypeI }
     * 
     */
    public FreeTextQualificationTypeI createFreeTextQualificationTypeI() {
        return new FreeTextQualificationTypeI();
    }

    /**
     * Create an instance of {@link GenericDetailsTypeI }
     * 
     */
    public GenericDetailsTypeI createGenericDetailsTypeI() {
        return new GenericDetailsTypeI();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextTypeI }
     * 
     */
    public InteractiveFreeTextTypeI createInteractiveFreeTextTypeI() {
        return new InteractiveFreeTextTypeI();
    }

    /**
     * Create an instance of {@link LocationTypeI }
     * 
     */
    public LocationTypeI createLocationTypeI() {
        return new LocationTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI }
     * 
     */
    public MonetaryInformationDetailsTypeI createMonetaryInformationDetailsTypeI() {
        return new MonetaryInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI }
     * 
     */
    public MonetaryInformationTypeI createMonetaryInformationTypeI() {
        return new MonetaryInformationTypeI();
    }

    /**
     * Create an instance of {@link OverwingSeatRowRangeTypeI }
     * 
     */
    public OverwingSeatRowRangeTypeI createOverwingSeatRowRangeTypeI() {
        return new OverwingSeatRowRangeTypeI();
    }

    /**
     * Create an instance of {@link ProductDateTimeTypeI }
     * 
     */
    public ProductDateTimeTypeI createProductDateTimeTypeI() {
        return new ProductDateTimeTypeI();
    }

    /**
     * Create an instance of {@link ProductFacilitiesTypeI }
     * 
     */
    public ProductFacilitiesTypeI createProductFacilitiesTypeI() {
        return new ProductFacilitiesTypeI();
    }

    /**
     * Create an instance of {@link ProductIdentificationDetailsTypeI }
     * 
     */
    public ProductIdentificationDetailsTypeI createProductIdentificationDetailsTypeI() {
        return new ProductIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link RangeOfRowsDetailsTypeI }
     * 
     */
    public RangeOfRowsDetailsTypeI createRangeOfRowsDetailsTypeI() {
        return new RangeOfRowsDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link RowCharacteristicsDetailsTypeI }
     * 
     */
    public RowCharacteristicsDetailsTypeI createRowCharacteristicsDetailsTypeI() {
        return new RowCharacteristicsDetailsTypeI();
    }

    /**
     * Create an instance of {@link RowDetailsTypeI }
     * 
     */
    public RowDetailsTypeI createRowDetailsTypeI() {
        return new RowDetailsTypeI();
    }

    /**
     * Create an instance of {@link RowDetailsType }
     * 
     */
    public RowDetailsType createRowDetailsType() {
        return new RowDetailsType();
    }

    /**
     * Create an instance of {@link SeatOccupationDetailsTypeI }
     * 
     */
    public SeatOccupationDetailsTypeI createSeatOccupationDetailsTypeI() {
        return new SeatOccupationDetailsTypeI();
    }

    /**
     * Create an instance of {@link SeatOccupationDetailsTypeI262401C }
     * 
     */
    public SeatOccupationDetailsTypeI262401C createSeatOccupationDetailsTypeI262401C() {
        return new SeatOccupationDetailsTypeI262401C();
    }

    /**
     * Create an instance of {@link SmokingAreaSeatRowRangeDetailsTypeI }
     * 
     */
    public SmokingAreaSeatRowRangeDetailsTypeI createSmokingAreaSeatRowRangeDetailsTypeI() {
        return new SmokingAreaSeatRowRangeDetailsTypeI();
    }

    /**
     * Create an instance of {@link StationInformationTypeI }
     * 
     */
    public StationInformationTypeI createStationInformationTypeI() {
        return new StationInformationTypeI();
    }

    /**
     * Create an instance of {@link TaxDetailsTypeI }
     * 
     */
    public TaxDetailsTypeI createTaxDetailsTypeI() {
        return new TaxDetailsTypeI();
    }

    /**
     * Create an instance of {@link TaxTypeI }
     * 
     */
    public TaxTypeI createTaxTypeI() {
        return new TaxTypeI();
    }

    /**
     * Create an instance of {@link TravelProductInformationTypeI }
     * 
     */
    public TravelProductInformationTypeI createTravelProductInformationTypeI() {
        return new TravelProductInformationTypeI();
    }

    /**
     * Create an instance of {@link TravellerDetailsTypeI }
     * 
     */
    public TravellerDetailsTypeI createTravellerDetailsTypeI() {
        return new TravellerDetailsTypeI();
    }

    /**
     * Create an instance of {@link TravellerInformationTypeI }
     * 
     */
    public TravellerInformationTypeI createTravellerInformationTypeI() {
        return new TravellerInformationTypeI();
    }

    /**
     * Create an instance of {@link TravellerSurnameInformationTypeI }
     * 
     */
    public TravellerSurnameInformationTypeI createTravellerSurnameInformationTypeI() {
        return new TravellerSurnameInformationTypeI();
    }

    /**
     * Create an instance of {@link TravellerTimeDetailsTypeI }
     * 
     */
    public TravellerTimeDetailsTypeI createTravellerTimeDetailsTypeI() {
        return new TravellerTimeDetailsTypeI();
    }

    /**
     * Create an instance of {@link WarningDetailsTypeI }
     * 
     */
    public WarningDetailsTypeI createWarningDetailsTypeI() {
        return new WarningDetailsTypeI();
    }

    /**
     * Create an instance of {@link WarningDetailsType }
     * 
     */
    public WarningDetailsType createWarningDetailsType() {
        return new WarningDetailsType();
    }

    /**
     * Create an instance of {@link WarningInformationTypeI }
     * 
     */
    public WarningInformationTypeI createWarningInformationTypeI() {
        return new WarningInformationTypeI();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply.SeatmapInformation.Cabin }
     * 
     */
    public AirRetrieveSeatMapReply.SeatmapInformation.Cabin createAirRetrieveSeatMapReplySeatmapInformationCabin() {
        return new AirRetrieveSeatMapReply.SeatmapInformation.Cabin();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply.SeatmapInformation.Row }
     * 
     */
    public AirRetrieveSeatMapReply.SeatmapInformation.Row createAirRetrieveSeatMapReplySeatmapInformationRow() {
        return new AirRetrieveSeatMapReply.SeatmapInformation.Row();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply.SeatmapInformation.CatalogData }
     * 
     */
    public AirRetrieveSeatMapReply.SeatmapInformation.CatalogData createAirRetrieveSeatMapReplySeatmapInformationCatalogData() {
        return new AirRetrieveSeatMapReply.SeatmapInformation.CatalogData();
    }

    /**
     * Create an instance of {@link AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice }
     * 
     */
    public AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice createAirRetrieveSeatMapReplySeatmapInformationCustomerCentricDataSeatPrice() {
        return new AirRetrieveSeatMapReply.SeatmapInformation.CustomerCentricData.SeatPrice();
    }

}
