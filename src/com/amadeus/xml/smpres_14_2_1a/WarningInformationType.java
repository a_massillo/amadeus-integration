
package com.amadeus.xml.smpres_14_2_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify the warning(s) resulting from processing.
 * 
 * <p>Java class for WarningInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WarningInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="warningDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}WarningDetailsType"/&gt;
 *         &lt;element name="otherWarningDetails" type="{http://xml.amadeus.com/SMPRES_14_2_1A}WarningDetailsType" maxOccurs="99" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WarningInformationType", propOrder = {
    "warningDetails",
    "otherWarningDetails"
})
public class WarningInformationType {

    @XmlElement(required = true)
    protected WarningDetailsType warningDetails;
    protected List<WarningDetailsType> otherWarningDetails;

    /**
     * Gets the value of the warningDetails property.
     * 
     * @return
     *     possible object is
     *     {@link WarningDetailsType }
     *     
     */
    public WarningDetailsType getWarningDetails() {
        return warningDetails;
    }

    /**
     * Sets the value of the warningDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link WarningDetailsType }
     *     
     */
    public void setWarningDetails(WarningDetailsType value) {
        this.warningDetails = value;
    }

    /**
     * Gets the value of the otherWarningDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherWarningDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherWarningDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WarningDetailsType }
     * 
     * 
     */
    public List<WarningDetailsType> getOtherWarningDetails() {
        if (otherWarningDetails == null) {
            otherWarningDetails = new ArrayList<WarningDetailsType>();
        }
        return this.otherWarningDetails;
    }

}
