
package com.amadeus.xml.trfpcq_13_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Conveys ticket stock range information. A stock range is identified by a stock type, a range number and an allocation status.
 * 
 * <p>Java class for StockRangeInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StockRangeInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stockTypeCode" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}AlphaNumericString_Length1To2" minOccurs="0"/&gt;
 *         &lt;element name="stockProvider" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}AlphaNumericString_Length1To3" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StockRangeInformationType", propOrder = {
    "stockTypeCode",
    "stockProvider"
})
public class StockRangeInformationType {

    protected String stockTypeCode;
    protected String stockProvider;

    /**
     * Gets the value of the stockTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockTypeCode() {
        return stockTypeCode;
    }

    /**
     * Sets the value of the stockTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockTypeCode(String value) {
        this.stockTypeCode = value;
    }

    /**
     * Gets the value of the stockProvider property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockProvider() {
        return stockProvider;
    }

    /**
     * Sets the value of the stockProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockProvider(String value) {
        this.stockProvider = value;
    }

}
