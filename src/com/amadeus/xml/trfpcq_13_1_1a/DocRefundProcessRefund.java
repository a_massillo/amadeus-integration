
package com.amadeus.xml.trfpcq_13_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="actionDetails" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}StatusTypeI" minOccurs="0"/&gt;
 *         &lt;element name="dummysegment" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}DummySegmentTypeI" minOccurs="0"/&gt;
 *         &lt;element name="printerReference" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}ReferenceInformationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="targetStockProvider" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}StockRangeInformationType" minOccurs="0"/&gt;
 *         &lt;element name="refundedItinerary" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}RefundedItineraryType" maxOccurs="16" minOccurs="0"/&gt;
 *         &lt;element name="phoneFaxEmailAddress" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}PhoneAndEmailAddressType" maxOccurs="3" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "actionDetails",
    "dummysegment",
    "printerReference",
    "targetStockProvider",
    "refundedItinerary",
    "phoneFaxEmailAddress"
})
@XmlRootElement(name = "DocRefund_ProcessRefund")
public class DocRefundProcessRefund {

    protected StatusTypeI actionDetails;
    protected DummySegmentTypeI dummysegment;
    protected ReferenceInformationTypeI printerReference;
    protected StockRangeInformationType targetStockProvider;
    protected List<RefundedItineraryType> refundedItinerary;
    protected List<PhoneAndEmailAddressType> phoneFaxEmailAddress;

    /**
     * Gets the value of the actionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link StatusTypeI }
     *     
     */
    public StatusTypeI getActionDetails() {
        return actionDetails;
    }

    /**
     * Sets the value of the actionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusTypeI }
     *     
     */
    public void setActionDetails(StatusTypeI value) {
        this.actionDetails = value;
    }

    /**
     * Gets the value of the dummysegment property.
     * 
     * @return
     *     possible object is
     *     {@link DummySegmentTypeI }
     *     
     */
    public DummySegmentTypeI getDummysegment() {
        return dummysegment;
    }

    /**
     * Sets the value of the dummysegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link DummySegmentTypeI }
     *     
     */
    public void setDummysegment(DummySegmentTypeI value) {
        this.dummysegment = value;
    }

    /**
     * Gets the value of the printerReference property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceInformationTypeI }
     *     
     */
    public ReferenceInformationTypeI getPrinterReference() {
        return printerReference;
    }

    /**
     * Sets the value of the printerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceInformationTypeI }
     *     
     */
    public void setPrinterReference(ReferenceInformationTypeI value) {
        this.printerReference = value;
    }

    /**
     * Gets the value of the targetStockProvider property.
     * 
     * @return
     *     possible object is
     *     {@link StockRangeInformationType }
     *     
     */
    public StockRangeInformationType getTargetStockProvider() {
        return targetStockProvider;
    }

    /**
     * Sets the value of the targetStockProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link StockRangeInformationType }
     *     
     */
    public void setTargetStockProvider(StockRangeInformationType value) {
        this.targetStockProvider = value;
    }

    /**
     * Gets the value of the refundedItinerary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refundedItinerary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefundedItinerary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefundedItineraryType }
     * 
     * 
     */
    public List<RefundedItineraryType> getRefundedItinerary() {
        if (refundedItinerary == null) {
            refundedItinerary = new ArrayList<RefundedItineraryType>();
        }
        return this.refundedItinerary;
    }

    /**
     * Gets the value of the phoneFaxEmailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the phoneFaxEmailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhoneFaxEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhoneAndEmailAddressType }
     * 
     * 
     */
    public List<PhoneAndEmailAddressType> getPhoneFaxEmailAddress() {
        if (phoneFaxEmailAddress == null) {
            phoneFaxEmailAddress = new ArrayList<PhoneAndEmailAddressType>();
        }
        return this.phoneFaxEmailAddress;
    }

}
