
package com.amadeus.xml.trfpcq_13_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.trfpcq_13_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.trfpcq_13_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocRefundProcessRefund }
     * 
     */
    public DocRefundProcessRefund createDocRefundProcessRefund() {
        return new DocRefundProcessRefund();
    }

    /**
     * Create an instance of {@link StatusTypeI }
     * 
     */
    public StatusTypeI createStatusTypeI() {
        return new StatusTypeI();
    }

    /**
     * Create an instance of {@link DummySegmentTypeI }
     * 
     */
    public DummySegmentTypeI createDummySegmentTypeI() {
        return new DummySegmentTypeI();
    }

    /**
     * Create an instance of {@link ReferenceInformationTypeI }
     * 
     */
    public ReferenceInformationTypeI createReferenceInformationTypeI() {
        return new ReferenceInformationTypeI();
    }

    /**
     * Create an instance of {@link StockRangeInformationType }
     * 
     */
    public StockRangeInformationType createStockRangeInformationType() {
        return new StockRangeInformationType();
    }

    /**
     * Create an instance of {@link RefundedItineraryType }
     * 
     */
    public RefundedItineraryType createRefundedItineraryType() {
        return new RefundedItineraryType();
    }

    /**
     * Create an instance of {@link PhoneAndEmailAddressType }
     * 
     */
    public PhoneAndEmailAddressType createPhoneAndEmailAddressType() {
        return new PhoneAndEmailAddressType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI }
     * 
     */
    public OriginAndDestinationDetailsTypeI createOriginAndDestinationDetailsTypeI() {
        return new OriginAndDestinationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI }
     * 
     */
    public ReferencingDetailsTypeI createReferencingDetailsTypeI() {
        return new ReferencingDetailsTypeI();
    }

    /**
     * Create an instance of {@link StatusDetailsTypeI }
     * 
     */
    public StatusDetailsTypeI createStatusDetailsTypeI() {
        return new StatusDetailsTypeI();
    }

    /**
     * Create an instance of {@link StructuredTelephoneNumberType }
     * 
     */
    public StructuredTelephoneNumberType createStructuredTelephoneNumberType() {
        return new StructuredTelephoneNumberType();
    }

    /**
     * Create an instance of {@link TransportIdentifierTypeI }
     * 
     */
    public TransportIdentifierTypeI createTransportIdentifierTypeI() {
        return new TransportIdentifierTypeI();
    }

}
