
package com.amadeus.xml.trfpcq_13_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To convey the refunded itinerary
 * 
 * <p>Java class for RefundedItineraryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RefundedItineraryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="airlineCodeRfndItinerary" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}TransportIdentifierTypeI"/&gt;
 *         &lt;element name="originDestinationRfndItinerary" type="{http://xml.amadeus.com/TRFPCQ_13_1_1A}OriginAndDestinationDetailsTypeI"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefundedItineraryType", propOrder = {
    "airlineCodeRfndItinerary",
    "originDestinationRfndItinerary"
})
public class RefundedItineraryType {

    @XmlElement(required = true)
    protected TransportIdentifierTypeI airlineCodeRfndItinerary;
    @XmlElement(required = true)
    protected OriginAndDestinationDetailsTypeI originDestinationRfndItinerary;

    /**
     * Gets the value of the airlineCodeRfndItinerary property.
     * 
     * @return
     *     possible object is
     *     {@link TransportIdentifierTypeI }
     *     
     */
    public TransportIdentifierTypeI getAirlineCodeRfndItinerary() {
        return airlineCodeRfndItinerary;
    }

    /**
     * Sets the value of the airlineCodeRfndItinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportIdentifierTypeI }
     *     
     */
    public void setAirlineCodeRfndItinerary(TransportIdentifierTypeI value) {
        this.airlineCodeRfndItinerary = value;
    }

    /**
     * Gets the value of the originDestinationRfndItinerary property.
     * 
     * @return
     *     possible object is
     *     {@link OriginAndDestinationDetailsTypeI }
     *     
     */
    public OriginAndDestinationDetailsTypeI getOriginDestinationRfndItinerary() {
        return originDestinationRfndItinerary;
    }

    /**
     * Sets the value of the originDestinationRfndItinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginAndDestinationDetailsTypeI }
     *     
     */
    public void setOriginDestinationRfndItinerary(OriginAndDestinationDetailsTypeI value) {
        this.originDestinationRfndItinerary = value;
    }

}
