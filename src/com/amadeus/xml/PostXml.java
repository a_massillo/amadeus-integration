package com.amadeus.xml;


import java.net.*;
import java.io.*;

public class PostXml {

  public static void main(String[] args) {
		
    try {
      String xmldata = 
   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +   
      "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\""+
      	"xmlns:ns1=\"http://xml.amadeus.com/VLSSLQ_06_1_1A\">"+
      	"<SOAP-ENV:Body>"+
      		"<ns1:Security_Authenticate>"+
      			"<ns1:userIdentifier>"+
      				"<ns1:originIdentification>"+
      					"<ns1:sourceOffice>LONU128WH</ns1:sourceOffice>"+
      				"</ns1:originIdentification>"+
      				"<ns1:originatorTypeCode>U</ns1:originatorTypeCode>"+
      				"<ns1:originator>WSWAFWFI</ns1:originator>"+
      			"</ns1:userIdentifier>"+
      			"<ns1:dutyCode>"+
      				"<ns1:dutyCodeDetails>"+
      					"<ns1:referenceQualifier>DUT</ns1:referenceQualifier>"+
      					"<ns1:referenceIdentifier>SU</ns1:referenceIdentifier>"+
      				"</ns1:dutyCodeDetails>"+
      			"</ns1:dutyCode>"+
      			"<ns1:systemDetails>"+
      				"<ns1:organizationDetails>"+
      					"<ns1:organizationId>NMC-UK</ns1:organizationId>"+
      				"</ns1:organizationDetails>"+
      			"</ns1:systemDetails>"+
      			"<ns1:passwordInfo>"+
      				"<ns1:dataLength>7</ns1:dataLength>"+
      				"<ns1:dataType>E</ns1:dataType>"+
      				"<ns1:binaryData>QU1BREVVUw==</ns1:binaryData>"+
      			"</ns1:passwordInfo>"+
      		"</ns1:Security_Authenticate>"+
      	"</SOAP-ENV:Body>"+
      "</SOAP-ENV:Envelope>";
			
      //Create socket
      String hostname = "test.webservices.amadeus.com";
      int port = 443;
      InetAddress  addr = InetAddress.getByName(hostname);
      Socket sock = new Socket(addr, port);
			
      //Send header
      String path = "/rcx-ws/rcx";
      BufferedWriter  wr = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream(),"UTF-8"));
      // You can use "UTF8" for compatibility with the Microsoft virtual machine.
      wr.write("POST " + path + " HTTP/1.0\r\n");
      //wr.write("Host: www.pascalbotte.be\r\n");
      wr.write("Content-Length: " + xmldata.length() + "\r\n");
      wr.write("Content-Type: text/xml; charset=\"utf-8\"\r\n");
      wr.write("\r\n");
			
      //Send data
      wr.write(xmldata);
      wr.flush();
			
      // Response
      BufferedReader rd = new BufferedReader(new InputStreamReader(sock.getInputStream()));
      String line;
      while((line = rd.readLine()) != null)
	System.out.println(line);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

