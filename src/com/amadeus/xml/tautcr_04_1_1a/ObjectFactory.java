
package com.amadeus.xml.tautcr_04_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.tautcr_04_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.tautcr_04_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TicketCreateTSTFromPricingReply }
     * 
     */
    public TicketCreateTSTFromPricingReply createTicketCreateTSTFromPricingReply() {
        return new TicketCreateTSTFromPricingReply();
    }

    /**
     * Create an instance of {@link TicketCreateTSTFromPricingReply.ApplicationError }
     * 
     */
    public TicketCreateTSTFromPricingReply.ApplicationError createTicketCreateTSTFromPricingReplyApplicationError() {
        return new TicketCreateTSTFromPricingReply.ApplicationError();
    }

    /**
     * Create an instance of {@link ReservationControlInformationTypeI }
     * 
     */
    public ReservationControlInformationTypeI createReservationControlInformationTypeI() {
        return new ReservationControlInformationTypeI();
    }

    /**
     * Create an instance of {@link TicketCreateTSTFromPricingReply.TstList }
     * 
     */
    public TicketCreateTSTFromPricingReply.TstList createTicketCreateTSTFromPricingReplyTstList() {
        return new TicketCreateTSTFromPricingReply.TstList();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailType }
     * 
     */
    public ApplicationErrorDetailType createApplicationErrorDetailType() {
        return new ApplicationErrorDetailType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType }
     * 
     */
    public ApplicationErrorInformationType createApplicationErrorInformationType() {
        return new ApplicationErrorInformationType();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextTypeI }
     * 
     */
    public InteractiveFreeTextTypeI createInteractiveFreeTextTypeI() {
        return new InteractiveFreeTextTypeI();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType }
     * 
     */
    public ItemReferencesAndVersionsType createItemReferencesAndVersionsType() {
        return new ItemReferencesAndVersionsType();
    }

    /**
     * Create an instance of {@link ReferenceInformationTypeI }
     * 
     */
    public ReferenceInformationTypeI createReferenceInformationTypeI() {
        return new ReferenceInformationTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI }
     * 
     */
    public ReferencingDetailsTypeI createReferencingDetailsTypeI() {
        return new ReferencingDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsTypeI }
     * 
     */
    public ReservationControlInformationDetailsTypeI createReservationControlInformationDetailsTypeI() {
        return new ReservationControlInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link UniqueIdDescriptionType }
     * 
     */
    public UniqueIdDescriptionType createUniqueIdDescriptionType() {
        return new UniqueIdDescriptionType();
    }

}
