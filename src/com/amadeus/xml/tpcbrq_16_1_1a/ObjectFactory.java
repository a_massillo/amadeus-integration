
package com.amadeus.xml.tpcbrq_16_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.tpcbrq_16_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.tpcbrq_16_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClass }
     * 
     */
    public FarePricePNRWithBookingClass createFarePricePNRWithBookingClass() {
        return new FarePricePNRWithBookingClass();
    }

    /**
     * Create an instance of {@link FarePricePNRWithBookingClass.PricingOptionGroup }
     * 
     */
    public FarePricePNRWithBookingClass.PricingOptionGroup createFarePricePNRWithBookingClassPricingOptionGroup() {
        return new FarePricePNRWithBookingClass.PricingOptionGroup();
    }

    /**
     * Create an instance of {@link AttributeInformationTypeU }
     * 
     */
    public AttributeInformationTypeU createAttributeInformationTypeU() {
        return new AttributeInformationTypeU();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link CurrenciesType }
     * 
     */
    public CurrenciesType createCurrenciesType() {
        return new CurrenciesType();
    }

    /**
     * Create an instance of {@link CurrencyDetailsTypeU }
     * 
     */
    public CurrencyDetailsTypeU createCurrencyDetailsTypeU() {
        return new CurrencyDetailsTypeU();
    }

    /**
     * Create an instance of {@link DiscountAndPenaltyInformationType }
     * 
     */
    public DiscountAndPenaltyInformationType createDiscountAndPenaltyInformationType() {
        return new DiscountAndPenaltyInformationType();
    }

    /**
     * Create an instance of {@link DiscountPenaltyMonetaryInformationType }
     * 
     */
    public DiscountPenaltyMonetaryInformationType createDiscountPenaltyMonetaryInformationType() {
        return new DiscountPenaltyMonetaryInformationType();
    }

    /**
     * Create an instance of {@link DutyTaxFeeAccountDetailType }
     * 
     */
    public DutyTaxFeeAccountDetailType createDutyTaxFeeAccountDetailType() {
        return new DutyTaxFeeAccountDetailType();
    }

    /**
     * Create an instance of {@link DutyTaxFeeDetailType }
     * 
     */
    public DutyTaxFeeDetailType createDutyTaxFeeDetailType() {
        return new DutyTaxFeeDetailType();
    }

    /**
     * Create an instance of {@link DutyTaxFeeDetailsType }
     * 
     */
    public DutyTaxFeeDetailsType createDutyTaxFeeDetailsType() {
        return new DutyTaxFeeDetailsType();
    }

    /**
     * Create an instance of {@link FormOfPaymentDetailsType }
     * 
     */
    public FormOfPaymentDetailsType createFormOfPaymentDetailsType() {
        return new FormOfPaymentDetailsType();
    }

    /**
     * Create an instance of {@link FormOfPaymentType }
     * 
     */
    public FormOfPaymentType createFormOfPaymentType() {
        return new FormOfPaymentType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationCodeType }
     * 
     */
    public FrequentTravellerIdentificationCodeType createFrequentTravellerIdentificationCodeType() {
        return new FrequentTravellerIdentificationCodeType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationType }
     * 
     */
    public FrequentTravellerIdentificationType createFrequentTravellerIdentificationType() {
        return new FrequentTravellerIdentificationType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType }
     * 
     */
    public MonetaryInformationDetailsType createMonetaryInformationDetailsType() {
        return new MonetaryInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link PlaceLocationIdentificationType }
     * 
     */
    public PlaceLocationIdentificationType createPlaceLocationIdentificationType() {
        return new PlaceLocationIdentificationType();
    }

    /**
     * Create an instance of {@link PricingOptionKey }
     * 
     */
    public PricingOptionKey createPricingOptionKey() {
        return new PricingOptionKey();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link RelatedLocationOneIdentificationType }
     * 
     */
    public RelatedLocationOneIdentificationType createRelatedLocationOneIdentificationType() {
        return new RelatedLocationOneIdentificationType();
    }

    /**
     * Create an instance of {@link RelatedLocationTwoIdentificationType }
     * 
     */
    public RelatedLocationTwoIdentificationType createRelatedLocationTwoIdentificationType() {
        return new RelatedLocationTwoIdentificationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType }
     * 
     */
    public StructuredDateTimeInformationType createStructuredDateTimeInformationType() {
        return new StructuredDateTimeInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link TransportIdentifierType }
     * 
     */
    public TransportIdentifierType createTransportIdentifierType() {
        return new TransportIdentifierType();
    }

}
