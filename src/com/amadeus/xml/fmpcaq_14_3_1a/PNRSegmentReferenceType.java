
package com.amadeus.xml.fmpcaq_14_3_1a;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Reference to PNR segment
 * 
 * <p>Java class for PNRSegmentReferenceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PNRSegmentReferenceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pnrSegmentTattoo" type="{http://xml.amadeus.com/FMPCAQ_14_3_1A}NumericInteger_Length0To35" minOccurs="0"/&gt;
 *         &lt;element name="pnrSegmentQualifier" type="{http://xml.amadeus.com/FMPCAQ_14_3_1A}AlphaString_Length1To1" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PNRSegmentReferenceType", propOrder = {
    "pnrSegmentTattoo",
    "pnrSegmentQualifier"
})
public class PNRSegmentReferenceType {

    protected BigInteger pnrSegmentTattoo;
    protected String pnrSegmentQualifier;

    /**
     * Gets the value of the pnrSegmentTattoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPnrSegmentTattoo() {
        return pnrSegmentTattoo;
    }

    /**
     * Sets the value of the pnrSegmentTattoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPnrSegmentTattoo(BigInteger value) {
        this.pnrSegmentTattoo = value;
    }

    /**
     * Gets the value of the pnrSegmentQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnrSegmentQualifier() {
        return pnrSegmentQualifier;
    }

    /**
     * Sets the value of the pnrSegmentQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnrSegmentQualifier(String value) {
        this.pnrSegmentQualifier = value;
    }

}
