
package com.amadeus.xml.fmpcaq_14_3_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.fmpcaq_14_3_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.fmpcaq_14_3_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar }
     * 
     */
    public FareMasterPricerCalendar createFareMasterPricerCalendar() {
        return new FareMasterPricerCalendar();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.OfficeIdDetails }
     * 
     */
    public FareMasterPricerCalendar.OfficeIdDetails createFareMasterPricerCalendarOfficeIdDetails() {
        return new FareMasterPricerCalendar.OfficeIdDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FeeOption }
     * 
     */
    public FareMasterPricerCalendar.FeeOption createFareMasterPricerCalendarFeeOption() {
        return new FareMasterPricerCalendar.FeeOption();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FeeOption.FeeDetails }
     * 
     */
    public FareMasterPricerCalendar.FeeOption.FeeDetails createFareMasterPricerCalendarFeeOptionFeeDetails() {
        return new FareMasterPricerCalendar.FeeOption.FeeDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.TicketChangeInfo }
     * 
     */
    public FareMasterPricerCalendar.TicketChangeInfo createFareMasterPricerCalendarTicketChangeInfo() {
        return new FareMasterPricerCalendar.TicketChangeInfo();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.Itinerary }
     * 
     */
    public FareMasterPricerCalendar.Itinerary createFareMasterPricerCalendarItinerary() {
        return new FareMasterPricerCalendar.Itinerary();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.Itinerary.FlightInfoPNR }
     * 
     */
    public FareMasterPricerCalendar.Itinerary.FlightInfoPNR createFareMasterPricerCalendarItineraryFlightInfoPNR() {
        return new FareMasterPricerCalendar.Itinerary.FlightInfoPNR();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.Itinerary.GroupOfFlights }
     * 
     */
    public FareMasterPricerCalendar.Itinerary.GroupOfFlights createFareMasterPricerCalendarItineraryGroupOfFlights() {
        return new FareMasterPricerCalendar.Itinerary.GroupOfFlights();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FareFamilies }
     * 
     */
    public FareMasterPricerCalendar.FareFamilies createFareMasterPricerCalendarFareFamilies() {
        return new FareMasterPricerCalendar.FareFamilies();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FareFamilies.OtherPossibleCriteria }
     * 
     */
    public FareMasterPricerCalendar.FareFamilies.OtherPossibleCriteria createFareMasterPricerCalendarFareFamiliesOtherPossibleCriteria() {
        return new FareMasterPricerCalendar.FareFamilies.OtherPossibleCriteria();
    }

    /**
     * Create an instance of {@link GroupPassengerDetailsType }
     * 
     */
    public GroupPassengerDetailsType createGroupPassengerDetailsType() {
        return new GroupPassengerDetailsType();
    }

    /**
     * Create an instance of {@link NumberOfUnitsType }
     * 
     */
    public NumberOfUnitsType createNumberOfUnitsType() {
        return new NumberOfUnitsType();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link TravellerReferenceInformationType }
     * 
     */
    public TravellerReferenceInformationType createTravellerReferenceInformationType() {
        return new TravellerReferenceInformationType();
    }

    /**
     * Create an instance of {@link ConsumerReferenceInformationType }
     * 
     */
    public ConsumerReferenceInformationType createConsumerReferenceInformationType() {
        return new ConsumerReferenceInformationType();
    }

    /**
     * Create an instance of {@link FOPRepresentationType }
     * 
     */
    public FOPRepresentationType createFOPRepresentationType() {
        return new FOPRepresentationType();
    }

    /**
     * Create an instance of {@link FareInformationType }
     * 
     */
    public FareInformationType createFareInformationType() {
        return new FareInformationType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FareOptions }
     * 
     */
    public FareMasterPricerCalendar.FareOptions createFareMasterPricerCalendarFareOptions() {
        return new FareMasterPricerCalendar.FareOptions();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link TaxType }
     * 
     */
    public TaxType createTaxType() {
        return new TaxType();
    }

    /**
     * Create an instance of {@link TravelFlightInformationType185853S }
     * 
     */
    public TravelFlightInformationType185853S createTravelFlightInformationType185853S() {
        return new TravelFlightInformationType185853S();
    }

    /**
     * Create an instance of {@link ValueSearchCriteriaType }
     * 
     */
    public ValueSearchCriteriaType createValueSearchCriteriaType() {
        return new ValueSearchCriteriaType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.CombinationFareFamilies }
     * 
     */
    public FareMasterPricerCalendar.CombinationFareFamilies createFareMasterPricerCalendarCombinationFareFamilies() {
        return new FareMasterPricerCalendar.CombinationFareFamilies();
    }

    /**
     * Create an instance of {@link ActionIdentificationType }
     * 
     */
    public ActionIdentificationType createActionIdentificationType() {
        return new ActionIdentificationType();
    }

    /**
     * Create an instance of {@link AdditionalProductDetailsTypeI }
     * 
     */
    public AdditionalProductDetailsTypeI createAdditionalProductDetailsTypeI() {
        return new AdditionalProductDetailsTypeI();
    }

    /**
     * Create an instance of {@link AdditionalProductDetailsType }
     * 
     */
    public AdditionalProductDetailsType createAdditionalProductDetailsType() {
        return new AdditionalProductDetailsType();
    }

    /**
     * Create an instance of {@link AdditionalProductTypeI }
     * 
     */
    public AdditionalProductTypeI createAdditionalProductTypeI() {
        return new AdditionalProductTypeI();
    }

    /**
     * Create an instance of {@link AgentIdentificationType }
     * 
     */
    public AgentIdentificationType createAgentIdentificationType() {
        return new AgentIdentificationType();
    }

    /**
     * Create an instance of {@link ArithmeticEvaluationType }
     * 
     */
    public ArithmeticEvaluationType createArithmeticEvaluationType() {
        return new ArithmeticEvaluationType();
    }

    /**
     * Create an instance of {@link ArrivalLocalizationType }
     * 
     */
    public ArrivalLocalizationType createArrivalLocalizationType() {
        return new ArrivalLocalizationType();
    }

    /**
     * Create an instance of {@link ArrivalLocationDetailsType }
     * 
     */
    public ArrivalLocationDetailsType createArrivalLocationDetailsType() {
        return new ArrivalLocationDetailsType();
    }

    /**
     * Create an instance of {@link ArrivalLocationDetailsType120834C }
     * 
     */
    public ArrivalLocationDetailsType120834C createArrivalLocationDetailsType120834C() {
        return new ArrivalLocationDetailsType120834C();
    }

    /**
     * Create an instance of {@link AttributeInformationType }
     * 
     */
    public AttributeInformationType createAttributeInformationType() {
        return new AttributeInformationType();
    }

    /**
     * Create an instance of {@link AttributeInformationType97181C }
     * 
     */
    public AttributeInformationType97181C createAttributeInformationType97181C() {
        return new AttributeInformationType97181C();
    }

    /**
     * Create an instance of {@link AttributeType61377S }
     * 
     */
    public AttributeType61377S createAttributeType61377S() {
        return new AttributeType61377S();
    }

    /**
     * Create an instance of {@link BooleanExpressionRuleType }
     * 
     */
    public BooleanExpressionRuleType createBooleanExpressionRuleType() {
        return new BooleanExpressionRuleType();
    }

    /**
     * Create an instance of {@link CabinClassDesignationType }
     * 
     */
    public CabinClassDesignationType createCabinClassDesignationType() {
        return new CabinClassDesignationType();
    }

    /**
     * Create an instance of {@link CabinIdentificationType }
     * 
     */
    public CabinIdentificationType createCabinIdentificationType() {
        return new CabinIdentificationType();
    }

    /**
     * Create an instance of {@link CabinIdentificationType233500C }
     * 
     */
    public CabinIdentificationType233500C createCabinIdentificationType233500C() {
        return new CabinIdentificationType233500C();
    }

    /**
     * Create an instance of {@link CabinProductDetailsType }
     * 
     */
    public CabinProductDetailsType createCabinProductDetailsType() {
        return new CabinProductDetailsType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType }
     * 
     */
    public CodedAttributeInformationType createCodedAttributeInformationType() {
        return new CodedAttributeInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType120742C }
     * 
     */
    public CodedAttributeInformationType120742C createCodedAttributeInformationType120742C() {
        return new CodedAttributeInformationType120742C();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType139508C }
     * 
     */
    public CodedAttributeInformationType139508C createCodedAttributeInformationType139508C() {
        return new CodedAttributeInformationType139508C();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType247828C }
     * 
     */
    public CodedAttributeInformationType247828C createCodedAttributeInformationType247828C() {
        return new CodedAttributeInformationType247828C();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType247829C }
     * 
     */
    public CodedAttributeInformationType247829C createCodedAttributeInformationType247829C() {
        return new CodedAttributeInformationType247829C();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType254574C }
     * 
     */
    public CodedAttributeInformationType254574C createCodedAttributeInformationType254574C() {
        return new CodedAttributeInformationType254574C();
    }

    /**
     * Create an instance of {@link CodedAttributeType }
     * 
     */
    public CodedAttributeType createCodedAttributeType() {
        return new CodedAttributeType();
    }

    /**
     * Create an instance of {@link CodedAttributeType181239S }
     * 
     */
    public CodedAttributeType181239S createCodedAttributeType181239S() {
        return new CodedAttributeType181239S();
    }

    /**
     * Create an instance of {@link CodedAttributeType78500S }
     * 
     */
    public CodedAttributeType78500S createCodedAttributeType78500S() {
        return new CodedAttributeType78500S();
    }

    /**
     * Create an instance of {@link CommercialAgreementsType }
     * 
     */
    public CommercialAgreementsType createCommercialAgreementsType() {
        return new CommercialAgreementsType();
    }

    /**
     * Create an instance of {@link CommercialAgreementsType78540S }
     * 
     */
    public CommercialAgreementsType78540S createCommercialAgreementsType78540S() {
        return new CommercialAgreementsType78540S();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link CompanyIdentificationType }
     * 
     */
    public CompanyIdentificationType createCompanyIdentificationType() {
        return new CompanyIdentificationType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationType120719C }
     * 
     */
    public CompanyIdentificationType120719C createCompanyIdentificationType120719C() {
        return new CompanyIdentificationType120719C();
    }

    /**
     * Create an instance of {@link CompanyIdentificationType195544C }
     * 
     */
    public CompanyIdentificationType195544C createCompanyIdentificationType195544C() {
        return new CompanyIdentificationType195544C();
    }

    /**
     * Create an instance of {@link CompanyIdentificationType233548C }
     * 
     */
    public CompanyIdentificationType233548C createCompanyIdentificationType233548C() {
        return new CompanyIdentificationType233548C();
    }

    /**
     * Create an instance of {@link CompanyRoleIdentificationType }
     * 
     */
    public CompanyRoleIdentificationType createCompanyRoleIdentificationType() {
        return new CompanyRoleIdentificationType();
    }

    /**
     * Create an instance of {@link CompanyRoleIdentificationType120761C }
     * 
     */
    public CompanyRoleIdentificationType120761C createCompanyRoleIdentificationType120761C() {
        return new CompanyRoleIdentificationType120761C();
    }

    /**
     * Create an instance of {@link ConnectPointDetailsType }
     * 
     */
    public ConnectPointDetailsType createConnectPointDetailsType() {
        return new ConnectPointDetailsType();
    }

    /**
     * Create an instance of {@link ConnectPointDetailsType195492C }
     * 
     */
    public ConnectPointDetailsType195492C createConnectPointDetailsType195492C() {
        return new ConnectPointDetailsType195492C();
    }

    /**
     * Create an instance of {@link ConnectionDetailsTypeI }
     * 
     */
    public ConnectionDetailsTypeI createConnectionDetailsTypeI() {
        return new ConnectionDetailsTypeI();
    }

    /**
     * Create an instance of {@link ConnectionTypeI }
     * 
     */
    public ConnectionTypeI createConnectionTypeI() {
        return new ConnectionTypeI();
    }

    /**
     * Create an instance of {@link ConsumerReferenceIdentificationTypeI }
     * 
     */
    public ConsumerReferenceIdentificationTypeI createConsumerReferenceIdentificationTypeI() {
        return new ConsumerReferenceIdentificationTypeI();
    }

    /**
     * Create an instance of {@link ConversionRateDetailsType }
     * 
     */
    public ConversionRateDetailsType createConversionRateDetailsType() {
        return new ConversionRateDetailsType();
    }

    /**
     * Create an instance of {@link ConversionRateType }
     * 
     */
    public ConversionRateType createConversionRateType() {
        return new ConversionRateType();
    }

    /**
     * Create an instance of {@link CorporateFareIdentifiersType }
     * 
     */
    public CorporateFareIdentifiersType createCorporateFareIdentifiersType() {
        return new CorporateFareIdentifiersType();
    }

    /**
     * Create an instance of {@link CorporateFareInformationType }
     * 
     */
    public CorporateFareInformationType createCorporateFareInformationType() {
        return new CorporateFareInformationType();
    }

    /**
     * Create an instance of {@link CorporateIdentificationType }
     * 
     */
    public CorporateIdentificationType createCorporateIdentificationType() {
        return new CorporateIdentificationType();
    }

    /**
     * Create an instance of {@link CorporateIdentityType }
     * 
     */
    public CorporateIdentityType createCorporateIdentityType() {
        return new CorporateIdentityType();
    }

    /**
     * Create an instance of {@link CriteriaiDetaislType }
     * 
     */
    public CriteriaiDetaislType createCriteriaiDetaislType() {
        return new CriteriaiDetaislType();
    }

    /**
     * Create an instance of {@link DataInformationType }
     * 
     */
    public DataInformationType createDataInformationType() {
        return new DataInformationType();
    }

    /**
     * Create an instance of {@link DataTypeInformationType }
     * 
     */
    public DataTypeInformationType createDataTypeInformationType() {
        return new DataTypeInformationType();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsTypeI }
     * 
     */
    public DateAndTimeDetailsTypeI createDateAndTimeDetailsTypeI() {
        return new DateAndTimeDetailsTypeI();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsTypeI120740C }
     * 
     */
    public DateAndTimeDetailsTypeI120740C createDateAndTimeDetailsTypeI120740C() {
        return new DateAndTimeDetailsTypeI120740C();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsType }
     * 
     */
    public DateAndTimeDetailsType createDateAndTimeDetailsType() {
        return new DateAndTimeDetailsType();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsType120762C }
     * 
     */
    public DateAndTimeDetailsType120762C createDateAndTimeDetailsType120762C() {
        return new DateAndTimeDetailsType120762C();
    }

    /**
     * Create an instance of {@link DateAndTimeDetailsType254619C }
     * 
     */
    public DateAndTimeDetailsType254619C createDateAndTimeDetailsType254619C() {
        return new DateAndTimeDetailsType254619C();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationTypeI }
     * 
     */
    public DateAndTimeInformationTypeI createDateAndTimeInformationTypeI() {
        return new DateAndTimeInformationTypeI();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationType }
     * 
     */
    public DateAndTimeInformationType createDateAndTimeInformationType() {
        return new DateAndTimeInformationType();
    }

    /**
     * Create an instance of {@link DateAndTimeInformationType181295S }
     * 
     */
    public DateAndTimeInformationType181295S createDateAndTimeInformationType181295S() {
        return new DateAndTimeInformationType181295S();
    }

    /**
     * Create an instance of {@link DateTimePeriodDetailsTypeI }
     * 
     */
    public DateTimePeriodDetailsTypeI createDateTimePeriodDetailsTypeI() {
        return new DateTimePeriodDetailsTypeI();
    }

    /**
     * Create an instance of {@link DepartureLocationType }
     * 
     */
    public DepartureLocationType createDepartureLocationType() {
        return new DepartureLocationType();
    }

    /**
     * Create an instance of {@link FareDetailsType }
     * 
     */
    public FareDetailsType createFareDetailsType() {
        return new FareDetailsType();
    }

    /**
     * Create an instance of {@link FareFamilyCriteriaType }
     * 
     */
    public FareFamilyCriteriaType createFareFamilyCriteriaType() {
        return new FareFamilyCriteriaType();
    }

    /**
     * Create an instance of {@link FareFamilyDetailsType }
     * 
     */
    public FareFamilyDetailsType createFareFamilyDetailsType() {
        return new FareFamilyDetailsType();
    }

    /**
     * Create an instance of {@link FareFamilyType }
     * 
     */
    public FareFamilyType createFareFamilyType() {
        return new FareFamilyType();
    }

    /**
     * Create an instance of {@link FareInformationTypeI }
     * 
     */
    public FareInformationTypeI createFareInformationTypeI() {
        return new FareInformationTypeI();
    }

    /**
     * Create an instance of {@link FareProductDetailsType }
     * 
     */
    public FareProductDetailsType createFareProductDetailsType() {
        return new FareProductDetailsType();
    }

    /**
     * Create an instance of {@link FareQualifierInformationType }
     * 
     */
    public FareQualifierInformationType createFareQualifierInformationType() {
        return new FareQualifierInformationType();
    }

    /**
     * Create an instance of {@link FareTypeGroupingInformationType }
     * 
     */
    public FareTypeGroupingInformationType createFareTypeGroupingInformationType() {
        return new FareTypeGroupingInformationType();
    }

    /**
     * Create an instance of {@link FlightProductInformationType }
     * 
     */
    public FlightProductInformationType createFlightProductInformationType() {
        return new FlightProductInformationType();
    }

    /**
     * Create an instance of {@link FormOfPaymentDetailsTypeI }
     * 
     */
    public FormOfPaymentDetailsTypeI createFormOfPaymentDetailsTypeI() {
        return new FormOfPaymentDetailsTypeI();
    }

    /**
     * Create an instance of {@link FormOfPaymentTypeI }
     * 
     */
    public FormOfPaymentTypeI createFormOfPaymentTypeI() {
        return new FormOfPaymentTypeI();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link FrequencyType }
     * 
     */
    public FrequencyType createFrequencyType() {
        return new FrequencyType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationCodeType }
     * 
     */
    public FrequentTravellerIdentificationCodeType createFrequentTravellerIdentificationCodeType() {
        return new FrequentTravellerIdentificationCodeType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationCodeType177150S }
     * 
     */
    public FrequentTravellerIdentificationCodeType177150S createFrequentTravellerIdentificationCodeType177150S() {
        return new FrequentTravellerIdentificationCodeType177150S();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationType }
     * 
     */
    public FrequentTravellerIdentificationType createFrequentTravellerIdentificationType() {
        return new FrequentTravellerIdentificationType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationType249074C }
     * 
     */
    public FrequentTravellerIdentificationType249074C createFrequentTravellerIdentificationType249074C() {
        return new FrequentTravellerIdentificationType249074C();
    }

    /**
     * Create an instance of {@link HeaderInformationTypeI }
     * 
     */
    public HeaderInformationTypeI createHeaderInformationTypeI() {
        return new HeaderInformationTypeI();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationType }
     * 
     */
    public ItemNumberIdentificationType createItemNumberIdentificationType() {
        return new ItemNumberIdentificationType();
    }

    /**
     * Create an instance of {@link ItemNumberType }
     * 
     */
    public ItemNumberType createItemNumberType() {
        return new ItemNumberType();
    }

    /**
     * Create an instance of {@link ItemNumberType80866S }
     * 
     */
    public ItemNumberType80866S createItemNumberType80866S() {
        return new ItemNumberType80866S();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType }
     * 
     */
    public ItemReferencesAndVersionsType createItemReferencesAndVersionsType() {
        return new ItemReferencesAndVersionsType();
    }

    /**
     * Create an instance of {@link ItineraryDetailsType }
     * 
     */
    public ItineraryDetailsType createItineraryDetailsType() {
        return new ItineraryDetailsType();
    }

    /**
     * Create an instance of {@link LocationDetailsTypeI }
     * 
     */
    public LocationDetailsTypeI createLocationDetailsTypeI() {
        return new LocationDetailsTypeI();
    }

    /**
     * Create an instance of {@link LocationIdentificationDetailsType }
     * 
     */
    public LocationIdentificationDetailsType createLocationIdentificationDetailsType() {
        return new LocationIdentificationDetailsType();
    }

    /**
     * Create an instance of {@link LocationTypeI }
     * 
     */
    public LocationTypeI createLocationTypeI() {
        return new LocationTypeI();
    }

    /**
     * Create an instance of {@link MileageTimeDetailsTypeI }
     * 
     */
    public MileageTimeDetailsTypeI createMileageTimeDetailsTypeI() {
        return new MileageTimeDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryAndCabinInformationDetailsType }
     * 
     */
    public MonetaryAndCabinInformationDetailsType createMonetaryAndCabinInformationDetailsType() {
        return new MonetaryAndCabinInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryAndCabinInformationType }
     * 
     */
    public MonetaryAndCabinInformationType createMonetaryAndCabinInformationType() {
        return new MonetaryAndCabinInformationType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI }
     * 
     */
    public MonetaryInformationDetailsTypeI createMonetaryInformationDetailsTypeI() {
        return new MonetaryInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI194597C }
     * 
     */
    public MonetaryInformationDetailsTypeI194597C createMonetaryInformationDetailsTypeI194597C() {
        return new MonetaryInformationDetailsTypeI194597C();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI65140C }
     * 
     */
    public MonetaryInformationDetailsTypeI65140C createMonetaryInformationDetailsTypeI65140C() {
        return new MonetaryInformationDetailsTypeI65140C();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI65141C }
     * 
     */
    public MonetaryInformationDetailsTypeI65141C createMonetaryInformationDetailsTypeI65141C() {
        return new MonetaryInformationDetailsTypeI65141C();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI }
     * 
     */
    public MonetaryInformationTypeI createMonetaryInformationTypeI() {
        return new MonetaryInformationTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationType80162S }
     * 
     */
    public MonetaryInformationType80162S createMonetaryInformationType80162S() {
        return new MonetaryInformationType80162S();
    }

    /**
     * Create an instance of {@link MultiCityOptionType }
     * 
     */
    public MultiCityOptionType createMultiCityOptionType() {
        return new MultiCityOptionType();
    }

    /**
     * Create an instance of {@link MultipleIdentificationNumbersTypeI }
     * 
     */
    public MultipleIdentificationNumbersTypeI createMultipleIdentificationNumbersTypeI() {
        return new MultipleIdentificationNumbersTypeI();
    }

    /**
     * Create an instance of {@link NumberOfUnitDetailsTypeI }
     * 
     */
    public NumberOfUnitDetailsTypeI createNumberOfUnitDetailsTypeI() {
        return new NumberOfUnitDetailsTypeI();
    }

    /**
     * Create an instance of {@link NumberOfUnitDetailsType }
     * 
     */
    public NumberOfUnitDetailsType createNumberOfUnitDetailsType() {
        return new NumberOfUnitDetailsType();
    }

    /**
     * Create an instance of {@link NumberOfUnitDetailsType260583C }
     * 
     */
    public NumberOfUnitDetailsType260583C createNumberOfUnitDetailsType260583C() {
        return new NumberOfUnitDetailsType260583C();
    }

    /**
     * Create an instance of {@link NumberOfUnitsType80154S }
     * 
     */
    public NumberOfUnitsType80154S createNumberOfUnitsType80154S() {
        return new NumberOfUnitsType80154S();
    }

    /**
     * Create an instance of {@link OriginAndDestinationRequestType }
     * 
     */
    public OriginAndDestinationRequestType createOriginAndDestinationRequestType() {
        return new OriginAndDestinationRequestType();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsTypeI }
     * 
     */
    public OriginatorIdentificationDetailsTypeI createOriginatorIdentificationDetailsTypeI() {
        return new OriginatorIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link PNRSegmentReferenceType }
     * 
     */
    public PNRSegmentReferenceType createPNRSegmentReferenceType() {
        return new PNRSegmentReferenceType();
    }

    /**
     * Create an instance of {@link PassengerItineraryInformationType }
     * 
     */
    public PassengerItineraryInformationType createPassengerItineraryInformationType() {
        return new PassengerItineraryInformationType();
    }

    /**
     * Create an instance of {@link PricingTicketingDetailsType }
     * 
     */
    public PricingTicketingDetailsType createPricingTicketingDetailsType() {
        return new PricingTicketingDetailsType();
    }

    /**
     * Create an instance of {@link PricingTicketingInformationType }
     * 
     */
    public PricingTicketingInformationType createPricingTicketingInformationType() {
        return new PricingTicketingInformationType();
    }

    /**
     * Create an instance of {@link ProductDateTimeTypeI }
     * 
     */
    public ProductDateTimeTypeI createProductDateTimeTypeI() {
        return new ProductDateTimeTypeI();
    }

    /**
     * Create an instance of {@link ProductDateTimeTypeI194583C }
     * 
     */
    public ProductDateTimeTypeI194583C createProductDateTimeTypeI194583C() {
        return new ProductDateTimeTypeI194583C();
    }

    /**
     * Create an instance of {@link ProductDateTimeTypeI194598C }
     * 
     */
    public ProductDateTimeTypeI194598C createProductDateTimeTypeI194598C() {
        return new ProductDateTimeTypeI194598C();
    }

    /**
     * Create an instance of {@link ProductDateTimeType }
     * 
     */
    public ProductDateTimeType createProductDateTimeType() {
        return new ProductDateTimeType();
    }

    /**
     * Create an instance of {@link ProductDateTimeType195546C }
     * 
     */
    public ProductDateTimeType195546C createProductDateTimeType195546C() {
        return new ProductDateTimeType195546C();
    }

    /**
     * Create an instance of {@link ProductFacilitiesType }
     * 
     */
    public ProductFacilitiesType createProductFacilitiesType() {
        return new ProductFacilitiesType();
    }

    /**
     * Create an instance of {@link ProductIdentificationDetailsTypeI }
     * 
     */
    public ProductIdentificationDetailsTypeI createProductIdentificationDetailsTypeI() {
        return new ProductIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductIdentificationDetailsTypeI50878C }
     * 
     */
    public ProductIdentificationDetailsTypeI50878C createProductIdentificationDetailsTypeI50878C() {
        return new ProductIdentificationDetailsTypeI50878C();
    }

    /**
     * Create an instance of {@link ProductLocationDetailsTypeI }
     * 
     */
    public ProductLocationDetailsTypeI createProductLocationDetailsTypeI() {
        return new ProductLocationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsTypeI }
     * 
     */
    public ProductTypeDetailsTypeI createProductTypeDetailsTypeI() {
        return new ProductTypeDetailsTypeI();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsType }
     * 
     */
    public ProductTypeDetailsType createProductTypeDetailsType() {
        return new ProductTypeDetailsType();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsType120801C }
     * 
     */
    public ProductTypeDetailsType120801C createProductTypeDetailsType120801C() {
        return new ProductTypeDetailsType120801C();
    }

    /**
     * Create an instance of {@link ProductTypeDetailsType205137C }
     * 
     */
    public ProductTypeDetailsType205137C createProductTypeDetailsType205137C() {
        return new ProductTypeDetailsType205137C();
    }

    /**
     * Create an instance of {@link ProposedSegmentDetailsType }
     * 
     */
    public ProposedSegmentDetailsType createProposedSegmentDetailsType() {
        return new ProposedSegmentDetailsType();
    }

    /**
     * Create an instance of {@link ProposedSegmentType }
     * 
     */
    public ProposedSegmentType createProposedSegmentType() {
        return new ProposedSegmentType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link RoutingInformationTypeI }
     * 
     */
    public RoutingInformationTypeI createRoutingInformationTypeI() {
        return new RoutingInformationTypeI();
    }

    /**
     * Create an instance of {@link SegmentRepetitionControlDetailsTypeI }
     * 
     */
    public SegmentRepetitionControlDetailsTypeI createSegmentRepetitionControlDetailsTypeI() {
        return new SegmentRepetitionControlDetailsTypeI();
    }

    /**
     * Create an instance of {@link SegmentRepetitionControlTypeI }
     * 
     */
    public SegmentRepetitionControlTypeI createSegmentRepetitionControlTypeI() {
        return new SegmentRepetitionControlTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsInformationTypeI }
     * 
     */
    public SelectionDetailsInformationTypeI createSelectionDetailsInformationTypeI() {
        return new SelectionDetailsInformationTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsInformationType }
     * 
     */
    public SelectionDetailsInformationType createSelectionDetailsInformationType() {
        return new SelectionDetailsInformationType();
    }

    /**
     * Create an instance of {@link SelectionDetailsType }
     * 
     */
    public SelectionDetailsType createSelectionDetailsType() {
        return new SelectionDetailsType();
    }

    /**
     * Create an instance of {@link SpecialRequirementsDataDetailsType }
     * 
     */
    public SpecialRequirementsDataDetailsType createSpecialRequirementsDataDetailsType() {
        return new SpecialRequirementsDataDetailsType();
    }

    /**
     * Create an instance of {@link SpecialRequirementsDetailsType }
     * 
     */
    public SpecialRequirementsDetailsType createSpecialRequirementsDetailsType() {
        return new SpecialRequirementsDetailsType();
    }

    /**
     * Create an instance of {@link SpecialRequirementsTypeDetailsType }
     * 
     */
    public SpecialRequirementsTypeDetailsType createSpecialRequirementsTypeDetailsType() {
        return new SpecialRequirementsTypeDetailsType();
    }

    /**
     * Create an instance of {@link SpecificDataInformationType }
     * 
     */
    public SpecificDataInformationType createSpecificDataInformationType() {
        return new SpecificDataInformationType();
    }

    /**
     * Create an instance of {@link StationInformationTypeI }
     * 
     */
    public StationInformationTypeI createStationInformationTypeI() {
        return new StationInformationTypeI();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link StructuredPeriodInformationType }
     * 
     */
    public StructuredPeriodInformationType createStructuredPeriodInformationType() {
        return new StructuredPeriodInformationType();
    }

    /**
     * Create an instance of {@link TaxDetailsTypeI }
     * 
     */
    public TaxDetailsTypeI createTaxDetailsTypeI() {
        return new TaxDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

    /**
     * Create an instance of {@link TicketingPriceSchemeType }
     * 
     */
    public TicketingPriceSchemeType createTicketingPriceSchemeType() {
        return new TicketingPriceSchemeType();
    }

    /**
     * Create an instance of {@link TrafficRestrictionDetailsTypeI }
     * 
     */
    public TrafficRestrictionDetailsTypeI createTrafficRestrictionDetailsTypeI() {
        return new TrafficRestrictionDetailsTypeI();
    }

    /**
     * Create an instance of {@link TrafficRestrictionTypeI }
     * 
     */
    public TrafficRestrictionTypeI createTrafficRestrictionTypeI() {
        return new TrafficRestrictionTypeI();
    }

    /**
     * Create an instance of {@link TravelFlightInformationType }
     * 
     */
    public TravelFlightInformationType createTravelFlightInformationType() {
        return new TravelFlightInformationType();
    }

    /**
     * Create an instance of {@link TravelFlightInformationType165053S }
     * 
     */
    public TravelFlightInformationType165053S createTravelFlightInformationType165053S() {
        return new TravelFlightInformationType165053S();
    }

    /**
     * Create an instance of {@link TravelProductInformationTypeI }
     * 
     */
    public TravelProductInformationTypeI createTravelProductInformationTypeI() {
        return new TravelProductInformationTypeI();
    }

    /**
     * Create an instance of {@link TravelProductType }
     * 
     */
    public TravelProductType createTravelProductType() {
        return new TravelProductType();
    }

    /**
     * Create an instance of {@link TravellerDetailsType }
     * 
     */
    public TravellerDetailsType createTravellerDetailsType() {
        return new TravellerDetailsType();
    }

    /**
     * Create an instance of {@link UniqueIdDescriptionType }
     * 
     */
    public UniqueIdDescriptionType createUniqueIdDescriptionType() {
        return new UniqueIdDescriptionType();
    }

    /**
     * Create an instance of {@link UserIdentificationType }
     * 
     */
    public UserIdentificationType createUserIdentificationType() {
        return new UserIdentificationType();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.OfficeIdDetails.AirlineDistributionDetails }
     * 
     */
    public FareMasterPricerCalendar.OfficeIdDetails.AirlineDistributionDetails createFareMasterPricerCalendarOfficeIdDetailsAirlineDistributionDetails() {
        return new FareMasterPricerCalendar.OfficeIdDetails.AirlineDistributionDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FeeOption.FeeDetails.FeeDescriptionGrp }
     * 
     */
    public FareMasterPricerCalendar.FeeOption.FeeDetails.FeeDescriptionGrp createFareMasterPricerCalendarFeeOptionFeeDetailsFeeDescriptionGrp() {
        return new FareMasterPricerCalendar.FeeOption.FeeDetails.FeeDescriptionGrp();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.TicketChangeInfo.TicketRequestedSegments }
     * 
     */
    public FareMasterPricerCalendar.TicketChangeInfo.TicketRequestedSegments createFareMasterPricerCalendarTicketChangeInfoTicketRequestedSegments() {
        return new FareMasterPricerCalendar.TicketChangeInfo.TicketRequestedSegments();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.Itinerary.FlightInfoPNR.IncidentalStopInfo }
     * 
     */
    public FareMasterPricerCalendar.Itinerary.FlightInfoPNR.IncidentalStopInfo createFareMasterPricerCalendarItineraryFlightInfoPNRIncidentalStopInfo() {
        return new FareMasterPricerCalendar.Itinerary.FlightInfoPNR.IncidentalStopInfo();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.Itinerary.GroupOfFlights.FlightDetails }
     * 
     */
    public FareMasterPricerCalendar.Itinerary.GroupOfFlights.FlightDetails createFareMasterPricerCalendarItineraryGroupOfFlightsFlightDetails() {
        return new FareMasterPricerCalendar.Itinerary.GroupOfFlights.FlightDetails();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FareFamilies.FareFamilySegment }
     * 
     */
    public FareMasterPricerCalendar.FareFamilies.FareFamilySegment createFareMasterPricerCalendarFareFamiliesFareFamilySegment() {
        return new FareMasterPricerCalendar.FareFamilies.FareFamilySegment();
    }

    /**
     * Create an instance of {@link FareMasterPricerCalendar.FareFamilies.OtherPossibleCriteria.FareFamilySegment }
     * 
     */
    public FareMasterPricerCalendar.FareFamilies.OtherPossibleCriteria.FareFamilySegment createFareMasterPricerCalendarFareFamiliesOtherPossibleCriteriaFareFamilySegment() {
        return new FareMasterPricerCalendar.FareFamilies.OtherPossibleCriteria.FareFamilySegment();
    }

    /**
     * Create an instance of {@link GroupPassengerDetailsType.PsgDetailsInfo }
     * 
     */
    public GroupPassengerDetailsType.PsgDetailsInfo createGroupPassengerDetailsTypePsgDetailsInfo() {
        return new GroupPassengerDetailsType.PsgDetailsInfo();
    }

}
