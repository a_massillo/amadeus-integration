
package com.amadeus.xml.flires_07_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.flires_07_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.flires_07_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AirFlightInfoReply }
     * 
     */
    public AirFlightInfoReply createAirFlightInfoReply() {
        return new AirFlightInfoReply();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails createAirFlightInfoReplyFlightScheduleDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.InteractiveFreeText }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.InteractiveFreeText createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsInteractiveFreeText() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.InteractiveFreeText();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.EquipmentInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.EquipmentInfo createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsEquipmentInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.EquipmentInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.ProductInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.ProductInfo createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsProductInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.ProductInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsGeneralFlightInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData createAirFlightInfoReplyFlightScheduleDetailsErrorResponseOrWarningData() {
        return new AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.InteractiveFreeText }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.InteractiveFreeText createAirFlightInfoReplyFlightScheduleDetailsErrorResponseOrWarningDataInteractiveFreeText() {
        return new AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.InteractiveFreeText();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.ErrorInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.ErrorInfo createAirFlightInfoReplyFlightScheduleDetailsErrorResponseOrWarningDataErrorInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.ErrorInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.InteractiveFreeText }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.InteractiveFreeText createAirFlightInfoReplyFlightScheduleDetailsInteractiveFreeText() {
        return new AirFlightInfoReply.FlightScheduleDetails.InteractiveFreeText();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ProductInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ProductInfo createAirFlightInfoReplyFlightScheduleDetailsProductInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.ProductInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo createAirFlightInfoReplyFlightScheduleDetailsGeneralFlightInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.ResponseError }
     * 
     */
    public AirFlightInfoReply.ResponseError createAirFlightInfoReplyResponseError() {
        return new AirFlightInfoReply.ResponseError();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.ResponseError.InteractiveFreeText }
     * 
     */
    public AirFlightInfoReply.ResponseError.InteractiveFreeText createAirFlightInfoReplyResponseErrorInteractiveFreeText() {
        return new AirFlightInfoReply.ResponseError.InteractiveFreeText();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.ResponseError.ErrorInfo }
     * 
     */
    public AirFlightInfoReply.ResponseError.ErrorInfo createAirFlightInfoReplyResponseErrorErrorInfo() {
        return new AirFlightInfoReply.ResponseError.ErrorInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.InteractiveFreeText }
     * 
     */
    public AirFlightInfoReply.InteractiveFreeText createAirFlightInfoReplyInteractiveFreeText() {
        return new AirFlightInfoReply.InteractiveFreeText();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.MessageActionDetails }
     * 
     */
    public AirFlightInfoReply.MessageActionDetails createAirFlightInfoReplyMessageActionDetails() {
        return new AirFlightInfoReply.MessageActionDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.DummySegment }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.DummySegment createAirFlightInfoReplyFlightScheduleDetailsDummySegment() {
        return new AirFlightInfoReply.FlightScheduleDetails.DummySegment();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.InteractiveFreeText.FreeTextQualification }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.InteractiveFreeText.FreeTextQualification createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsInteractiveFreeTextFreeTextQualification() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.InteractiveFreeText.FreeTextQualification();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.EquipmentInfo.CabinClassDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.EquipmentInfo.CabinClassDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsEquipmentInfoCabinClassDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.EquipmentInfo.CabinClassDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.ProductInfo.BookingClassDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.ProductInfo.BookingClassDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsProductInfoBookingClassDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.ProductInfo.BookingClassDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.LegDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.LegDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetailsLegDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.LegDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.DepartureStationInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.DepartureStationInfo createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetailsDepartureStationInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.DepartureStationInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.ArrivalStationInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.ArrivalStationInfo createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetailsArrivalStationInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.ArrivalStationInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.FlightLegMileag }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.FlightLegMileag createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetailsFlightLegMileag() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.FlightLegMileag();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.TravellerTimeDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.TravellerTimeDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetailsTravellerTimeDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.TravellerTimeDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.FacilitiesInformation }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.FacilitiesInformation createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsAdditionalProductDetailsFacilitiesInformation() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.AdditionalProductDetails.FacilitiesInformation();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.FlightDate }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.FlightDate createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsGeneralFlightInfoFlightDate() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.FlightDate();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.BoardPointDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.BoardPointDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsGeneralFlightInfoBoardPointDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.BoardPointDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.OffPointDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.OffPointDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsGeneralFlightInfoOffPointDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.OffPointDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.CompanyDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.CompanyDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsGeneralFlightInfoCompanyDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.CompanyDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.ProductIdDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.ProductIdDetails createAirFlightInfoReplyFlightScheduleDetailsBoardPointAndOffPointDetailsGeneralFlightInfoProductIdDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.BoardPointAndOffPointDetails.GeneralFlightInfo.ProductIdDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.InteractiveFreeText.FreeTextQualification }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.InteractiveFreeText.FreeTextQualification createAirFlightInfoReplyFlightScheduleDetailsErrorResponseOrWarningDataInteractiveFreeTextFreeTextQualification() {
        return new AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.InteractiveFreeText.FreeTextQualification();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.ErrorInfo.ErrorDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.ErrorInfo.ErrorDetails createAirFlightInfoReplyFlightScheduleDetailsErrorResponseOrWarningDataErrorInfoErrorDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.ErrorResponseOrWarningData.ErrorInfo.ErrorDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.InteractiveFreeText.FreeTextQualification }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.InteractiveFreeText.FreeTextQualification createAirFlightInfoReplyFlightScheduleDetailsInteractiveFreeTextFreeTextQualification() {
        return new AirFlightInfoReply.FlightScheduleDetails.InteractiveFreeText.FreeTextQualification();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.ProductInfo.BookingClassDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.ProductInfo.BookingClassDetails createAirFlightInfoReplyFlightScheduleDetailsProductInfoBookingClassDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.ProductInfo.BookingClassDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.LegDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.LegDetails createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetailsLegDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.LegDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.DepartureStationInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.DepartureStationInfo createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetailsDepartureStationInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.DepartureStationInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.ArrivalStationInfo }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.ArrivalStationInfo createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetailsArrivalStationInfo() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.ArrivalStationInfo();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.FlightLegMileag }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.FlightLegMileag createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetailsFlightLegMileag() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.FlightLegMileag();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.TravellerTimeDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.TravellerTimeDetails createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetailsTravellerTimeDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.TravellerTimeDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.FacilitiesInformation }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.FacilitiesInformation createAirFlightInfoReplyFlightScheduleDetailsAdditionalProductDetailsFacilitiesInformation() {
        return new AirFlightInfoReply.FlightScheduleDetails.AdditionalProductDetails.FacilitiesInformation();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.FlightDate }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.FlightDate createAirFlightInfoReplyFlightScheduleDetailsGeneralFlightInfoFlightDate() {
        return new AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.FlightDate();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.BoardPointDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.BoardPointDetails createAirFlightInfoReplyFlightScheduleDetailsGeneralFlightInfoBoardPointDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.BoardPointDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.OffPointDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.OffPointDetails createAirFlightInfoReplyFlightScheduleDetailsGeneralFlightInfoOffPointDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.OffPointDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.CompanyDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.CompanyDetails createAirFlightInfoReplyFlightScheduleDetailsGeneralFlightInfoCompanyDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.CompanyDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.ProductIdDetails }
     * 
     */
    public AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.ProductIdDetails createAirFlightInfoReplyFlightScheduleDetailsGeneralFlightInfoProductIdDetails() {
        return new AirFlightInfoReply.FlightScheduleDetails.GeneralFlightInfo.ProductIdDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.ResponseError.InteractiveFreeText.FreeTextQualification }
     * 
     */
    public AirFlightInfoReply.ResponseError.InteractiveFreeText.FreeTextQualification createAirFlightInfoReplyResponseErrorInteractiveFreeTextFreeTextQualification() {
        return new AirFlightInfoReply.ResponseError.InteractiveFreeText.FreeTextQualification();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.ResponseError.ErrorInfo.ErrorDetails }
     * 
     */
    public AirFlightInfoReply.ResponseError.ErrorInfo.ErrorDetails createAirFlightInfoReplyResponseErrorErrorInfoErrorDetails() {
        return new AirFlightInfoReply.ResponseError.ErrorInfo.ErrorDetails();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.InteractiveFreeText.FreeTextQualification }
     * 
     */
    public AirFlightInfoReply.InteractiveFreeText.FreeTextQualification createAirFlightInfoReplyInteractiveFreeTextFreeTextQualification() {
        return new AirFlightInfoReply.InteractiveFreeText.FreeTextQualification();
    }

    /**
     * Create an instance of {@link AirFlightInfoReply.MessageActionDetails.MessageFunctionDetails }
     * 
     */
    public AirFlightInfoReply.MessageActionDetails.MessageFunctionDetails createAirFlightInfoReplyMessageActionDetailsMessageFunctionDetails() {
        return new AirFlightInfoReply.MessageActionDetails.MessageFunctionDetails();
    }

}
