
package com.amadeus.xml.trfsrq_14_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.trfsrq_14_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.trfsrq_14_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocRefundInitRefund }
     * 
     */
    public DocRefundInitRefund createDocRefundInitRefund() {
        return new DocRefundInitRefund();
    }

    /**
     * Create an instance of {@link DocRefundInitRefund.TicketNumberGroup }
     * 
     */
    public DocRefundInitRefund.TicketNumberGroup createDocRefundInitRefundTicketNumberGroup() {
        return new DocRefundInitRefund.TicketNumberGroup();
    }

    /**
     * Create an instance of {@link StatusTypeI }
     * 
     */
    public StatusTypeI createStatusTypeI() {
        return new StatusTypeI();
    }

    /**
     * Create an instance of {@link CurrenciesType }
     * 
     */
    public CurrenciesType createCurrenciesType() {
        return new CurrenciesType();
    }

    /**
     * Create an instance of {@link DocRefundInitRefund.ItemNumberGroup }
     * 
     */
    public DocRefundInitRefund.ItemNumberGroup createDocRefundInitRefundItemNumberGroup() {
        return new DocRefundInitRefund.ItemNumberGroup();
    }

    /**
     * Create an instance of {@link StockRangeInformationType }
     * 
     */
    public StockRangeInformationType createStockRangeInformationType() {
        return new StockRangeInformationType();
    }

    /**
     * Create an instance of {@link AdditionalBusinessSourceInformationType }
     * 
     */
    public AdditionalBusinessSourceInformationType createAdditionalBusinessSourceInformationType() {
        return new AdditionalBusinessSourceInformationType();
    }

    /**
     * Create an instance of {@link TransactionContextType }
     * 
     */
    public TransactionContextType createTransactionContextType() {
        return new TransactionContextType();
    }

    /**
     * Create an instance of {@link CouponInformationDetailsTypeI }
     * 
     */
    public CouponInformationDetailsTypeI createCouponInformationDetailsTypeI() {
        return new CouponInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link CouponInformationTypeI }
     * 
     */
    public CouponInformationTypeI createCouponInformationTypeI() {
        return new CouponInformationTypeI();
    }

    /**
     * Create an instance of {@link CurrencyDetailsTypeU }
     * 
     */
    public CurrencyDetailsTypeU createCurrencyDetailsTypeU() {
        return new CurrencyDetailsTypeU();
    }

    /**
     * Create an instance of {@link ItemNumberIdentificationTypeI }
     * 
     */
    public ItemNumberIdentificationTypeI createItemNumberIdentificationTypeI() {
        return new ItemNumberIdentificationTypeI();
    }

    /**
     * Create an instance of {@link ItemNumberTypeI }
     * 
     */
    public ItemNumberTypeI createItemNumberTypeI() {
        return new ItemNumberTypeI();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsTypeI }
     * 
     */
    public OriginatorIdentificationDetailsTypeI createOriginatorIdentificationDetailsTypeI() {
        return new OriginatorIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link StatusDetailsTypeI }
     * 
     */
    public StatusDetailsTypeI createStatusDetailsTypeI() {
        return new StatusDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

}
