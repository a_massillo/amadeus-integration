
package com.amadeus.xml.trfsrq_14_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ticketNumberGroup" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="documentNumberDetails" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}TicketNumberTypeI"/&gt;
 *                   &lt;element name="paperCouponOfDocNumber" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}CouponInformationTypeI" maxOccurs="16" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="actionDetails" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}StatusTypeI" minOccurs="0"/&gt;
 *         &lt;element name="currencyOverride" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}CurrenciesType" minOccurs="0"/&gt;
 *         &lt;element name="itemNumberGroup" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="sequenceNumber" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}ItemNumberTypeI"/&gt;
 *                   &lt;element name="paperCouponOfItemNumber" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}CouponInformationTypeI" maxOccurs="16" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="stockProviderDetails" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}StockRangeInformationType" minOccurs="0"/&gt;
 *         &lt;element name="targetOfficeDetails" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}AdditionalBusinessSourceInformationType" minOccurs="0"/&gt;
 *         &lt;element name="transactionContext" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}TransactionContextType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ticketNumberGroup",
    "actionDetails",
    "currencyOverride",
    "itemNumberGroup",
    "stockProviderDetails",
    "targetOfficeDetails",
    "transactionContext"
})
@XmlRootElement(name = "DocRefund_InitRefund")
public class DocRefundInitRefund {

    protected DocRefundInitRefund.TicketNumberGroup ticketNumberGroup;
    protected StatusTypeI actionDetails;
    protected CurrenciesType currencyOverride;
    protected DocRefundInitRefund.ItemNumberGroup itemNumberGroup;
    protected StockRangeInformationType stockProviderDetails;
    protected AdditionalBusinessSourceInformationType targetOfficeDetails;
    protected TransactionContextType transactionContext;

    /**
     * Gets the value of the ticketNumberGroup property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundInitRefund.TicketNumberGroup }
     *     
     */
    public DocRefundInitRefund.TicketNumberGroup getTicketNumberGroup() {
        return ticketNumberGroup;
    }

    /**
     * Sets the value of the ticketNumberGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundInitRefund.TicketNumberGroup }
     *     
     */
    public void setTicketNumberGroup(DocRefundInitRefund.TicketNumberGroup value) {
        this.ticketNumberGroup = value;
    }

    /**
     * Gets the value of the actionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link StatusTypeI }
     *     
     */
    public StatusTypeI getActionDetails() {
        return actionDetails;
    }

    /**
     * Sets the value of the actionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusTypeI }
     *     
     */
    public void setActionDetails(StatusTypeI value) {
        this.actionDetails = value;
    }

    /**
     * Gets the value of the currencyOverride property.
     * 
     * @return
     *     possible object is
     *     {@link CurrenciesType }
     *     
     */
    public CurrenciesType getCurrencyOverride() {
        return currencyOverride;
    }

    /**
     * Sets the value of the currencyOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrenciesType }
     *     
     */
    public void setCurrencyOverride(CurrenciesType value) {
        this.currencyOverride = value;
    }

    /**
     * Gets the value of the itemNumberGroup property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundInitRefund.ItemNumberGroup }
     *     
     */
    public DocRefundInitRefund.ItemNumberGroup getItemNumberGroup() {
        return itemNumberGroup;
    }

    /**
     * Sets the value of the itemNumberGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundInitRefund.ItemNumberGroup }
     *     
     */
    public void setItemNumberGroup(DocRefundInitRefund.ItemNumberGroup value) {
        this.itemNumberGroup = value;
    }

    /**
     * Gets the value of the stockProviderDetails property.
     * 
     * @return
     *     possible object is
     *     {@link StockRangeInformationType }
     *     
     */
    public StockRangeInformationType getStockProviderDetails() {
        return stockProviderDetails;
    }

    /**
     * Sets the value of the stockProviderDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link StockRangeInformationType }
     *     
     */
    public void setStockProviderDetails(StockRangeInformationType value) {
        this.stockProviderDetails = value;
    }

    /**
     * Gets the value of the targetOfficeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalBusinessSourceInformationType }
     *     
     */
    public AdditionalBusinessSourceInformationType getTargetOfficeDetails() {
        return targetOfficeDetails;
    }

    /**
     * Sets the value of the targetOfficeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalBusinessSourceInformationType }
     *     
     */
    public void setTargetOfficeDetails(AdditionalBusinessSourceInformationType value) {
        this.targetOfficeDetails = value;
    }

    /**
     * Gets the value of the transactionContext property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionContextType }
     *     
     */
    public TransactionContextType getTransactionContext() {
        return transactionContext;
    }

    /**
     * Sets the value of the transactionContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionContextType }
     *     
     */
    public void setTransactionContext(TransactionContextType value) {
        this.transactionContext = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="sequenceNumber" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}ItemNumberTypeI"/&gt;
     *         &lt;element name="paperCouponOfItemNumber" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}CouponInformationTypeI" maxOccurs="16" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sequenceNumber",
        "paperCouponOfItemNumber"
    })
    public static class ItemNumberGroup {

        @XmlElement(required = true)
        protected ItemNumberTypeI sequenceNumber;
        protected List<CouponInformationTypeI> paperCouponOfItemNumber;

        /**
         * Gets the value of the sequenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link ItemNumberTypeI }
         *     
         */
        public ItemNumberTypeI getSequenceNumber() {
            return sequenceNumber;
        }

        /**
         * Sets the value of the sequenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemNumberTypeI }
         *     
         */
        public void setSequenceNumber(ItemNumberTypeI value) {
            this.sequenceNumber = value;
        }

        /**
         * Gets the value of the paperCouponOfItemNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paperCouponOfItemNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPaperCouponOfItemNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CouponInformationTypeI }
         * 
         * 
         */
        public List<CouponInformationTypeI> getPaperCouponOfItemNumber() {
            if (paperCouponOfItemNumber == null) {
                paperCouponOfItemNumber = new ArrayList<CouponInformationTypeI>();
            }
            return this.paperCouponOfItemNumber;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="documentNumberDetails" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}TicketNumberTypeI"/&gt;
     *         &lt;element name="paperCouponOfDocNumber" type="{http://xml.amadeus.com/TRFSRQ_14_1_1A}CouponInformationTypeI" maxOccurs="16" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "documentNumberDetails",
        "paperCouponOfDocNumber"
    })
    public static class TicketNumberGroup {

        @XmlElement(required = true)
        protected TicketNumberTypeI documentNumberDetails;
        protected List<CouponInformationTypeI> paperCouponOfDocNumber;

        /**
         * Gets the value of the documentNumberDetails property.
         * 
         * @return
         *     possible object is
         *     {@link TicketNumberTypeI }
         *     
         */
        public TicketNumberTypeI getDocumentNumberDetails() {
            return documentNumberDetails;
        }

        /**
         * Sets the value of the documentNumberDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link TicketNumberTypeI }
         *     
         */
        public void setDocumentNumberDetails(TicketNumberTypeI value) {
            this.documentNumberDetails = value;
        }

        /**
         * Gets the value of the paperCouponOfDocNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paperCouponOfDocNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPaperCouponOfDocNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CouponInformationTypeI }
         * 
         * 
         */
        public List<CouponInformationTypeI> getPaperCouponOfDocNumber() {
            if (paperCouponOfDocNumber == null) {
                paperCouponOfDocNumber = new ArrayList<CouponInformationTypeI>();
            }
            return this.paperCouponOfDocNumber;
        }

    }

}
