
package com.amadeus.xml.tmdsiq_15_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.tmdsiq_15_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.tmdsiq_15_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocIssuanceIssueMiscellaneousDocuments }
     * 
     */
    public DocIssuanceIssueMiscellaneousDocuments createDocIssuanceIssueMiscellaneousDocuments() {
        return new DocIssuanceIssueMiscellaneousDocuments();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferenceInformationType }
     * 
     */
    public ReferenceInformationType createReferenceInformationType() {
        return new ReferenceInformationType();
    }

    /**
     * Create an instance of {@link StockInformationType }
     * 
     */
    public StockInformationType createStockInformationType() {
        return new StockInformationType();
    }

    /**
     * Create an instance of {@link DocIssuanceIssueMiscellaneousDocuments.OptionGroup }
     * 
     */
    public DocIssuanceIssueMiscellaneousDocuments.OptionGroup createDocIssuanceIssueMiscellaneousDocumentsOptionGroup() {
        return new DocIssuanceIssueMiscellaneousDocuments.OptionGroup();
    }

    /**
     * Create an instance of {@link TravellerInformationType }
     * 
     */
    public TravellerInformationType createTravellerInformationType() {
        return new TravellerInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeType }
     * 
     */
    public CodedAttributeType createCodedAttributeType() {
        return new CodedAttributeType();
    }

    /**
     * Create an instance of {@link AttributeInformationTypeU }
     * 
     */
    public AttributeInformationTypeU createAttributeInformationTypeU() {
        return new AttributeInformationTypeU();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType }
     * 
     */
    public CodedAttributeInformationType createCodedAttributeInformationType() {
        return new CodedAttributeInformationType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType }
     * 
     */
    public ReferencingDetailsType createReferencingDetailsType() {
        return new ReferencingDetailsType();
    }

    /**
     * Create an instance of {@link ReferencingDetailsType108978C }
     * 
     */
    public ReferencingDetailsType108978C createReferencingDetailsType108978C() {
        return new ReferencingDetailsType108978C();
    }

    /**
     * Create an instance of {@link StatusDetailsTypeI }
     * 
     */
    public StatusDetailsTypeI createStatusDetailsTypeI() {
        return new StatusDetailsTypeI();
    }

    /**
     * Create an instance of {@link StatusTypeI }
     * 
     */
    public StatusTypeI createStatusTypeI() {
        return new StatusTypeI();
    }

    /**
     * Create an instance of {@link StockTicketNumberDetailsType }
     * 
     */
    public StockTicketNumberDetailsType createStockTicketNumberDetailsType() {
        return new StockTicketNumberDetailsType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType }
     * 
     */
    public StructuredDateTimeInformationType createStructuredDateTimeInformationType() {
        return new StructuredDateTimeInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link TravellerSurnameInformationType }
     * 
     */
    public TravellerSurnameInformationType createTravellerSurnameInformationType() {
        return new TravellerSurnameInformationType();
    }

}
