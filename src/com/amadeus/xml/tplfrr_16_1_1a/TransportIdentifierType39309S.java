
package com.amadeus.xml.tplfrr_16_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify the transport service(s) which is /are to be updated or cancelled
 * 
 * <p>Java class for TransportIdentifierType_39309S complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransportIdentifierType_39309S"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="carrierInformation" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}CompanyIdentificationTypeI_66149C" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransportIdentifierType_39309S", propOrder = {
    "carrierInformation"
})
public class TransportIdentifierType39309S {

    protected CompanyIdentificationTypeI66149C carrierInformation;

    /**
     * Gets the value of the carrierInformation property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyIdentificationTypeI66149C }
     *     
     */
    public CompanyIdentificationTypeI66149C getCarrierInformation() {
        return carrierInformation;
    }

    /**
     * Sets the value of the carrierInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyIdentificationTypeI66149C }
     *     
     */
    public void setCarrierInformation(CompanyIdentificationTypeI66149C value) {
        this.carrierInformation = value;
    }

}
