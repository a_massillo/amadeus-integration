
package com.amadeus.xml.tplfrr_16_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify details related to a product.
 * 
 * <p>Java class for TravelProductInformationTypeI_39317S complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TravelProductInformationTypeI_39317S"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="departureCity" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}LocationTypeI_66170C" minOccurs="0"/&gt;
 *         &lt;element name="arrivalCity" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}LocationTypeI_66170C" minOccurs="0"/&gt;
 *         &lt;element name="airlineDetail" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}CompanyIdentificationTypeI_66149C" minOccurs="0"/&gt;
 *         &lt;element name="segmentDetail" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}ProductIdentificationDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="ticketingStatus" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}AlphaNumericString_Length1To2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelProductInformationTypeI_39317S", propOrder = {
    "departureCity",
    "arrivalCity",
    "airlineDetail",
    "segmentDetail",
    "ticketingStatus"
})
public class TravelProductInformationTypeI39317S {

    protected LocationTypeI66170C departureCity;
    protected LocationTypeI66170C arrivalCity;
    protected CompanyIdentificationTypeI66149C airlineDetail;
    protected ProductIdentificationDetailsTypeI segmentDetail;
    protected String ticketingStatus;

    /**
     * Gets the value of the departureCity property.
     * 
     * @return
     *     possible object is
     *     {@link LocationTypeI66170C }
     *     
     */
    public LocationTypeI66170C getDepartureCity() {
        return departureCity;
    }

    /**
     * Sets the value of the departureCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationTypeI66170C }
     *     
     */
    public void setDepartureCity(LocationTypeI66170C value) {
        this.departureCity = value;
    }

    /**
     * Gets the value of the arrivalCity property.
     * 
     * @return
     *     possible object is
     *     {@link LocationTypeI66170C }
     *     
     */
    public LocationTypeI66170C getArrivalCity() {
        return arrivalCity;
    }

    /**
     * Sets the value of the arrivalCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationTypeI66170C }
     *     
     */
    public void setArrivalCity(LocationTypeI66170C value) {
        this.arrivalCity = value;
    }

    /**
     * Gets the value of the airlineDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyIdentificationTypeI66149C }
     *     
     */
    public CompanyIdentificationTypeI66149C getAirlineDetail() {
        return airlineDetail;
    }

    /**
     * Sets the value of the airlineDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyIdentificationTypeI66149C }
     *     
     */
    public void setAirlineDetail(CompanyIdentificationTypeI66149C value) {
        this.airlineDetail = value;
    }

    /**
     * Gets the value of the segmentDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentificationDetailsTypeI }
     *     
     */
    public ProductIdentificationDetailsTypeI getSegmentDetail() {
        return segmentDetail;
    }

    /**
     * Sets the value of the segmentDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentificationDetailsTypeI }
     *     
     */
    public void setSegmentDetail(ProductIdentificationDetailsTypeI value) {
        this.segmentDetail = value;
    }

    /**
     * Gets the value of the ticketingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingStatus() {
        return ticketingStatus;
    }

    /**
     * Sets the value of the ticketingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingStatus(String value) {
        this.ticketingStatus = value;
    }

}
