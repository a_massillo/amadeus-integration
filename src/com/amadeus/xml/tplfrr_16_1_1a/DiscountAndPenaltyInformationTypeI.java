
package com.amadeus.xml.tplfrr_16_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify information about discounts and penalties
 * 
 * <p>Java class for DiscountAndPenaltyInformationTypeI complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiscountAndPenaltyInformationTypeI"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="penDisData" type="{http://xml.amadeus.com/TPLFRR_16_1_1A}DiscountPenaltyMonetaryInformationTypeI" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscountAndPenaltyInformationTypeI", propOrder = {
    "penDisData"
})
public class DiscountAndPenaltyInformationTypeI {

    protected DiscountPenaltyMonetaryInformationTypeI penDisData;

    /**
     * Gets the value of the penDisData property.
     * 
     * @return
     *     possible object is
     *     {@link DiscountPenaltyMonetaryInformationTypeI }
     *     
     */
    public DiscountPenaltyMonetaryInformationTypeI getPenDisData() {
        return penDisData;
    }

    /**
     * Sets the value of the penDisData property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscountPenaltyMonetaryInformationTypeI }
     *     
     */
    public void setPenDisData(DiscountPenaltyMonetaryInformationTypeI value) {
        this.penDisData = value;
    }

}
