
package com.amadeus.xml.trfuur_14_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To provide free form or coded text information.
 * 
 * <p>Java class for InteractiveFreeTextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InteractiveFreeTextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="freeTextQualification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FreeTextQualificationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="freeText" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}AlphaNumericString_Length1To126"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InteractiveFreeTextType", propOrder = {
    "freeTextQualification",
    "freeText"
})
public class InteractiveFreeTextType {

    protected FreeTextQualificationTypeI freeTextQualification;
    @XmlElement(required = true)
    protected String freeText;

    /**
     * Gets the value of the freeTextQualification property.
     * 
     * @return
     *     possible object is
     *     {@link FreeTextQualificationTypeI }
     *     
     */
    public FreeTextQualificationTypeI getFreeTextQualification() {
        return freeTextQualification;
    }

    /**
     * Sets the value of the freeTextQualification property.
     * 
     * @param value
     *     allowed object is
     *     {@link FreeTextQualificationTypeI }
     *     
     */
    public void setFreeTextQualification(FreeTextQualificationTypeI value) {
        this.freeTextQualification = value;
    }

    /**
     * Gets the value of the freeText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeText() {
        return freeText;
    }

    /**
     * Sets the value of the freeText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeText(String value) {
        this.freeText = value;
    }

}
