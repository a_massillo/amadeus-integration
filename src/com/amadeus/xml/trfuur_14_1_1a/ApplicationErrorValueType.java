
package com.amadeus.xml.trfuur_14_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * To identify the data in error and the erroneous value.
 * 
 * <p>Java class for ApplicationErrorValueType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationErrorValueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataLabel" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}AlphaNumericString_Length1To10" minOccurs="0"/&gt;
 *         &lt;element name="occurenceNumber" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}AlphaNumericString_Length1To35" minOccurs="0"/&gt;
 *         &lt;element name="dataValue" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}AlphaNumericString_Length1To70" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationErrorValueType", propOrder = {
    "dataLabel",
    "occurenceNumber",
    "dataValue"
})
public class ApplicationErrorValueType {

    protected String dataLabel;
    protected String occurenceNumber;
    protected String dataValue;

    /**
     * Gets the value of the dataLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataLabel() {
        return dataLabel;
    }

    /**
     * Sets the value of the dataLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataLabel(String value) {
        this.dataLabel = value;
    }

    /**
     * Gets the value of the occurenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccurenceNumber() {
        return occurenceNumber;
    }

    /**
     * Sets the value of the occurenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccurenceNumber(String value) {
        this.occurenceNumber = value;
    }

    /**
     * Gets the value of the dataValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataValue() {
        return dataValue;
    }

    /**
     * Sets the value of the dataValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataValue(String value) {
        this.dataValue = value;
    }

}
