
package com.amadeus.xml.trfuur_14_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.trfuur_14_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.trfuur_14_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply }
     * 
     */
    public DocRefundUpdateRefundReply createDocRefundUpdateRefundReply() {
        return new DocRefundUpdateRefundReply();
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply.RefundableDocument }
     * 
     */
    public DocRefundUpdateRefundReply.RefundableDocument createDocRefundUpdateRefundReplyRefundableDocument() {
        return new DocRefundUpdateRefundReply.RefundableDocument();
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply.RefundableDocument.FeeGroup }
     * 
     */
    public DocRefundUpdateRefundReply.RefundableDocument.FeeGroup createDocRefundUpdateRefundReplyRefundableDocumentFeeGroup() {
        return new DocRefundUpdateRefundReply.RefundableDocument.FeeGroup();
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply.RefundableDocument.Ticket }
     * 
     */
    public DocRefundUpdateRefundReply.RefundableDocument.Ticket createDocRefundUpdateRefundReplyRefundableDocumentTicket() {
        return new DocRefundUpdateRefundReply.RefundableDocument.Ticket();
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply.ApplicationErrorGroup }
     * 
     */
    public DocRefundUpdateRefundReply.ApplicationErrorGroup createDocRefundUpdateRefundReplyApplicationErrorGroup() {
        return new DocRefundUpdateRefundReply.ApplicationErrorGroup();
    }

    /**
     * Create an instance of {@link ActionIdentificationType }
     * 
     */
    public ActionIdentificationType createActionIdentificationType() {
        return new ActionIdentificationType();
    }

    /**
     * Create an instance of {@link ApplicationErrorValueType }
     * 
     */
    public ApplicationErrorValueType createApplicationErrorValueType() {
        return new ApplicationErrorValueType();
    }

    /**
     * Create an instance of {@link CodedAttributeInformationType }
     * 
     */
    public CodedAttributeInformationType createCodedAttributeInformationType() {
        return new CodedAttributeInformationType();
    }

    /**
     * Create an instance of {@link CodedAttributeType }
     * 
     */
    public CodedAttributeType createCodedAttributeType() {
        return new CodedAttributeType();
    }

    /**
     * Create an instance of {@link CommissionDetailsType }
     * 
     */
    public CommissionDetailsType createCommissionDetailsType() {
        return new CommissionDetailsType();
    }

    /**
     * Create an instance of {@link CommissionInformationType }
     * 
     */
    public CommissionInformationType createCommissionInformationType() {
        return new CommissionInformationType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link CouponInformationDetailsTypeI }
     * 
     */
    public CouponInformationDetailsTypeI createCouponInformationDetailsTypeI() {
        return new CouponInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link CouponInformationTypeI }
     * 
     */
    public CouponInformationTypeI createCouponInformationTypeI() {
        return new CouponInformationTypeI();
    }

    /**
     * Create an instance of {@link DataInformationTypeI }
     * 
     */
    public DataInformationTypeI createDataInformationTypeI() {
        return new DataInformationTypeI();
    }

    /**
     * Create an instance of {@link DataTypeInformationTypeI }
     * 
     */
    public DataTypeInformationTypeI createDataTypeInformationTypeI() {
        return new DataTypeInformationTypeI();
    }

    /**
     * Create an instance of {@link DeparturePlanEventType }
     * 
     */
    public DeparturePlanEventType createDeparturePlanEventType() {
        return new DeparturePlanEventType();
    }

    /**
     * Create an instance of {@link EventReferenceType }
     * 
     */
    public EventReferenceType createEventReferenceType() {
        return new EventReferenceType();
    }

    /**
     * Create an instance of {@link FOPRepresentationType }
     * 
     */
    public FOPRepresentationType createFOPRepresentationType() {
        return new FOPRepresentationType();
    }

    /**
     * Create an instance of {@link FormOfPaymentDetailsTypeI }
     * 
     */
    public FormOfPaymentDetailsTypeI createFormOfPaymentDetailsTypeI() {
        return new FormOfPaymentDetailsTypeI();
    }

    /**
     * Create an instance of {@link FormOfPaymentTypeI }
     * 
     */
    public FormOfPaymentTypeI createFormOfPaymentTypeI() {
        return new FormOfPaymentTypeI();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link FreeTextQualificationTypeI }
     * 
     */
    public FreeTextQualificationTypeI createFreeTextQualificationTypeI() {
        return new FreeTextQualificationTypeI();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationCodeType }
     * 
     */
    public FrequentTravellerIdentificationCodeType createFrequentTravellerIdentificationCodeType() {
        return new FrequentTravellerIdentificationCodeType();
    }

    /**
     * Create an instance of {@link FrequentTravellerIdentificationType }
     * 
     */
    public FrequentTravellerIdentificationType createFrequentTravellerIdentificationType() {
        return new FrequentTravellerIdentificationType();
    }

    /**
     * Create an instance of {@link InteractiveFreeTextType }
     * 
     */
    public InteractiveFreeTextType createInteractiveFreeTextType() {
        return new InteractiveFreeTextType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsTypeI }
     * 
     */
    public MonetaryInformationDetailsTypeI createMonetaryInformationDetailsTypeI() {
        return new MonetaryInformationDetailsTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType }
     * 
     */
    public MonetaryInformationDetailsType createMonetaryInformationDetailsType() {
        return new MonetaryInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryInformationTypeI }
     * 
     */
    public MonetaryInformationTypeI createMonetaryInformationTypeI() {
        return new MonetaryInformationTypeI();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI }
     * 
     */
    public OriginAndDestinationDetailsTypeI createOriginAndDestinationDetailsTypeI() {
        return new OriginAndDestinationDetailsTypeI();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI67857S }
     * 
     */
    public OriginAndDestinationDetailsTypeI67857S createOriginAndDestinationDetailsTypeI67857S() {
        return new OriginAndDestinationDetailsTypeI67857S();
    }

    /**
     * Create an instance of {@link OriginatorIdentificationDetailsTypeI }
     * 
     */
    public OriginatorIdentificationDetailsTypeI createOriginatorIdentificationDetailsTypeI() {
        return new OriginatorIdentificationDetailsTypeI();
    }

    /**
     * Create an instance of {@link PricingTicketingDetailsTypeI }
     * 
     */
    public PricingTicketingDetailsTypeI createPricingTicketingDetailsTypeI() {
        return new PricingTicketingDetailsTypeI();
    }

    /**
     * Create an instance of {@link PricingTicketingInformationTypeI }
     * 
     */
    public PricingTicketingInformationTypeI createPricingTicketingInformationTypeI() {
        return new PricingTicketingInformationTypeI();
    }

    /**
     * Create an instance of {@link PriorityDetailsTypeU }
     * 
     */
    public PriorityDetailsTypeU createPriorityDetailsTypeU() {
        return new PriorityDetailsTypeU();
    }

    /**
     * Create an instance of {@link PriorityTypeU }
     * 
     */
    public PriorityTypeU createPriorityTypeU() {
        return new PriorityTypeU();
    }

    /**
     * Create an instance of {@link ProductLocationDetailsTypeI }
     * 
     */
    public ProductLocationDetailsTypeI createProductLocationDetailsTypeI() {
        return new ProductLocationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferenceInfoType }
     * 
     */
    public ReferenceInfoType createReferenceInfoType() {
        return new ReferenceInfoType();
    }

    /**
     * Create an instance of {@link ReferenceInfoType57269S }
     * 
     */
    public ReferenceInfoType57269S createReferenceInfoType57269S() {
        return new ReferenceInfoType57269S();
    }

    /**
     * Create an instance of {@link ReferenceInformationTypeI }
     * 
     */
    public ReferenceInformationTypeI createReferenceInformationTypeI() {
        return new ReferenceInformationTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI }
     * 
     */
    public ReferencingDetailsTypeI createReferencingDetailsTypeI() {
        return new ReferencingDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI87874C }
     * 
     */
    public ReferencingDetailsTypeI87874C createReferencingDetailsTypeI87874C() {
        return new ReferencingDetailsTypeI87874C();
    }

    /**
     * Create an instance of {@link RefundedItineraryType }
     * 
     */
    public RefundedItineraryType createRefundedItineraryType() {
        return new RefundedItineraryType();
    }

    /**
     * Create an instance of {@link RoutingInformationTypeI }
     * 
     */
    public RoutingInformationTypeI createRoutingInformationTypeI() {
        return new RoutingInformationTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsInformationTypeI }
     * 
     */
    public SelectionDetailsInformationTypeI createSelectionDetailsInformationTypeI() {
        return new SelectionDetailsInformationTypeI();
    }

    /**
     * Create an instance of {@link SelectionDetailsTypeI }
     * 
     */
    public SelectionDetailsTypeI createSelectionDetailsTypeI() {
        return new SelectionDetailsTypeI();
    }

    /**
     * Create an instance of {@link SpecificDataInformationTypeI }
     * 
     */
    public SpecificDataInformationTypeI createSpecificDataInformationTypeI() {
        return new SpecificDataInformationTypeI();
    }

    /**
     * Create an instance of {@link StructuredAddressInformationType }
     * 
     */
    public StructuredAddressInformationType createStructuredAddressInformationType() {
        return new StructuredAddressInformationType();
    }

    /**
     * Create an instance of {@link StructuredAddressType }
     * 
     */
    public StructuredAddressType createStructuredAddressType() {
        return new StructuredAddressType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType }
     * 
     */
    public StructuredDateTimeInformationType createStructuredDateTimeInformationType() {
        return new StructuredDateTimeInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link StructuredPeriodInformationType }
     * 
     */
    public StructuredPeriodInformationType createStructuredPeriodInformationType() {
        return new StructuredPeriodInformationType();
    }

    /**
     * Create an instance of {@link TaxDetailsTypeI }
     * 
     */
    public TaxDetailsTypeI createTaxDetailsTypeI() {
        return new TaxDetailsTypeI();
    }

    /**
     * Create an instance of {@link TaxDetailsTypeI89138C }
     * 
     */
    public TaxDetailsTypeI89138C createTaxDetailsTypeI89138C() {
        return new TaxDetailsTypeI89138C();
    }

    /**
     * Create an instance of {@link TaxTypeI }
     * 
     */
    public TaxTypeI createTaxTypeI() {
        return new TaxTypeI();
    }

    /**
     * Create an instance of {@link TaxType }
     * 
     */
    public TaxType createTaxType() {
        return new TaxType();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsTypeI }
     * 
     */
    public TicketNumberDetailsTypeI createTicketNumberDetailsTypeI() {
        return new TicketNumberDetailsTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberTypeI }
     * 
     */
    public TicketNumberTypeI createTicketNumberTypeI() {
        return new TicketNumberTypeI();
    }

    /**
     * Create an instance of {@link TourDetailsTypeI }
     * 
     */
    public TourDetailsTypeI createTourDetailsTypeI() {
        return new TourDetailsTypeI();
    }

    /**
     * Create an instance of {@link TourInformationTypeI }
     * 
     */
    public TourInformationTypeI createTourInformationTypeI() {
        return new TourInformationTypeI();
    }

    /**
     * Create an instance of {@link TransactionInformationForTicketingType }
     * 
     */
    public TransactionInformationForTicketingType createTransactionInformationForTicketingType() {
        return new TransactionInformationForTicketingType();
    }

    /**
     * Create an instance of {@link TransactionInformationsType }
     * 
     */
    public TransactionInformationsType createTransactionInformationsType() {
        return new TransactionInformationsType();
    }

    /**
     * Create an instance of {@link TransportIdentifierTypeI }
     * 
     */
    public TransportIdentifierTypeI createTransportIdentifierTypeI() {
        return new TransportIdentifierTypeI();
    }

    /**
     * Create an instance of {@link TravellerInformationTypeI }
     * 
     */
    public TravellerInformationTypeI createTravellerInformationTypeI() {
        return new TravellerInformationTypeI();
    }

    /**
     * Create an instance of {@link TravellerPriorityDetailsTypeI }
     * 
     */
    public TravellerPriorityDetailsTypeI createTravellerPriorityDetailsTypeI() {
        return new TravellerPriorityDetailsTypeI();
    }

    /**
     * Create an instance of {@link TravellerSurnameInformationTypeI }
     * 
     */
    public TravellerSurnameInformationTypeI createTravellerSurnameInformationTypeI() {
        return new TravellerSurnameInformationTypeI();
    }

    /**
     * Create an instance of {@link UserIdentificationType }
     * 
     */
    public UserIdentificationType createUserIdentificationType() {
        return new UserIdentificationType();
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup }
     * 
     */
    public DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup createDocRefundUpdateRefundReplyRefundableDocumentFeeGroupFeeSubGroup() {
        return new DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup();
    }

    /**
     * Create an instance of {@link DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup }
     * 
     */
    public DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup createDocRefundUpdateRefundReplyRefundableDocumentTicketTicketGroup() {
        return new DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup();
    }

}
