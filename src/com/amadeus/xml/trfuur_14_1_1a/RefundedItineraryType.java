
package com.amadeus.xml.trfuur_14_1_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * To convey the refunded itinerary
 * 
 * <p>Java class for RefundedItineraryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RefundedItineraryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="airlineCodeRfndItinerary" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TransportIdentifierTypeI"/&gt;
 *         &lt;element name="originDestinationRfndItinerary" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}OriginAndDestinationDetailsTypeI_67857S"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefundedItineraryType", propOrder = {
    "airlineCodeRfndItinerary",
    "originDestinationRfndItinerary"
})
public class RefundedItineraryType {

    @XmlElement(required = true)
    protected TransportIdentifierTypeI airlineCodeRfndItinerary;
    @XmlElement(required = true)
    protected OriginAndDestinationDetailsTypeI67857S originDestinationRfndItinerary;

    /**
     * Gets the value of the airlineCodeRfndItinerary property.
     * 
     * @return
     *     possible object is
     *     {@link TransportIdentifierTypeI }
     *     
     */
    public TransportIdentifierTypeI getAirlineCodeRfndItinerary() {
        return airlineCodeRfndItinerary;
    }

    /**
     * Sets the value of the airlineCodeRfndItinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransportIdentifierTypeI }
     *     
     */
    public void setAirlineCodeRfndItinerary(TransportIdentifierTypeI value) {
        this.airlineCodeRfndItinerary = value;
    }

    /**
     * Gets the value of the originDestinationRfndItinerary property.
     * 
     * @return
     *     possible object is
     *     {@link OriginAndDestinationDetailsTypeI67857S }
     *     
     */
    public OriginAndDestinationDetailsTypeI67857S getOriginDestinationRfndItinerary() {
        return originDestinationRfndItinerary;
    }

    /**
     * Sets the value of the originDestinationRfndItinerary property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginAndDestinationDetailsTypeI67857S }
     *     
     */
    public void setOriginDestinationRfndItinerary(OriginAndDestinationDetailsTypeI67857S value) {
        this.originDestinationRfndItinerary = value;
    }

}
