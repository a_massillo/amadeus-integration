
package com.amadeus.xml.trfuur_14_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="applicationErrorGroup" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="applicationErrorValue" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ApplicationErrorValueType"/&gt;
 *                   &lt;element name="errorText" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FreeTextInformationType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="refundableDocument" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="userIdentification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}UserIdentificationType"/&gt;
 *                   &lt;element name="ruleId" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInformationTypeI"/&gt;
 *                   &lt;element name="reportingTransactionDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TransactionInformationForTicketingType"/&gt;
 *                   &lt;element name="ticketNumber" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TicketNumberTypeI" maxOccurs="99" minOccurs="0"/&gt;
 *                   &lt;element name="dateTimeInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}StructuredDateTimeInformationType" maxOccurs="2"/&gt;
 *                   &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInfoType" minOccurs="0"/&gt;
 *                   &lt;element name="travellerInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TravellerInformationTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="rapidrewardaccount" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FrequentTravellerIdentificationCodeType" minOccurs="0"/&gt;
 *                   &lt;element name="ticket" maxOccurs="7" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ticketInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TicketNumberTypeI"/&gt;
 *                             &lt;element name="ticketGroup" maxOccurs="4" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}CouponInformationTypeI"/&gt;
 *                                       &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
 *                                       &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
 *                                       &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="travellerPriorityInfo" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TravellerPriorityDetailsTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="monetaryInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}MonetaryInformationType" minOccurs="0"/&gt;
 *                   &lt;element name="pricingDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}PricingTicketingDetailsTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="commission" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}CommissionInformationType" minOccurs="0"/&gt;
 *                   &lt;element name="eventDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}DeparturePlanEventType" minOccurs="0"/&gt;
 *                   &lt;element name="taxDetailsInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TaxType" maxOccurs="198" minOccurs="0"/&gt;
 *                   &lt;element name="tourInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TourInformationTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="interactiveFreeText" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}InteractiveFreeTextType" maxOccurs="3" minOccurs="0"/&gt;
 *                   &lt;element name="fopGroup" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FOPRepresentationType" maxOccurs="99" minOccurs="0"/&gt;
 *                   &lt;element name="firstAndLastSegmentDates" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}StructuredPeriodInformationType" minOccurs="0"/&gt;
 *                   &lt;element name="originAndDestination" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}OriginAndDestinationDetailsTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="refundedItinerary" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}RefundedItineraryType" maxOccurs="16" minOccurs="0"/&gt;
 *                   &lt;element name="refundedRoute" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}RoutingInformationTypeI" minOccurs="0"/&gt;
 *                   &lt;element name="structuredAddress" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}StructuredAddressType" minOccurs="0"/&gt;
 *                   &lt;element name="feeGroup" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="feeType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SelectionDetailsTypeI"/&gt;
 *                             &lt;element name="feeSubGroup" maxOccurs="99"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SpecificDataInformationTypeI"/&gt;
 *                                       &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}MonetaryInformationTypeI"/&gt;
 *                                       &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TaxTypeI" maxOccurs="99" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "applicationErrorGroup",
    "refundableDocument"
})
@XmlRootElement(name = "DocRefund_UpdateRefundReply")
public class DocRefundUpdateRefundReply {

    protected DocRefundUpdateRefundReply.ApplicationErrorGroup applicationErrorGroup;
    protected DocRefundUpdateRefundReply.RefundableDocument refundableDocument;

    /**
     * Gets the value of the applicationErrorGroup property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundUpdateRefundReply.ApplicationErrorGroup }
     *     
     */
    public DocRefundUpdateRefundReply.ApplicationErrorGroup getApplicationErrorGroup() {
        return applicationErrorGroup;
    }

    /**
     * Sets the value of the applicationErrorGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundUpdateRefundReply.ApplicationErrorGroup }
     *     
     */
    public void setApplicationErrorGroup(DocRefundUpdateRefundReply.ApplicationErrorGroup value) {
        this.applicationErrorGroup = value;
    }

    /**
     * Gets the value of the refundableDocument property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundUpdateRefundReply.RefundableDocument }
     *     
     */
    public DocRefundUpdateRefundReply.RefundableDocument getRefundableDocument() {
        return refundableDocument;
    }

    /**
     * Sets the value of the refundableDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundUpdateRefundReply.RefundableDocument }
     *     
     */
    public void setRefundableDocument(DocRefundUpdateRefundReply.RefundableDocument value) {
        this.refundableDocument = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="applicationErrorValue" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ApplicationErrorValueType"/&gt;
     *         &lt;element name="errorText" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FreeTextInformationType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "applicationErrorValue",
        "errorText"
    })
    public static class ApplicationErrorGroup {

        @XmlElement(required = true)
        protected ApplicationErrorValueType applicationErrorValue;
        protected FreeTextInformationType errorText;

        /**
         * Gets the value of the applicationErrorValue property.
         * 
         * @return
         *     possible object is
         *     {@link ApplicationErrorValueType }
         *     
         */
        public ApplicationErrorValueType getApplicationErrorValue() {
            return applicationErrorValue;
        }

        /**
         * Sets the value of the applicationErrorValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link ApplicationErrorValueType }
         *     
         */
        public void setApplicationErrorValue(ApplicationErrorValueType value) {
            this.applicationErrorValue = value;
        }

        /**
         * Gets the value of the errorText property.
         * 
         * @return
         *     possible object is
         *     {@link FreeTextInformationType }
         *     
         */
        public FreeTextInformationType getErrorText() {
            return errorText;
        }

        /**
         * Sets the value of the errorText property.
         * 
         * @param value
         *     allowed object is
         *     {@link FreeTextInformationType }
         *     
         */
        public void setErrorText(FreeTextInformationType value) {
            this.errorText = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="userIdentification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}UserIdentificationType"/&gt;
     *         &lt;element name="ruleId" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInformationTypeI"/&gt;
     *         &lt;element name="reportingTransactionDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TransactionInformationForTicketingType"/&gt;
     *         &lt;element name="ticketNumber" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TicketNumberTypeI" maxOccurs="99" minOccurs="0"/&gt;
     *         &lt;element name="dateTimeInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}StructuredDateTimeInformationType" maxOccurs="2"/&gt;
     *         &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInfoType" minOccurs="0"/&gt;
     *         &lt;element name="travellerInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TravellerInformationTypeI" minOccurs="0"/&gt;
     *         &lt;element name="rapidrewardaccount" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FrequentTravellerIdentificationCodeType" minOccurs="0"/&gt;
     *         &lt;element name="ticket" maxOccurs="7" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ticketInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TicketNumberTypeI"/&gt;
     *                   &lt;element name="ticketGroup" maxOccurs="4" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}CouponInformationTypeI"/&gt;
     *                             &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
     *                             &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
     *                             &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="travellerPriorityInfo" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TravellerPriorityDetailsTypeI" minOccurs="0"/&gt;
     *         &lt;element name="monetaryInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}MonetaryInformationType" minOccurs="0"/&gt;
     *         &lt;element name="pricingDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}PricingTicketingDetailsTypeI" minOccurs="0"/&gt;
     *         &lt;element name="commission" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}CommissionInformationType" minOccurs="0"/&gt;
     *         &lt;element name="eventDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}DeparturePlanEventType" minOccurs="0"/&gt;
     *         &lt;element name="taxDetailsInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TaxType" maxOccurs="198" minOccurs="0"/&gt;
     *         &lt;element name="tourInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TourInformationTypeI" minOccurs="0"/&gt;
     *         &lt;element name="interactiveFreeText" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}InteractiveFreeTextType" maxOccurs="3" minOccurs="0"/&gt;
     *         &lt;element name="fopGroup" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}FOPRepresentationType" maxOccurs="99" minOccurs="0"/&gt;
     *         &lt;element name="firstAndLastSegmentDates" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}StructuredPeriodInformationType" minOccurs="0"/&gt;
     *         &lt;element name="originAndDestination" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}OriginAndDestinationDetailsTypeI" minOccurs="0"/&gt;
     *         &lt;element name="refundedItinerary" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}RefundedItineraryType" maxOccurs="16" minOccurs="0"/&gt;
     *         &lt;element name="refundedRoute" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}RoutingInformationTypeI" minOccurs="0"/&gt;
     *         &lt;element name="structuredAddress" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}StructuredAddressType" minOccurs="0"/&gt;
     *         &lt;element name="feeGroup" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="feeType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SelectionDetailsTypeI"/&gt;
     *                   &lt;element name="feeSubGroup" maxOccurs="99"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SpecificDataInformationTypeI"/&gt;
     *                             &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}MonetaryInformationTypeI"/&gt;
     *                             &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TaxTypeI" maxOccurs="99" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "userIdentification",
        "ruleId",
        "reportingTransactionDetails",
        "ticketNumber",
        "dateTimeInformation",
        "referenceInformation",
        "travellerInformation",
        "rapidrewardaccount",
        "ticket",
        "travellerPriorityInfo",
        "monetaryInformation",
        "pricingDetails",
        "commission",
        "eventDetails",
        "taxDetailsInformation",
        "tourInformation",
        "interactiveFreeText",
        "fopGroup",
        "firstAndLastSegmentDates",
        "originAndDestination",
        "refundedItinerary",
        "refundedRoute",
        "structuredAddress",
        "feeGroup"
    })
    public static class RefundableDocument {

        @XmlElement(required = true)
        protected UserIdentificationType userIdentification;
        @XmlElement(required = true)
        protected ReferenceInformationTypeI ruleId;
        @XmlElement(required = true)
        protected TransactionInformationForTicketingType reportingTransactionDetails;
        protected List<TicketNumberTypeI> ticketNumber;
        @XmlElement(required = true)
        protected List<StructuredDateTimeInformationType> dateTimeInformation;
        protected ReferenceInfoType referenceInformation;
        protected TravellerInformationTypeI travellerInformation;
        protected FrequentTravellerIdentificationCodeType rapidrewardaccount;
        protected List<DocRefundUpdateRefundReply.RefundableDocument.Ticket> ticket;
        protected TravellerPriorityDetailsTypeI travellerPriorityInfo;
        protected MonetaryInformationType monetaryInformation;
        protected PricingTicketingDetailsTypeI pricingDetails;
        protected CommissionInformationType commission;
        protected DeparturePlanEventType eventDetails;
        protected List<TaxType> taxDetailsInformation;
        protected TourInformationTypeI tourInformation;
        protected List<InteractiveFreeTextType> interactiveFreeText;
        protected List<FOPRepresentationType> fopGroup;
        protected StructuredPeriodInformationType firstAndLastSegmentDates;
        protected OriginAndDestinationDetailsTypeI originAndDestination;
        protected List<RefundedItineraryType> refundedItinerary;
        protected RoutingInformationTypeI refundedRoute;
        protected StructuredAddressType structuredAddress;
        protected DocRefundUpdateRefundReply.RefundableDocument.FeeGroup feeGroup;

        /**
         * Gets the value of the userIdentification property.
         * 
         * @return
         *     possible object is
         *     {@link UserIdentificationType }
         *     
         */
        public UserIdentificationType getUserIdentification() {
            return userIdentification;
        }

        /**
         * Sets the value of the userIdentification property.
         * 
         * @param value
         *     allowed object is
         *     {@link UserIdentificationType }
         *     
         */
        public void setUserIdentification(UserIdentificationType value) {
            this.userIdentification = value;
        }

        /**
         * Gets the value of the ruleId property.
         * 
         * @return
         *     possible object is
         *     {@link ReferenceInformationTypeI }
         *     
         */
        public ReferenceInformationTypeI getRuleId() {
            return ruleId;
        }

        /**
         * Sets the value of the ruleId property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReferenceInformationTypeI }
         *     
         */
        public void setRuleId(ReferenceInformationTypeI value) {
            this.ruleId = value;
        }

        /**
         * Gets the value of the reportingTransactionDetails property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionInformationForTicketingType }
         *     
         */
        public TransactionInformationForTicketingType getReportingTransactionDetails() {
            return reportingTransactionDetails;
        }

        /**
         * Sets the value of the reportingTransactionDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionInformationForTicketingType }
         *     
         */
        public void setReportingTransactionDetails(TransactionInformationForTicketingType value) {
            this.reportingTransactionDetails = value;
        }

        /**
         * Gets the value of the ticketNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ticketNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTicketNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TicketNumberTypeI }
         * 
         * 
         */
        public List<TicketNumberTypeI> getTicketNumber() {
            if (ticketNumber == null) {
                ticketNumber = new ArrayList<TicketNumberTypeI>();
            }
            return this.ticketNumber;
        }

        /**
         * Gets the value of the dateTimeInformation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dateTimeInformation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDateTimeInformation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link StructuredDateTimeInformationType }
         * 
         * 
         */
        public List<StructuredDateTimeInformationType> getDateTimeInformation() {
            if (dateTimeInformation == null) {
                dateTimeInformation = new ArrayList<StructuredDateTimeInformationType>();
            }
            return this.dateTimeInformation;
        }

        /**
         * Gets the value of the referenceInformation property.
         * 
         * @return
         *     possible object is
         *     {@link ReferenceInfoType }
         *     
         */
        public ReferenceInfoType getReferenceInformation() {
            return referenceInformation;
        }

        /**
         * Sets the value of the referenceInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReferenceInfoType }
         *     
         */
        public void setReferenceInformation(ReferenceInfoType value) {
            this.referenceInformation = value;
        }

        /**
         * Gets the value of the travellerInformation property.
         * 
         * @return
         *     possible object is
         *     {@link TravellerInformationTypeI }
         *     
         */
        public TravellerInformationTypeI getTravellerInformation() {
            return travellerInformation;
        }

        /**
         * Sets the value of the travellerInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link TravellerInformationTypeI }
         *     
         */
        public void setTravellerInformation(TravellerInformationTypeI value) {
            this.travellerInformation = value;
        }

        /**
         * Gets the value of the rapidrewardaccount property.
         * 
         * @return
         *     possible object is
         *     {@link FrequentTravellerIdentificationCodeType }
         *     
         */
        public FrequentTravellerIdentificationCodeType getRapidrewardaccount() {
            return rapidrewardaccount;
        }

        /**
         * Sets the value of the rapidrewardaccount property.
         * 
         * @param value
         *     allowed object is
         *     {@link FrequentTravellerIdentificationCodeType }
         *     
         */
        public void setRapidrewardaccount(FrequentTravellerIdentificationCodeType value) {
            this.rapidrewardaccount = value;
        }

        /**
         * Gets the value of the ticket property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ticket property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTicket().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DocRefundUpdateRefundReply.RefundableDocument.Ticket }
         * 
         * 
         */
        public List<DocRefundUpdateRefundReply.RefundableDocument.Ticket> getTicket() {
            if (ticket == null) {
                ticket = new ArrayList<DocRefundUpdateRefundReply.RefundableDocument.Ticket>();
            }
            return this.ticket;
        }

        /**
         * Gets the value of the travellerPriorityInfo property.
         * 
         * @return
         *     possible object is
         *     {@link TravellerPriorityDetailsTypeI }
         *     
         */
        public TravellerPriorityDetailsTypeI getTravellerPriorityInfo() {
            return travellerPriorityInfo;
        }

        /**
         * Sets the value of the travellerPriorityInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link TravellerPriorityDetailsTypeI }
         *     
         */
        public void setTravellerPriorityInfo(TravellerPriorityDetailsTypeI value) {
            this.travellerPriorityInfo = value;
        }

        /**
         * Gets the value of the monetaryInformation property.
         * 
         * @return
         *     possible object is
         *     {@link MonetaryInformationType }
         *     
         */
        public MonetaryInformationType getMonetaryInformation() {
            return monetaryInformation;
        }

        /**
         * Sets the value of the monetaryInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link MonetaryInformationType }
         *     
         */
        public void setMonetaryInformation(MonetaryInformationType value) {
            this.monetaryInformation = value;
        }

        /**
         * Gets the value of the pricingDetails property.
         * 
         * @return
         *     possible object is
         *     {@link PricingTicketingDetailsTypeI }
         *     
         */
        public PricingTicketingDetailsTypeI getPricingDetails() {
            return pricingDetails;
        }

        /**
         * Sets the value of the pricingDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link PricingTicketingDetailsTypeI }
         *     
         */
        public void setPricingDetails(PricingTicketingDetailsTypeI value) {
            this.pricingDetails = value;
        }

        /**
         * Gets the value of the commission property.
         * 
         * @return
         *     possible object is
         *     {@link CommissionInformationType }
         *     
         */
        public CommissionInformationType getCommission() {
            return commission;
        }

        /**
         * Sets the value of the commission property.
         * 
         * @param value
         *     allowed object is
         *     {@link CommissionInformationType }
         *     
         */
        public void setCommission(CommissionInformationType value) {
            this.commission = value;
        }

        /**
         * Gets the value of the eventDetails property.
         * 
         * @return
         *     possible object is
         *     {@link DeparturePlanEventType }
         *     
         */
        public DeparturePlanEventType getEventDetails() {
            return eventDetails;
        }

        /**
         * Sets the value of the eventDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link DeparturePlanEventType }
         *     
         */
        public void setEventDetails(DeparturePlanEventType value) {
            this.eventDetails = value;
        }

        /**
         * Gets the value of the taxDetailsInformation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the taxDetailsInformation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTaxDetailsInformation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TaxType }
         * 
         * 
         */
        public List<TaxType> getTaxDetailsInformation() {
            if (taxDetailsInformation == null) {
                taxDetailsInformation = new ArrayList<TaxType>();
            }
            return this.taxDetailsInformation;
        }

        /**
         * Gets the value of the tourInformation property.
         * 
         * @return
         *     possible object is
         *     {@link TourInformationTypeI }
         *     
         */
        public TourInformationTypeI getTourInformation() {
            return tourInformation;
        }

        /**
         * Sets the value of the tourInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link TourInformationTypeI }
         *     
         */
        public void setTourInformation(TourInformationTypeI value) {
            this.tourInformation = value;
        }

        /**
         * Gets the value of the interactiveFreeText property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the interactiveFreeText property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInteractiveFreeText().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link InteractiveFreeTextType }
         * 
         * 
         */
        public List<InteractiveFreeTextType> getInteractiveFreeText() {
            if (interactiveFreeText == null) {
                interactiveFreeText = new ArrayList<InteractiveFreeTextType>();
            }
            return this.interactiveFreeText;
        }

        /**
         * Gets the value of the fopGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fopGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFopGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FOPRepresentationType }
         * 
         * 
         */
        public List<FOPRepresentationType> getFopGroup() {
            if (fopGroup == null) {
                fopGroup = new ArrayList<FOPRepresentationType>();
            }
            return this.fopGroup;
        }

        /**
         * Gets the value of the firstAndLastSegmentDates property.
         * 
         * @return
         *     possible object is
         *     {@link StructuredPeriodInformationType }
         *     
         */
        public StructuredPeriodInformationType getFirstAndLastSegmentDates() {
            return firstAndLastSegmentDates;
        }

        /**
         * Sets the value of the firstAndLastSegmentDates property.
         * 
         * @param value
         *     allowed object is
         *     {@link StructuredPeriodInformationType }
         *     
         */
        public void setFirstAndLastSegmentDates(StructuredPeriodInformationType value) {
            this.firstAndLastSegmentDates = value;
        }

        /**
         * Gets the value of the originAndDestination property.
         * 
         * @return
         *     possible object is
         *     {@link OriginAndDestinationDetailsTypeI }
         *     
         */
        public OriginAndDestinationDetailsTypeI getOriginAndDestination() {
            return originAndDestination;
        }

        /**
         * Sets the value of the originAndDestination property.
         * 
         * @param value
         *     allowed object is
         *     {@link OriginAndDestinationDetailsTypeI }
         *     
         */
        public void setOriginAndDestination(OriginAndDestinationDetailsTypeI value) {
            this.originAndDestination = value;
        }

        /**
         * Gets the value of the refundedItinerary property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refundedItinerary property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefundedItinerary().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefundedItineraryType }
         * 
         * 
         */
        public List<RefundedItineraryType> getRefundedItinerary() {
            if (refundedItinerary == null) {
                refundedItinerary = new ArrayList<RefundedItineraryType>();
            }
            return this.refundedItinerary;
        }

        /**
         * Gets the value of the refundedRoute property.
         * 
         * @return
         *     possible object is
         *     {@link RoutingInformationTypeI }
         *     
         */
        public RoutingInformationTypeI getRefundedRoute() {
            return refundedRoute;
        }

        /**
         * Sets the value of the refundedRoute property.
         * 
         * @param value
         *     allowed object is
         *     {@link RoutingInformationTypeI }
         *     
         */
        public void setRefundedRoute(RoutingInformationTypeI value) {
            this.refundedRoute = value;
        }

        /**
         * Gets the value of the structuredAddress property.
         * 
         * @return
         *     possible object is
         *     {@link StructuredAddressType }
         *     
         */
        public StructuredAddressType getStructuredAddress() {
            return structuredAddress;
        }

        /**
         * Sets the value of the structuredAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link StructuredAddressType }
         *     
         */
        public void setStructuredAddress(StructuredAddressType value) {
            this.structuredAddress = value;
        }

        /**
         * Gets the value of the feeGroup property.
         * 
         * @return
         *     possible object is
         *     {@link DocRefundUpdateRefundReply.RefundableDocument.FeeGroup }
         *     
         */
        public DocRefundUpdateRefundReply.RefundableDocument.FeeGroup getFeeGroup() {
            return feeGroup;
        }

        /**
         * Sets the value of the feeGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link DocRefundUpdateRefundReply.RefundableDocument.FeeGroup }
         *     
         */
        public void setFeeGroup(DocRefundUpdateRefundReply.RefundableDocument.FeeGroup value) {
            this.feeGroup = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="feeType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SelectionDetailsTypeI"/&gt;
         *         &lt;element name="feeSubGroup" maxOccurs="99"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SpecificDataInformationTypeI"/&gt;
         *                   &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}MonetaryInformationTypeI"/&gt;
         *                   &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TaxTypeI" maxOccurs="99" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "feeType",
            "feeSubGroup"
        })
        public static class FeeGroup {

            @XmlElement(required = true)
            protected SelectionDetailsTypeI feeType;
            @XmlElement(required = true)
            protected List<DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup> feeSubGroup;

            /**
             * Gets the value of the feeType property.
             * 
             * @return
             *     possible object is
             *     {@link SelectionDetailsTypeI }
             *     
             */
            public SelectionDetailsTypeI getFeeType() {
                return feeType;
            }

            /**
             * Sets the value of the feeType property.
             * 
             * @param value
             *     allowed object is
             *     {@link SelectionDetailsTypeI }
             *     
             */
            public void setFeeType(SelectionDetailsTypeI value) {
                this.feeType = value;
            }

            /**
             * Gets the value of the feeSubGroup property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the feeSubGroup property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFeeSubGroup().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup }
             * 
             * 
             */
            public List<DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup> getFeeSubGroup() {
                if (feeSubGroup == null) {
                    feeSubGroup = new ArrayList<DocRefundUpdateRefundReply.RefundableDocument.FeeGroup.FeeSubGroup>();
                }
                return this.feeSubGroup;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}SpecificDataInformationTypeI"/&gt;
             *         &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}MonetaryInformationTypeI"/&gt;
             *         &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TaxTypeI" maxOccurs="99" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "feeSubType",
                "feeMonetaryInfo",
                "feesTaxes"
            })
            public static class FeeSubGroup {

                @XmlElement(required = true)
                protected SpecificDataInformationTypeI feeSubType;
                @XmlElement(required = true)
                protected MonetaryInformationTypeI feeMonetaryInfo;
                protected List<TaxTypeI> feesTaxes;

                /**
                 * Gets the value of the feeSubType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link SpecificDataInformationTypeI }
                 *     
                 */
                public SpecificDataInformationTypeI getFeeSubType() {
                    return feeSubType;
                }

                /**
                 * Sets the value of the feeSubType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SpecificDataInformationTypeI }
                 *     
                 */
                public void setFeeSubType(SpecificDataInformationTypeI value) {
                    this.feeSubType = value;
                }

                /**
                 * Gets the value of the feeMonetaryInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link MonetaryInformationTypeI }
                 *     
                 */
                public MonetaryInformationTypeI getFeeMonetaryInfo() {
                    return feeMonetaryInfo;
                }

                /**
                 * Sets the value of the feeMonetaryInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link MonetaryInformationTypeI }
                 *     
                 */
                public void setFeeMonetaryInfo(MonetaryInformationTypeI value) {
                    this.feeMonetaryInfo = value;
                }

                /**
                 * Gets the value of the feesTaxes property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the feesTaxes property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getFeesTaxes().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TaxTypeI }
                 * 
                 * 
                 */
                public List<TaxTypeI> getFeesTaxes() {
                    if (feesTaxes == null) {
                        feesTaxes = new ArrayList<TaxTypeI>();
                    }
                    return this.feesTaxes;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ticketInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}TicketNumberTypeI"/&gt;
         *         &lt;element name="ticketGroup" maxOccurs="4" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}CouponInformationTypeI"/&gt;
         *                   &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
         *                   &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
         *                   &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ticketInformation",
            "ticketGroup"
        })
        public static class Ticket {

            @XmlElement(required = true)
            protected TicketNumberTypeI ticketInformation;
            protected List<DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup> ticketGroup;

            /**
             * Gets the value of the ticketInformation property.
             * 
             * @return
             *     possible object is
             *     {@link TicketNumberTypeI }
             *     
             */
            public TicketNumberTypeI getTicketInformation() {
                return ticketInformation;
            }

            /**
             * Sets the value of the ticketInformation property.
             * 
             * @param value
             *     allowed object is
             *     {@link TicketNumberTypeI }
             *     
             */
            public void setTicketInformation(TicketNumberTypeI value) {
                this.ticketInformation = value;
            }

            /**
             * Gets the value of the ticketGroup property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ticketGroup property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTicketGroup().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup }
             * 
             * 
             */
            public List<DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup> getTicketGroup() {
                if (ticketGroup == null) {
                    ticketGroup = new ArrayList<DocRefundUpdateRefundReply.RefundableDocument.Ticket.TicketGroup>();
                }
                return this.ticketGroup;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}CouponInformationTypeI"/&gt;
             *         &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
             *         &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
             *         &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUR_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "couponInformationDetails",
                "boardingPriority",
                "actionIdentification",
                "referenceInformation"
            })
            public static class TicketGroup {

                @XmlElement(required = true)
                protected CouponInformationTypeI couponInformationDetails;
                protected PriorityTypeU boardingPriority;
                protected ActionIdentificationType actionIdentification;
                protected ReferenceInfoType57269S referenceInformation;

                /**
                 * Gets the value of the couponInformationDetails property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CouponInformationTypeI }
                 *     
                 */
                public CouponInformationTypeI getCouponInformationDetails() {
                    return couponInformationDetails;
                }

                /**
                 * Sets the value of the couponInformationDetails property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CouponInformationTypeI }
                 *     
                 */
                public void setCouponInformationDetails(CouponInformationTypeI value) {
                    this.couponInformationDetails = value;
                }

                /**
                 * Gets the value of the boardingPriority property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PriorityTypeU }
                 *     
                 */
                public PriorityTypeU getBoardingPriority() {
                    return boardingPriority;
                }

                /**
                 * Sets the value of the boardingPriority property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PriorityTypeU }
                 *     
                 */
                public void setBoardingPriority(PriorityTypeU value) {
                    this.boardingPriority = value;
                }

                /**
                 * Gets the value of the actionIdentification property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActionIdentificationType }
                 *     
                 */
                public ActionIdentificationType getActionIdentification() {
                    return actionIdentification;
                }

                /**
                 * Sets the value of the actionIdentification property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActionIdentificationType }
                 *     
                 */
                public void setActionIdentification(ActionIdentificationType value) {
                    this.actionIdentification = value;
                }

                /**
                 * Gets the value of the referenceInformation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ReferenceInfoType57269S }
                 *     
                 */
                public ReferenceInfoType57269S getReferenceInformation() {
                    return referenceInformation;
                }

                /**
                 * Sets the value of the referenceInformation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ReferenceInfoType57269S }
                 *     
                 */
                public void setReferenceInformation(ReferenceInfoType57269S value) {
                    this.referenceInformation = value;
                }

            }

        }

    }

}
