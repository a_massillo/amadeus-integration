
package com.amadeus.xml.trfidq_09_2_ia;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ignoreInformation"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="actionRequest"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;minLength value="1"/&gt;
 *                         &lt;maxLength value="3"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ignoreInformation"
})
@XmlRootElement(name = "DocRefund_IgnoreRefund")
public class DocRefundIgnoreRefund {

    @XmlElement(required = true)
    protected DocRefundIgnoreRefund.IgnoreInformation ignoreInformation;

    /**
     * Gets the value of the ignoreInformation property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundIgnoreRefund.IgnoreInformation }
     *     
     */
    public DocRefundIgnoreRefund.IgnoreInformation getIgnoreInformation() {
        return ignoreInformation;
    }

    /**
     * Sets the value of the ignoreInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundIgnoreRefund.IgnoreInformation }
     *     
     */
    public void setIgnoreInformation(DocRefundIgnoreRefund.IgnoreInformation value) {
        this.ignoreInformation = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="actionRequest"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;minLength value="1"/&gt;
     *               &lt;maxLength value="3"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "actionRequest"
    })
    public static class IgnoreInformation {

        @XmlElement(required = true)
        protected String actionRequest;

        /**
         * Gets the value of the actionRequest property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActionRequest() {
            return actionRequest;
        }

        /**
         * Sets the value of the actionRequest property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActionRequest(String value) {
            this.actionRequest = value;
        }

    }

}
