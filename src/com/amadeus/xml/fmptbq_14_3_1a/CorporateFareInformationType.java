
package com.amadeus.xml.fmptbq_14_3_1a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * To specify corporate fare information
 * 
 * <p>Java class for CorporateFareInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporateFareInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="corporateFareIdentifiers" type="{http://xml.amadeus.com/FMPTBQ_14_3_1A}CorporateFareIdentifiersType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporateFareInformationType", propOrder = {
    "corporateFareIdentifiers"
})
public class CorporateFareInformationType {

    protected CorporateFareIdentifiersType corporateFareIdentifiers;

    /**
     * Gets the value of the corporateFareIdentifiers property.
     * 
     * @return
     *     possible object is
     *     {@link CorporateFareIdentifiersType }
     *     
     */
    public CorporateFareIdentifiersType getCorporateFareIdentifiers() {
        return corporateFareIdentifiers;
    }

    /**
     * Sets the value of the corporateFareIdentifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporateFareIdentifiersType }
     *     
     */
    public void setCorporateFareIdentifiers(CorporateFareIdentifiersType value) {
        this.corporateFareIdentifiers = value;
    }

}
