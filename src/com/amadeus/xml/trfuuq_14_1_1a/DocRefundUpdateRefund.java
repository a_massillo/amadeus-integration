
package com.amadeus.xml.trfuuq_14_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userIdentification" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}UserIdentificationType"/&gt;
 *         &lt;element name="ticketNumber" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TicketNumberTypeI" minOccurs="0"/&gt;
 *         &lt;element name="dateTimeInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}StructuredDateTimeInformationType" maxOccurs="2"/&gt;
 *         &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ReferenceInfoType" minOccurs="0"/&gt;
 *         &lt;element name="travellerInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TravellerInformationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="ticket" maxOccurs="7" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ticketInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TicketNumberTypeI"/&gt;
 *                   &lt;element name="ticketGroup" maxOccurs="4" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}CouponInformationTypeI"/&gt;
 *                             &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
 *                             &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
 *                             &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="travellerPriorityInfo" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TravellerPriorityDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="monetaryInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}MonetaryInformationType" minOccurs="0"/&gt;
 *         &lt;element name="pricingDetails" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}PricingTicketingDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="commission" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}CommissionInformationType" minOccurs="0"/&gt;
 *         &lt;element name="taxDetailsInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TaxTypeI" maxOccurs="198" minOccurs="0"/&gt;
 *         &lt;element name="tourInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TourInformationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="interactiveFreeText" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}InteractiveFreeTextType" maxOccurs="3" minOccurs="0"/&gt;
 *         &lt;element name="fopGroup" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}FOPRepresentationType" maxOccurs="99" minOccurs="0"/&gt;
 *         &lt;element name="transactionContext" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TransactionContextType" minOccurs="0"/&gt;
 *         &lt;element name="firstAndLastSegmentDates" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}StructuredPeriodInformationType" minOccurs="0"/&gt;
 *         &lt;element name="originAndDestination" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}OriginAndDestinationDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="refundedItinerary" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}RefundedItineraryType" maxOccurs="16" minOccurs="0"/&gt;
 *         &lt;element name="refundedRoute" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}RoutingInformationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="structuredAddress" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}StructuredAddressType" minOccurs="0"/&gt;
 *         &lt;element name="feeGroup" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="feeType" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}SelectionDetailsTypeI"/&gt;
 *                   &lt;element name="feeSubGroup" maxOccurs="99"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}SpecificDataInformationTypeI"/&gt;
 *                             &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}MonetaryInformationTypeI"/&gt;
 *                             &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TaxTypeI_55307S" maxOccurs="99" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userIdentification",
    "ticketNumber",
    "dateTimeInformation",
    "referenceInformation",
    "travellerInformation",
    "ticket",
    "travellerPriorityInfo",
    "monetaryInformation",
    "pricingDetails",
    "commission",
    "taxDetailsInformation",
    "tourInformation",
    "interactiveFreeText",
    "fopGroup",
    "transactionContext",
    "firstAndLastSegmentDates",
    "originAndDestination",
    "refundedItinerary",
    "refundedRoute",
    "structuredAddress",
    "feeGroup"
})
@XmlRootElement(name = "DocRefund_UpdateRefund")
public class DocRefundUpdateRefund {

    @XmlElement(required = true)
    protected UserIdentificationType userIdentification;
    protected TicketNumberTypeI ticketNumber;
    @XmlElement(required = true)
    protected List<StructuredDateTimeInformationType> dateTimeInformation;
    protected ReferenceInfoType referenceInformation;
    protected TravellerInformationTypeI travellerInformation;
    protected List<DocRefundUpdateRefund.Ticket> ticket;
    protected TravellerPriorityDetailsTypeI travellerPriorityInfo;
    protected MonetaryInformationType monetaryInformation;
    protected PricingTicketingDetailsTypeI pricingDetails;
    protected CommissionInformationType commission;
    protected List<TaxTypeI> taxDetailsInformation;
    protected TourInformationTypeI tourInformation;
    protected List<InteractiveFreeTextType> interactiveFreeText;
    protected List<FOPRepresentationType> fopGroup;
    protected TransactionContextType transactionContext;
    protected StructuredPeriodInformationType firstAndLastSegmentDates;
    protected OriginAndDestinationDetailsTypeI originAndDestination;
    protected List<RefundedItineraryType> refundedItinerary;
    protected RoutingInformationTypeI refundedRoute;
    protected StructuredAddressType structuredAddress;
    protected DocRefundUpdateRefund.FeeGroup feeGroup;

    /**
     * Gets the value of the userIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentificationType }
     *     
     */
    public UserIdentificationType getUserIdentification() {
        return userIdentification;
    }

    /**
     * Sets the value of the userIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentificationType }
     *     
     */
    public void setUserIdentification(UserIdentificationType value) {
        this.userIdentification = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * @return
     *     possible object is
     *     {@link TicketNumberTypeI }
     *     
     */
    public TicketNumberTypeI getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketNumberTypeI }
     *     
     */
    public void setTicketNumber(TicketNumberTypeI value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the dateTimeInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateTimeInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateTimeInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StructuredDateTimeInformationType }
     * 
     * 
     */
    public List<StructuredDateTimeInformationType> getDateTimeInformation() {
        if (dateTimeInformation == null) {
            dateTimeInformation = new ArrayList<StructuredDateTimeInformationType>();
        }
        return this.dateTimeInformation;
    }

    /**
     * Gets the value of the referenceInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceInfoType }
     *     
     */
    public ReferenceInfoType getReferenceInformation() {
        return referenceInformation;
    }

    /**
     * Sets the value of the referenceInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceInfoType }
     *     
     */
    public void setReferenceInformation(ReferenceInfoType value) {
        this.referenceInformation = value;
    }

    /**
     * Gets the value of the travellerInformation property.
     * 
     * @return
     *     possible object is
     *     {@link TravellerInformationTypeI }
     *     
     */
    public TravellerInformationTypeI getTravellerInformation() {
        return travellerInformation;
    }

    /**
     * Sets the value of the travellerInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravellerInformationTypeI }
     *     
     */
    public void setTravellerInformation(TravellerInformationTypeI value) {
        this.travellerInformation = value;
    }

    /**
     * Gets the value of the ticket property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticket property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicket().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocRefundUpdateRefund.Ticket }
     * 
     * 
     */
    public List<DocRefundUpdateRefund.Ticket> getTicket() {
        if (ticket == null) {
            ticket = new ArrayList<DocRefundUpdateRefund.Ticket>();
        }
        return this.ticket;
    }

    /**
     * Gets the value of the travellerPriorityInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TravellerPriorityDetailsTypeI }
     *     
     */
    public TravellerPriorityDetailsTypeI getTravellerPriorityInfo() {
        return travellerPriorityInfo;
    }

    /**
     * Sets the value of the travellerPriorityInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TravellerPriorityDetailsTypeI }
     *     
     */
    public void setTravellerPriorityInfo(TravellerPriorityDetailsTypeI value) {
        this.travellerPriorityInfo = value;
    }

    /**
     * Gets the value of the monetaryInformation property.
     * 
     * @return
     *     possible object is
     *     {@link MonetaryInformationType }
     *     
     */
    public MonetaryInformationType getMonetaryInformation() {
        return monetaryInformation;
    }

    /**
     * Sets the value of the monetaryInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonetaryInformationType }
     *     
     */
    public void setMonetaryInformation(MonetaryInformationType value) {
        this.monetaryInformation = value;
    }

    /**
     * Gets the value of the pricingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PricingTicketingDetailsTypeI }
     *     
     */
    public PricingTicketingDetailsTypeI getPricingDetails() {
        return pricingDetails;
    }

    /**
     * Sets the value of the pricingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingTicketingDetailsTypeI }
     *     
     */
    public void setPricingDetails(PricingTicketingDetailsTypeI value) {
        this.pricingDetails = value;
    }

    /**
     * Gets the value of the commission property.
     * 
     * @return
     *     possible object is
     *     {@link CommissionInformationType }
     *     
     */
    public CommissionInformationType getCommission() {
        return commission;
    }

    /**
     * Sets the value of the commission property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommissionInformationType }
     *     
     */
    public void setCommission(CommissionInformationType value) {
        this.commission = value;
    }

    /**
     * Gets the value of the taxDetailsInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxDetailsInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxDetailsInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxTypeI }
     * 
     * 
     */
    public List<TaxTypeI> getTaxDetailsInformation() {
        if (taxDetailsInformation == null) {
            taxDetailsInformation = new ArrayList<TaxTypeI>();
        }
        return this.taxDetailsInformation;
    }

    /**
     * Gets the value of the tourInformation property.
     * 
     * @return
     *     possible object is
     *     {@link TourInformationTypeI }
     *     
     */
    public TourInformationTypeI getTourInformation() {
        return tourInformation;
    }

    /**
     * Sets the value of the tourInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TourInformationTypeI }
     *     
     */
    public void setTourInformation(TourInformationTypeI value) {
        this.tourInformation = value;
    }

    /**
     * Gets the value of the interactiveFreeText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interactiveFreeText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInteractiveFreeText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InteractiveFreeTextType }
     * 
     * 
     */
    public List<InteractiveFreeTextType> getInteractiveFreeText() {
        if (interactiveFreeText == null) {
            interactiveFreeText = new ArrayList<InteractiveFreeTextType>();
        }
        return this.interactiveFreeText;
    }

    /**
     * Gets the value of the fopGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fopGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFopGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FOPRepresentationType }
     * 
     * 
     */
    public List<FOPRepresentationType> getFopGroup() {
        if (fopGroup == null) {
            fopGroup = new ArrayList<FOPRepresentationType>();
        }
        return this.fopGroup;
    }

    /**
     * Gets the value of the transactionContext property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionContextType }
     *     
     */
    public TransactionContextType getTransactionContext() {
        return transactionContext;
    }

    /**
     * Sets the value of the transactionContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionContextType }
     *     
     */
    public void setTransactionContext(TransactionContextType value) {
        this.transactionContext = value;
    }

    /**
     * Gets the value of the firstAndLastSegmentDates property.
     * 
     * @return
     *     possible object is
     *     {@link StructuredPeriodInformationType }
     *     
     */
    public StructuredPeriodInformationType getFirstAndLastSegmentDates() {
        return firstAndLastSegmentDates;
    }

    /**
     * Sets the value of the firstAndLastSegmentDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuredPeriodInformationType }
     *     
     */
    public void setFirstAndLastSegmentDates(StructuredPeriodInformationType value) {
        this.firstAndLastSegmentDates = value;
    }

    /**
     * Gets the value of the originAndDestination property.
     * 
     * @return
     *     possible object is
     *     {@link OriginAndDestinationDetailsTypeI }
     *     
     */
    public OriginAndDestinationDetailsTypeI getOriginAndDestination() {
        return originAndDestination;
    }

    /**
     * Sets the value of the originAndDestination property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginAndDestinationDetailsTypeI }
     *     
     */
    public void setOriginAndDestination(OriginAndDestinationDetailsTypeI value) {
        this.originAndDestination = value;
    }

    /**
     * Gets the value of the refundedItinerary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refundedItinerary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefundedItinerary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefundedItineraryType }
     * 
     * 
     */
    public List<RefundedItineraryType> getRefundedItinerary() {
        if (refundedItinerary == null) {
            refundedItinerary = new ArrayList<RefundedItineraryType>();
        }
        return this.refundedItinerary;
    }

    /**
     * Gets the value of the refundedRoute property.
     * 
     * @return
     *     possible object is
     *     {@link RoutingInformationTypeI }
     *     
     */
    public RoutingInformationTypeI getRefundedRoute() {
        return refundedRoute;
    }

    /**
     * Sets the value of the refundedRoute property.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingInformationTypeI }
     *     
     */
    public void setRefundedRoute(RoutingInformationTypeI value) {
        this.refundedRoute = value;
    }

    /**
     * Gets the value of the structuredAddress property.
     * 
     * @return
     *     possible object is
     *     {@link StructuredAddressType }
     *     
     */
    public StructuredAddressType getStructuredAddress() {
        return structuredAddress;
    }

    /**
     * Sets the value of the structuredAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link StructuredAddressType }
     *     
     */
    public void setStructuredAddress(StructuredAddressType value) {
        this.structuredAddress = value;
    }

    /**
     * Gets the value of the feeGroup property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundUpdateRefund.FeeGroup }
     *     
     */
    public DocRefundUpdateRefund.FeeGroup getFeeGroup() {
        return feeGroup;
    }

    /**
     * Sets the value of the feeGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundUpdateRefund.FeeGroup }
     *     
     */
    public void setFeeGroup(DocRefundUpdateRefund.FeeGroup value) {
        this.feeGroup = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="feeType" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}SelectionDetailsTypeI"/&gt;
     *         &lt;element name="feeSubGroup" maxOccurs="99"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}SpecificDataInformationTypeI"/&gt;
     *                   &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}MonetaryInformationTypeI"/&gt;
     *                   &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TaxTypeI_55307S" maxOccurs="99" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "feeType",
        "feeSubGroup"
    })
    public static class FeeGroup {

        @XmlElement(required = true)
        protected SelectionDetailsTypeI feeType;
        @XmlElement(required = true)
        protected List<DocRefundUpdateRefund.FeeGroup.FeeSubGroup> feeSubGroup;

        /**
         * Gets the value of the feeType property.
         * 
         * @return
         *     possible object is
         *     {@link SelectionDetailsTypeI }
         *     
         */
        public SelectionDetailsTypeI getFeeType() {
            return feeType;
        }

        /**
         * Sets the value of the feeType property.
         * 
         * @param value
         *     allowed object is
         *     {@link SelectionDetailsTypeI }
         *     
         */
        public void setFeeType(SelectionDetailsTypeI value) {
            this.feeType = value;
        }

        /**
         * Gets the value of the feeSubGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the feeSubGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFeeSubGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DocRefundUpdateRefund.FeeGroup.FeeSubGroup }
         * 
         * 
         */
        public List<DocRefundUpdateRefund.FeeGroup.FeeSubGroup> getFeeSubGroup() {
            if (feeSubGroup == null) {
                feeSubGroup = new ArrayList<DocRefundUpdateRefund.FeeGroup.FeeSubGroup>();
            }
            return this.feeSubGroup;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="feeSubType" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}SpecificDataInformationTypeI"/&gt;
         *         &lt;element name="feeMonetaryInfo" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}MonetaryInformationTypeI"/&gt;
         *         &lt;element name="feesTaxes" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TaxTypeI_55307S" maxOccurs="99" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "feeSubType",
            "feeMonetaryInfo",
            "feesTaxes"
        })
        public static class FeeSubGroup {

            @XmlElement(required = true)
            protected SpecificDataInformationTypeI feeSubType;
            @XmlElement(required = true)
            protected MonetaryInformationTypeI feeMonetaryInfo;
            protected List<TaxTypeI55307S> feesTaxes;

            /**
             * Gets the value of the feeSubType property.
             * 
             * @return
             *     possible object is
             *     {@link SpecificDataInformationTypeI }
             *     
             */
            public SpecificDataInformationTypeI getFeeSubType() {
                return feeSubType;
            }

            /**
             * Sets the value of the feeSubType property.
             * 
             * @param value
             *     allowed object is
             *     {@link SpecificDataInformationTypeI }
             *     
             */
            public void setFeeSubType(SpecificDataInformationTypeI value) {
                this.feeSubType = value;
            }

            /**
             * Gets the value of the feeMonetaryInfo property.
             * 
             * @return
             *     possible object is
             *     {@link MonetaryInformationTypeI }
             *     
             */
            public MonetaryInformationTypeI getFeeMonetaryInfo() {
                return feeMonetaryInfo;
            }

            /**
             * Sets the value of the feeMonetaryInfo property.
             * 
             * @param value
             *     allowed object is
             *     {@link MonetaryInformationTypeI }
             *     
             */
            public void setFeeMonetaryInfo(MonetaryInformationTypeI value) {
                this.feeMonetaryInfo = value;
            }

            /**
             * Gets the value of the feesTaxes property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the feesTaxes property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFeesTaxes().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TaxTypeI55307S }
             * 
             * 
             */
            public List<TaxTypeI55307S> getFeesTaxes() {
                if (feesTaxes == null) {
                    feesTaxes = new ArrayList<TaxTypeI55307S>();
                }
                return this.feesTaxes;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ticketInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}TicketNumberTypeI"/&gt;
     *         &lt;element name="ticketGroup" maxOccurs="4" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}CouponInformationTypeI"/&gt;
     *                   &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
     *                   &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
     *                   &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ticketInformation",
        "ticketGroup"
    })
    public static class Ticket {

        @XmlElement(required = true)
        protected TicketNumberTypeI ticketInformation;
        protected List<DocRefundUpdateRefund.Ticket.TicketGroup> ticketGroup;

        /**
         * Gets the value of the ticketInformation property.
         * 
         * @return
         *     possible object is
         *     {@link TicketNumberTypeI }
         *     
         */
        public TicketNumberTypeI getTicketInformation() {
            return ticketInformation;
        }

        /**
         * Sets the value of the ticketInformation property.
         * 
         * @param value
         *     allowed object is
         *     {@link TicketNumberTypeI }
         *     
         */
        public void setTicketInformation(TicketNumberTypeI value) {
            this.ticketInformation = value;
        }

        /**
         * Gets the value of the ticketGroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ticketGroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTicketGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DocRefundUpdateRefund.Ticket.TicketGroup }
         * 
         * 
         */
        public List<DocRefundUpdateRefund.Ticket.TicketGroup> getTicketGroup() {
            if (ticketGroup == null) {
                ticketGroup = new ArrayList<DocRefundUpdateRefund.Ticket.TicketGroup>();
            }
            return this.ticketGroup;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="couponInformationDetails" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}CouponInformationTypeI"/&gt;
         *         &lt;element name="boardingPriority" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}PriorityTypeU" minOccurs="0"/&gt;
         *         &lt;element name="actionIdentification" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ActionIdentificationType" minOccurs="0"/&gt;
         *         &lt;element name="referenceInformation" type="{http://xml.amadeus.com/TRFUUQ_14_1_1A}ReferenceInfoType_57269S" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "couponInformationDetails",
            "boardingPriority",
            "actionIdentification",
            "referenceInformation"
        })
        public static class TicketGroup {

            @XmlElement(required = true)
            protected CouponInformationTypeI couponInformationDetails;
            protected PriorityTypeU boardingPriority;
            protected ActionIdentificationType actionIdentification;
            protected ReferenceInfoType57269S referenceInformation;

            /**
             * Gets the value of the couponInformationDetails property.
             * 
             * @return
             *     possible object is
             *     {@link CouponInformationTypeI }
             *     
             */
            public CouponInformationTypeI getCouponInformationDetails() {
                return couponInformationDetails;
            }

            /**
             * Sets the value of the couponInformationDetails property.
             * 
             * @param value
             *     allowed object is
             *     {@link CouponInformationTypeI }
             *     
             */
            public void setCouponInformationDetails(CouponInformationTypeI value) {
                this.couponInformationDetails = value;
            }

            /**
             * Gets the value of the boardingPriority property.
             * 
             * @return
             *     possible object is
             *     {@link PriorityTypeU }
             *     
             */
            public PriorityTypeU getBoardingPriority() {
                return boardingPriority;
            }

            /**
             * Sets the value of the boardingPriority property.
             * 
             * @param value
             *     allowed object is
             *     {@link PriorityTypeU }
             *     
             */
            public void setBoardingPriority(PriorityTypeU value) {
                this.boardingPriority = value;
            }

            /**
             * Gets the value of the actionIdentification property.
             * 
             * @return
             *     possible object is
             *     {@link ActionIdentificationType }
             *     
             */
            public ActionIdentificationType getActionIdentification() {
                return actionIdentification;
            }

            /**
             * Sets the value of the actionIdentification property.
             * 
             * @param value
             *     allowed object is
             *     {@link ActionIdentificationType }
             *     
             */
            public void setActionIdentification(ActionIdentificationType value) {
                this.actionIdentification = value;
            }

            /**
             * Gets the value of the referenceInformation property.
             * 
             * @return
             *     possible object is
             *     {@link ReferenceInfoType57269S }
             *     
             */
            public ReferenceInfoType57269S getReferenceInformation() {
                return referenceInformation;
            }

            /**
             * Sets the value of the referenceInformation property.
             * 
             * @param value
             *     allowed object is
             *     {@link ReferenceInfoType57269S }
             *     
             */
            public void setReferenceInformation(ReferenceInfoType57269S value) {
                this.referenceInformation = value;
            }

        }

    }

}
