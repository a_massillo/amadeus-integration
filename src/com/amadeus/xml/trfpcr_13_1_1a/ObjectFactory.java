
package com.amadeus.xml.trfpcr_13_1_1a;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.amadeus.xml.trfpcr_13_1_1a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.amadeus.xml.trfpcr_13_1_1a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocRefundProcessRefundReply }
     * 
     */
    public DocRefundProcessRefundReply createDocRefundProcessRefundReply() {
        return new DocRefundProcessRefundReply();
    }

    /**
     * Create an instance of {@link InvoiceFopGroupType }
     * 
     */
    public InvoiceFopGroupType createInvoiceFopGroupType() {
        return new InvoiceFopGroupType();
    }

    /**
     * Create an instance of {@link DocRefundProcessRefundReply.ApplicationErrorGroup }
     * 
     */
    public DocRefundProcessRefundReply.ApplicationErrorGroup createDocRefundProcessRefundReplyApplicationErrorGroup() {
        return new DocRefundProcessRefundReply.ApplicationErrorGroup();
    }

    /**
     * Create an instance of {@link ResponseAnalysisDetailsTypeI }
     * 
     */
    public ResponseAnalysisDetailsTypeI createResponseAnalysisDetailsTypeI() {
        return new ResponseAnalysisDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferenceInformationTypeI }
     * 
     */
    public ReferenceInformationTypeI createReferenceInformationTypeI() {
        return new ReferenceInformationTypeI();
    }

    /**
     * Create an instance of {@link TicketNumberType161311S }
     * 
     */
    public TicketNumberType161311S createTicketNumberType161311S() {
        return new TicketNumberType161311S();
    }

    /**
     * Create an instance of {@link RefundedItineraryType }
     * 
     */
    public RefundedItineraryType createRefundedItineraryType() {
        return new RefundedItineraryType();
    }

    /**
     * Create an instance of {@link ItemReferencesAndVersionsType }
     * 
     */
    public ItemReferencesAndVersionsType createItemReferencesAndVersionsType() {
        return new ItemReferencesAndVersionsType();
    }

    /**
     * Create an instance of {@link ApplicationErrorDetailType }
     * 
     */
    public ApplicationErrorDetailType createApplicationErrorDetailType() {
        return new ApplicationErrorDetailType();
    }

    /**
     * Create an instance of {@link ApplicationErrorInformationType }
     * 
     */
    public ApplicationErrorInformationType createApplicationErrorInformationType() {
        return new ApplicationErrorInformationType();
    }

    /**
     * Create an instance of {@link CompanyIdentificationTypeI }
     * 
     */
    public CompanyIdentificationTypeI createCompanyIdentificationTypeI() {
        return new CompanyIdentificationTypeI();
    }

    /**
     * Create an instance of {@link FormOfPaymentDetailsTypeI }
     * 
     */
    public FormOfPaymentDetailsTypeI createFormOfPaymentDetailsTypeI() {
        return new FormOfPaymentDetailsTypeI();
    }

    /**
     * Create an instance of {@link FormOfPaymentTypeI }
     * 
     */
    public FormOfPaymentTypeI createFormOfPaymentTypeI() {
        return new FormOfPaymentTypeI();
    }

    /**
     * Create an instance of {@link FreeTextDetailsType }
     * 
     */
    public FreeTextDetailsType createFreeTextDetailsType() {
        return new FreeTextDetailsType();
    }

    /**
     * Create an instance of {@link FreeTextInformationType }
     * 
     */
    public FreeTextInformationType createFreeTextInformationType() {
        return new FreeTextInformationType();
    }

    /**
     * Create an instance of {@link MonetaryInformationDetailsType }
     * 
     */
    public MonetaryInformationDetailsType createMonetaryInformationDetailsType() {
        return new MonetaryInformationDetailsType();
    }

    /**
     * Create an instance of {@link MonetaryInformationType }
     * 
     */
    public MonetaryInformationType createMonetaryInformationType() {
        return new MonetaryInformationType();
    }

    /**
     * Create an instance of {@link OriginAndDestinationDetailsTypeI }
     * 
     */
    public OriginAndDestinationDetailsTypeI createOriginAndDestinationDetailsTypeI() {
        return new OriginAndDestinationDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReferencingDetailsTypeI }
     * 
     */
    public ReferencingDetailsTypeI createReferencingDetailsTypeI() {
        return new ReferencingDetailsTypeI();
    }

    /**
     * Create an instance of {@link ReservationControlInformationDetailsType }
     * 
     */
    public ReservationControlInformationDetailsType createReservationControlInformationDetailsType() {
        return new ReservationControlInformationDetailsType();
    }

    /**
     * Create an instance of {@link ReservationControlInformationType }
     * 
     */
    public ReservationControlInformationType createReservationControlInformationType() {
        return new ReservationControlInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeInformationType }
     * 
     */
    public StructuredDateTimeInformationType createStructuredDateTimeInformationType() {
        return new StructuredDateTimeInformationType();
    }

    /**
     * Create an instance of {@link StructuredDateTimeType }
     * 
     */
    public StructuredDateTimeType createStructuredDateTimeType() {
        return new StructuredDateTimeType();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsType }
     * 
     */
    public TicketNumberDetailsType createTicketNumberDetailsType() {
        return new TicketNumberDetailsType();
    }

    /**
     * Create an instance of {@link TicketNumberDetailsType228956C }
     * 
     */
    public TicketNumberDetailsType228956C createTicketNumberDetailsType228956C() {
        return new TicketNumberDetailsType228956C();
    }

    /**
     * Create an instance of {@link TicketNumberType }
     * 
     */
    public TicketNumberType createTicketNumberType() {
        return new TicketNumberType();
    }

    /**
     * Create an instance of {@link TransportIdentifierTypeI }
     * 
     */
    public TransportIdentifierTypeI createTransportIdentifierTypeI() {
        return new TransportIdentifierTypeI();
    }

    /**
     * Create an instance of {@link TravellerDetailsType }
     * 
     */
    public TravellerDetailsType createTravellerDetailsType() {
        return new TravellerDetailsType();
    }

    /**
     * Create an instance of {@link TravellerInformationType }
     * 
     */
    public TravellerInformationType createTravellerInformationType() {
        return new TravellerInformationType();
    }

    /**
     * Create an instance of {@link TravellerSurnameInformationType }
     * 
     */
    public TravellerSurnameInformationType createTravellerSurnameInformationType() {
        return new TravellerSurnameInformationType();
    }

    /**
     * Create an instance of {@link InvoiceFopGroupType.ParentTicketGroup }
     * 
     */
    public InvoiceFopGroupType.ParentTicketGroup createInvoiceFopGroupTypeParentTicketGroup() {
        return new InvoiceFopGroupType.ParentTicketGroup();
    }

}
