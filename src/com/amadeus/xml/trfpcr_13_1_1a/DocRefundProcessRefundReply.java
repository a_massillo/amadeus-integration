
package com.amadeus.xml.trfpcr_13_1_1a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="applicationErrorGroup" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="applicationErrorValue" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}ApplicationErrorInformationType"/&gt;
 *                   &lt;element name="errorText" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}FreeTextInformationType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="responseDetails" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}ResponseAnalysisDetailsTypeI" minOccurs="0"/&gt;
 *         &lt;element name="sacNumber" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}ReferenceInformationTypeI" minOccurs="0"/&gt;
 *         &lt;element name="ticketNumber" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}TicketNumberType_161311S" maxOccurs="99" minOccurs="0"/&gt;
 *         &lt;element name="refundedItinerary" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}RefundedItineraryType" maxOccurs="16" minOccurs="0"/&gt;
 *         &lt;element name="paymentdatagroup" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}InvoiceFopGroupType" minOccurs="0"/&gt;
 *         &lt;element name="paymentId" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}ItemReferencesAndVersionsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "applicationErrorGroup",
    "responseDetails",
    "sacNumber",
    "ticketNumber",
    "refundedItinerary",
    "paymentdatagroup",
    "paymentId"
})
@XmlRootElement(name = "DocRefund_ProcessRefundReply")
public class DocRefundProcessRefundReply {

    protected DocRefundProcessRefundReply.ApplicationErrorGroup applicationErrorGroup;
    protected ResponseAnalysisDetailsTypeI responseDetails;
    protected ReferenceInformationTypeI sacNumber;
    protected List<TicketNumberType161311S> ticketNumber;
    protected List<RefundedItineraryType> refundedItinerary;
    protected InvoiceFopGroupType paymentdatagroup;
    protected ItemReferencesAndVersionsType paymentId;

    /**
     * Gets the value of the applicationErrorGroup property.
     * 
     * @return
     *     possible object is
     *     {@link DocRefundProcessRefundReply.ApplicationErrorGroup }
     *     
     */
    public DocRefundProcessRefundReply.ApplicationErrorGroup getApplicationErrorGroup() {
        return applicationErrorGroup;
    }

    /**
     * Sets the value of the applicationErrorGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRefundProcessRefundReply.ApplicationErrorGroup }
     *     
     */
    public void setApplicationErrorGroup(DocRefundProcessRefundReply.ApplicationErrorGroup value) {
        this.applicationErrorGroup = value;
    }

    /**
     * Gets the value of the responseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAnalysisDetailsTypeI }
     *     
     */
    public ResponseAnalysisDetailsTypeI getResponseDetails() {
        return responseDetails;
    }

    /**
     * Sets the value of the responseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAnalysisDetailsTypeI }
     *     
     */
    public void setResponseDetails(ResponseAnalysisDetailsTypeI value) {
        this.responseDetails = value;
    }

    /**
     * Gets the value of the sacNumber property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceInformationTypeI }
     *     
     */
    public ReferenceInformationTypeI getSacNumber() {
        return sacNumber;
    }

    /**
     * Sets the value of the sacNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceInformationTypeI }
     *     
     */
    public void setSacNumber(ReferenceInformationTypeI value) {
        this.sacNumber = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ticketNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTicketNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TicketNumberType161311S }
     * 
     * 
     */
    public List<TicketNumberType161311S> getTicketNumber() {
        if (ticketNumber == null) {
            ticketNumber = new ArrayList<TicketNumberType161311S>();
        }
        return this.ticketNumber;
    }

    /**
     * Gets the value of the refundedItinerary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refundedItinerary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefundedItinerary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefundedItineraryType }
     * 
     * 
     */
    public List<RefundedItineraryType> getRefundedItinerary() {
        if (refundedItinerary == null) {
            refundedItinerary = new ArrayList<RefundedItineraryType>();
        }
        return this.refundedItinerary;
    }

    /**
     * Gets the value of the paymentdatagroup property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceFopGroupType }
     *     
     */
    public InvoiceFopGroupType getPaymentdatagroup() {
        return paymentdatagroup;
    }

    /**
     * Sets the value of the paymentdatagroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceFopGroupType }
     *     
     */
    public void setPaymentdatagroup(InvoiceFopGroupType value) {
        this.paymentdatagroup = value;
    }

    /**
     * Gets the value of the paymentId property.
     * 
     * @return
     *     possible object is
     *     {@link ItemReferencesAndVersionsType }
     *     
     */
    public ItemReferencesAndVersionsType getPaymentId() {
        return paymentId;
    }

    /**
     * Sets the value of the paymentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemReferencesAndVersionsType }
     *     
     */
    public void setPaymentId(ItemReferencesAndVersionsType value) {
        this.paymentId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="applicationErrorValue" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}ApplicationErrorInformationType"/&gt;
     *         &lt;element name="errorText" type="{http://xml.amadeus.com/TRFPCR_13_1_1A}FreeTextInformationType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "applicationErrorValue",
        "errorText"
    })
    public static class ApplicationErrorGroup {

        @XmlElement(required = true)
        protected ApplicationErrorInformationType applicationErrorValue;
        protected FreeTextInformationType errorText;

        /**
         * Gets the value of the applicationErrorValue property.
         * 
         * @return
         *     possible object is
         *     {@link ApplicationErrorInformationType }
         *     
         */
        public ApplicationErrorInformationType getApplicationErrorValue() {
            return applicationErrorValue;
        }

        /**
         * Sets the value of the applicationErrorValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link ApplicationErrorInformationType }
         *     
         */
        public void setApplicationErrorValue(ApplicationErrorInformationType value) {
            this.applicationErrorValue = value;
        }

        /**
         * Gets the value of the errorText property.
         * 
         * @return
         *     possible object is
         *     {@link FreeTextInformationType }
         *     
         */
        public FreeTextInformationType getErrorText() {
            return errorText;
        }

        /**
         * Sets the value of the errorText property.
         * 
         * @param value
         *     allowed object is
         *     {@link FreeTextInformationType }
         *     
         */
        public void setErrorText(FreeTextInformationType value) {
            this.errorText = value;
        }

    }

}
