package com.amadeus.constants;

import java.math.BigInteger;

public class AmadeusProperties {

	//SET PATH TO YOUR WSDL FILE
	public static final String pathToAmadeusWSDLFile = "file:/Users/pveron/Documents/Upwork/amadeus-integration/resources/wsdl/1ASIWWFIWAF_PDT_20180312_090416.wsdl";

	
	//USER ID
	public static final String userIdentifier_originIdentification_sourceOffice = "LONU128WH";
	
	public static final String userIdentifier_originator = "WSWAFWFI";
	
	public static final String userIdentifier_originatorTypeCode = "U";
	
	
	//DUTTY
	public static final String dutyCode_dutyCodeDetails_referenceQualifier = "DUT";
	
	public static final String dutyCode_dutyCodeDetails_referenceIdentifier = "SU";
	
	//ORGANIZATION
	
	public static final String systemDetails_organizationDetails_organizationId = "NMC-UK";
	
	//PASSWORD
	
	public static final BigInteger passwordInfo_dataLength = new BigInteger("7");
	
	public static final String passwordInfo_dataType = "E";
	
	public static final String passwordInfo_binaryData = "QU1BREVVUw==";
} 
