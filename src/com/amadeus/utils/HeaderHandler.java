package com.amadeus.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.NodeList;

public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {

	private boolean isStateful = false;
	
	//TODO:REMOVE NEXT VARIBALES. THIS IS NOT THE RIGHT PLACE  and not needed. Just temporary use.
	public String sessionId;
	public String sequenceNumber;
	public String securityToken;
	
	public void setStateful(boolean isStateful) {
		this.isStateful = isStateful;
	}
	
	public boolean isStateful() {
		return isStateful;
	}
	
	public boolean handleMessage(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		
		if (!outboundProperty.booleanValue()) {
			try {
				SOAPHeader header = smc.getMessage().getSOAPPart().getEnvelope().getHeader();
				
				NodeList sessionIdNode      = header.getElementsByTagNameNS("*", "SessionId");
				NodeList sequenceNumberNode = header.getElementsByTagNameNS("*", "SequenceNumber");
				NodeList securityTokenNode  = header.getElementsByTagNameNS("*", "SecurityToken");
				
			
				
				sessionId	  = sessionIdNode.item(0).getChildNodes().item(0).getNodeValue();
				sequenceNumber = sequenceNumberNode.item(0).getChildNodes().item(0).getNodeValue();
				securityToken  = securityTokenNode.item(0).getChildNodes().item(0).getNodeValue();
				
		
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return outboundProperty;
	}

	public Set<QName> getHeaders() {
		return null;
	}

	public void close(MessageContext arg0) {
		System.out.println();
	}

	public boolean handleFault(SOAPMessageContext arg0) {
		return false;
	}
}