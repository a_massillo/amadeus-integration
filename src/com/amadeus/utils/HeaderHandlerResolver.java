package com.amadeus.utils;

import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

public class HeaderHandlerResolver implements HandlerResolver {

	//TODO: Remove next line. Not requred.
	private HeaderHandler headHandler;
	
	@SuppressWarnings("unchecked")
	public List<Handler> getHandlerChain(PortInfo portInfo) {
		List<Handler> handlerChain = new ArrayList<Handler>();
		headHandler = new HeaderHandler();
		handlerChain.add(headHandler);
		return handlerChain;
	}
	
	//TODO: REMOVE NEXT METHOD NOT REQUIRED
	public HeaderHandler getHeadHandler() {
		return headHandler;
	} 
	
}