package com.amadeus.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class SOAPHeader4Util {

	public static String encodeBase64(String textPlane) {
		// Encode this value into Base6
		return java.util.Base64.getEncoder().encodeToString(textPlane.getBytes());
	}

	public static String sha1Raw(String input) {
		try {
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");

			msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
			byte[] bytes = msdDigest.digest();
			System.out.println("aca ->" + java.util.Base64.getEncoder().encodeToString(bytes));
			return byteToHex(bytes);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			// Logger.getLogger(Encriptacion.class.getName()).log(Level.SEVERE,
			// null, e);
		}
		return null;
	}

	public static byte[] sha1RawBytes(String input) {
		try {
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
			msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
			byte[] bytes = msdDigest.digest();
			return bytes;
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			// Logger.getLogger(Encriptacion.class.getName()).log(Level.SEVERE,
			// null, e);
		}
		return null;
	}

	private static byte[] sha1RawBytesFromBytes(byte[] input) {
		try {
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
			msdDigest.update(input, 0, input.length);
			byte[] bytes = msdDigest.digest();
			return bytes;
		} catch (NoSuchAlgorithmException e) {
			// Logger.getLogger(Encriptacion.class.getName()).log(Level.SEVERE,
			// null, e);
		}
		return null;
	}

	private static String sha1Raw(String input, boolean isRaw) {
		try {
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");

			msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
			byte[] bytes = msdDigest.digest();
			return new String(bytes);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			// Logger.getLogger(Encriptacion.class.getName()).log(Level.SEVERE,
			// null, e);
		}
		return null;
	}

	private static String byteToHex(final byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}

	public static String getDigestPassword(String nonce, String timestamp, String plainTextPassword) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(nonce.getBytes());
			outputStream.write(timestamp.getBytes());
			outputStream.write(SOAPHeader4Util.sha1RawBytes(plainTextPassword));

			byte[] allTogether = outputStream.toByteArray();

			byte[] allToghetherSHA1 = SOAPHeader4Util.sha1RawBytesFromBytes(allTogether);
			return java.util.Base64.getEncoder().encodeToString(allToghetherSHA1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String getDigestPasswordNonceBase64(byte[] nonce, String timestamp, String plainTextPassword) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(nonce);
			outputStream.write(timestamp.getBytes());
			outputStream.write(SOAPHeader4Util.sha1RawBytes(plainTextPassword));

			byte[] allTogether = outputStream.toByteArray();

			byte[] allToghetherSHA1 = SOAPHeader4Util.sha1RawBytesFromBytes(allTogether);
			return java.util.Base64.getEncoder().encodeToString(allToghetherSHA1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
