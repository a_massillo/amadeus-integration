package com.amadeus.utils.handlers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.message.WSSecHeader;
import org.apache.ws.security.message.WSSecUsernameToken;
import org.w3c.dom.Document;

import com.amadeus.constants.AmadeusProperties;
import com.amadeus.utils.Converter;
import com.amadeus.utils.SOAPHeader4Util;


/**
 * SOAP Handler used to manage security header and remove optional SOAP headers
 */
public class CustomSOAPHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Logger logger = Logger.getLogger(CustomSOAPHandler.class);
	private boolean mIsSecurityEnabled = false;

	@Override
    public Set<QName> getHeaders() {
		return null;
    }

	@Override
	public void close(MessageContext arg0) {
	}

	@Override
	public boolean handleFault(SOAPMessageContext arg0) {
		return true;
	}
	
	public void IsSecurityEnabled(boolean enabled){
		mIsSecurityEnabled = enabled;		
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		if (Boolean.FALSE.equals(context
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY))) {
			System.out.println();
		}
		
		if (Boolean.TRUE.equals(context
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY))) {
			try {
				
				//--
				//-- ADD SECURITY HEADER
				//--
				if (mIsSecurityEnabled)
				{
					//BuildSecurityHeader(context, "WS1APOC", "2rOEbri&WoUt");	
					//BuildSecurityHeader(context, AmadeusProperties.userIdentifier_originator, AmadeusProperties.passwordInfo_binaryData);
					BuildSecurityHeader(context, AmadeusProperties.userIdentifier_originator, "AMADEUS");

					//BuildSecurityHeader(context, AmadeusProperties.userIdentifier_originator, "bb7fd36196e6eca9c9049560f7749b2edd5e58c1");
					
				}
				
				//-- REMOVE OPTIONAL SOAP HEADERS
				// See Issue http://java.net/jira/browse/JAX_WS-1079 for further details
				//--
				SOAPMessage soapMsg = context.getMessage();
				SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
				SOAPHeader soapHeader = soapEnv.getHeader();
				Iterator<?> iterator = soapHeader.examineAllHeaderElements();
				QName sessionQName = new QName(
						"http://xml.amadeus.com/2010/06/Session_v3", "Session");
				QName securityQName = new QName(
						"http://xml.amadeus.com/2010/06/Security_v1", "AMA_SecurityHostedUser");
				QName linkQName = new QName(
						"http://wsdl.amadeus.com/2010/06/ws/Link_v1", "TransactionFlowLink");
				
				QName replyTo = new QName(
						"http://www.w3.org/2005/08/addressing", "ReplyTo");
				QName faultTo = new QName(
						"http://www.w3.org/2005/08/addressing", "FaultTo");
				for (; iterator.hasNext();) {
					SOAPHeaderElement header = (SOAPHeaderElement) iterator.next();					
					if ( !header.hasChildNodes() && (
							header.getElementQName().equals(securityQName)
							|| header.getElementQName().equals(linkQName)
							|| (header.getElementQName().equals(sessionQName)
									&& !header.hasAttribute("TransactionStatusCode"))
						)) {
						logger.debug("Remove " + header.getElementQName().getLocalPart());
						soapHeader.removeChild(header);
					}
					if(header.getElementQName().equals(replyTo) || header.getElementQName().equals(faultTo)) {
						soapHeader.removeChild(header);
					}
				}
				
			} catch (Exception e) {
				System.err.println(e);
				return false;
			}
		} else {
			System.out.println("");
		}
		SOAPMessage msg = context.getMessage();
		Converter.pringSOAPMessage(msg);
		try {
			String pepe = Converter.getStringFromDocument(msg.getSOAPPart().getEnvelope().getOwnerDocument());
			
	//		System.out.println(pepe);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
	
	/**
	 * Build the WS-Security SOAP Header
	 * @param context the message context
	 * @throws Exception
	 */
	static private void BuildSecurityHeader(SOAPMessageContext context, String username, String password)
			throws Exception
	{
		// Auto-generate the WS-Security Header using WSS4J
		String passwordEncoded = SOAPHeader4Util.encodeBase64(password);
		WSSecUsernameToken builder = new WSSecUsernameToken();
        builder.setPasswordType(WSConstants.PASSWORD_DIGEST);
        builder.setPasswordsAreEncoded(false );
        builder.addCreated();
        builder.addNonce();        
        builder.setUserInfo( username, password);  
        Document doc = context.getMessage().getSOAPPart().getEnvelope().getOwnerDocument();
        WSSecHeader secHeader = new WSSecHeader();
        secHeader.insertSecurityHeader(doc);
        builder.build(doc, secHeader);
        System.out.println();
	}

}
