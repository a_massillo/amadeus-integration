package com.waafi.amadeus.dto;

import java.math.BigInteger;

/**
 * DTO For traveller reference information
 * @author amassillo
 *
 */
public class Traveller {
	
	public Traveller(BigInteger ref) {
		this.setRef(ref);
	}
	
	public Traveller(BigInteger ref, BigInteger infantIndicator) {
		this.setRef(ref);
		this.setInfantIndicator(infantIndicator);
	}	
	
	public BigInteger getRef() {
		return ref;
	}

	public void setRef(BigInteger ref) {
		this.ref = ref;
	}

	public BigInteger getInfantIndicator() {
		return infantIndicator;
	}

	public void setInfantIndicator(BigInteger infantIndicator) {
		this.infantIndicator = infantIndicator;
	}	
	private BigInteger ref;
	private BigInteger infantIndicator = null;
	
}
