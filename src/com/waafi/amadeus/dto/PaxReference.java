package com.waafi.amadeus.dto;

import java.util.ArrayList;
import java.util.List;

import com.waafi.amadeus.enumerations.AmadeusEnumerations;
/**
 * DTO To carry passengers' information
 * @author amassillo
 *
 */
public class PaxReference {

	public PaxReference(AmadeusEnumerations.PNR_PassengerType pct) {
		this.pct = pct;
	}
	
	public AmadeusEnumerations.PNR_PassengerType getPct() {
		return pct;
	}
	public void setPct(AmadeusEnumerations.PNR_PassengerType pct) {
		this.pct = pct;
	}
	public List<Traveller> getTravellerList() {
		return travellerList;
	}
	public void setTravellerList(List<Traveller> travellerList) {
		this.travellerList = travellerList;
	}
	
	public void addTravellerToList(Traveller traveller) {
		this.travellerList.add(traveller);
	}	
	private AmadeusEnumerations.PNR_PassengerType pct;
	private List<Traveller> travellerList = new ArrayList<>();
	
}
