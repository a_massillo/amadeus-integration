package com.waafi.amadeus.enumerations;

/**
 * Contains all amadeus enumarations
 * @author amassillo
 *
 */
public class AmadeusEnumerations {

	/**
	 * PNR Passanger type enumeration
	 * @author amassillo
	 *
	 */
	public enum PNR_PassengerType{		
		ADT("Adult"),
		ACC("Accompanied person"),
		AGT("Agent"),
		ACP("Test data"),
		ASF("Air-sea fare"),
		ANN("(1)	Companion"),
		GOV("GVT blended"),
		GNN("(1)	Group child"),
		GMZ("GOV / CT / MIL / CATZ"),
		GIT("Group inclusive tour passenger"),
		GEX("Government exchange passenger"),
		GDP("Government employee dependent"),
		GCT("City/Council government passenger"),
		GCF("Government contract"),
		FFY("Frequent flyer"),
		FDT("Head of family"),
		ENN("(1)	Group Inclusive tour child"),
		EMI("Emigrant"),
		DOD("Dept of defense passenger"),
		CTZ("Category Z passenger"),
		CPN("Coupon discount passenger"),
		CNV("Convention"),
		CNN("(1)	Accompanied child"),
		CMX("ANN - CMP - CMA - Blended"),
		CMA("Adult with companion"),
		CLG("Clergy"),
		CHD("(2)	Child"),
		BUD("Airline buddy standby fare"),
		BRV("Bereavement"),
		BLD("Blind passenger"),
		BAG("Excess baggage"),
		ASB("Adult standby"),
		GVT("Government travel"),
		GVM("GOV / GOVCT / CATZ"),
		GST("State government"),
		GTF("Government travel"),
		MED("Patients travel for medical treatment"),
		MDP("Spouse and dependent children of military personnel."),
		MCR("Military charter"),
		MBT("Military - basic training graduate"),
		LBR("Laborer / worker"),
		JOB("Job corps"),
		ITX("Individual inclusive tour passenger"),
		INS("Infant occupying a seat"),
		INN("(1)	Individual inclusive tour child"),
		INF("(2)	Infant not occupying a seat"),
		IIT("Individual inclusive tour passenger"),
		FNN("(1)	Family plan child"),
		FTF("Frequent flyer"),
		HNN("Children charter"),
		HCR("Adult charter"),
		HOF("Head of family"),
		GVZ("Government / category Z"),
		ICP("Incentive certificates"),
		MIS("Missionary"),
		MIR("Military reserve on active duty"),
		MIL("Military confirmed passenger"),
		MFM("Military immediate family"),
		OTS("Passenger occupying two seats"),
		NSB("Non revenue standby"),
		NAT("NATO"),
		MXS("Military DOD not based in USA"),
		MSG("Multi state government"),
		MUS("Military DOD based in USA"),
		MSB("Military standby"),
		MRE("Retired military and dependent"),
		MPA("Military parents / parents in law"),
		MNN("(1)	Military child."),
		MLZ("Military / category. Z"),
		PFC("Private fare"),
		PFB("Private fare"),
		PFA("Private fare"),
		PNN("(1)	Children charter"),
		PCR("Adult charter"),
		PFI("Private fare"),
		PFH("Private fare"),
		PFG("Private fare"),
		PFF("Private fare"),
		PFE("Private fare"),
		PFD("Private fare"),
		PFR("Private fare"),
		PFQ("Private fare"),
		PFP("Private fare"),
		PFO("Private fare"),
		PFN("Private fare"),
		PFM("Private fare"),
		PFL("Private fare"),
		PFK("Private fare"),
		PFJ("Private fare"),
		REF("Refugee"),
		REC("Military recruit"),
		PFZ("Private fare"),
		PFY("Private fare"),
		PFX("Private fare"),
		PFW("Private fare"),
		PFV("Private fare"),
		PFU("Private fare"),
		PFT("Private fare"),
		PFS("Private fare"),
		ZNN("(1)	Group visit another country, child"),
		YTH("Youth confirmed"),
		YSB("Youth standby"),
		YNN("(1)	Government travel child"),
		YMZ("Category Z passenger"),
		YCR("Youth charter"),
		YCL("Clergy"),
		YCD("Senior citizen"),
		YCB("Senior citizen standby"),
		VNN("(1)	Visit another country child"),
		VFR("Visit friends relatives"),
		VAG("Group visit another country adult"),
		VAC("Visitor another country adult"),
		UNV("University employee"),
		UN("(1)	Unaccompanied child"),
		UAM("Unaccompanied minor"),
		TUR("Tour conductor"),
		TNN("(1)	Frequent flyer child"),
		STU("Student"),
		SPS("Spouse"),
		SNN("(1)	Senior citizen"),
		SEA("Seaman"),
		SDB("Student standby"),
		STR("State resident"),
		SRC("Senior citizen");
		public String description;
		private PNR_PassengerType(String description) {
			this.description = description;
		}
	}
	public enum TYPE_OF_UNIT{
		PX
	}
	
	/**
	 * Fare types enumeration
	 * @author amassillo
	 *
	 */
	public enum FARE_TYPE {
		FARETYPE_ATPCO_NEGO_FARES_CAT35("RA"),
	    FARETYPE_DDF_BASED_ON_PUBLIC_FARES("RD"),
	    FARETYPE_DDF_BASED_ON_PRIVATE_FARES("RDV"),
	    FARETYPE_AMADEUS_NEGO_FARES("RN"),
	    FARETYPE_PUBLISHED_FARES("RP"),
	    FARETYPE_UNIFARES("RU"),
	    FARETYPE_ATPCO_PRIVATE_FARES_CAT15("RV"),
	    FARETYPE_TAC("TAC");;
		
	    private String value;
	    private FARE_TYPE(String value){
	    	this.value = value;
	    }
	    public String getValue() {
	    	return value;
	    }
	}

}
