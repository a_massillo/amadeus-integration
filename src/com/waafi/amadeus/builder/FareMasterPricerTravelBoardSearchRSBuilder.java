package com.waafi.amadeus.builder;

import javax.xml.ws.Holder;

import com.amadeus.wsdl._2010._06.ws.link_v1.TransactionFlowLinkType;
import com.amadeus.xml._2010._06.session_v3.Session;
import com.amadeus.xml.fmptbr_14_3_1a.CompanyIdentificationTextType;
import com.amadeus.xml.fmptbr_14_3_1a.ConversionRateTypeI;
import com.amadeus.xml.fmptbr_14_3_1a.FareFamilyType;
import com.amadeus.xml.fmptbr_14_3_1a.FareInformationType;
import com.amadeus.xml.fmptbr_14_3_1a.FareMasterPricerTravelBoardSearchReply;
import com.amadeus.xml.fmptbr_14_3_1a.StatusType;
import com.amadeus.xml.fmptbr_14_3_1a.ValueSearchCriteriaType;

public class FareMasterPricerTravelBoardSearchRSBuilder {

    // FROM HERE JUST RESPONSES
    public Holder<StatusType> replyStatus = new Holder<StatusType>();
    public Holder<FareMasterPricerTravelBoardSearchReply.ErrorMessage> errorMessage = new Holder<FareMasterPricerTravelBoardSearchReply.ErrorMessage>();
    public Holder<ConversionRateTypeI> conversionRate = new Holder<ConversionRateTypeI>();
    public Holder<java.util.List<FareInformationType>> solutionFamily = new Holder<java.util.List<FareInformationType>>();
    public Holder<java.util.List<FareFamilyType>> familyInformation = new Holder<java.util.List<FareFamilyType>>();
    public Holder<FareMasterPricerTravelBoardSearchReply.AmountInfoForAllPax> amountInfoForAllPax = new Holder<FareMasterPricerTravelBoardSearchReply.AmountInfoForAllPax>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.AmountInfoPerPax>> amountInfoPerPax = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.AmountInfoPerPax>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.FeeDetails>> feeDetails = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.FeeDetails>>();
    public Holder<java.util.List<CompanyIdentificationTextType>> companyIdText = new Holder<java.util.List<CompanyIdentificationTextType>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.OfficeIdDetails>> officeIdDetails = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.OfficeIdDetails>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.FlightIndex>> flightIndex = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.FlightIndex>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.Recommendation>> recommendation = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.Recommendation>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.OtherSolutions>> otherSolutions = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.OtherSolutions>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.WarningInfo>> warningInfo = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.WarningInfo>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.GlobalInformation>> globalInformation = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.GlobalInformation>>();
    public Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp>> serviceFeesGrp = new Holder<java.util.List<FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp>>();
    public Holder<java.util.List<ValueSearchCriteriaType>> value = new Holder<java.util.List<ValueSearchCriteriaType>>();
    public Holder<FareMasterPricerTravelBoardSearchReply.MnrGrp> mnrGrp = new Holder<FareMasterPricerTravelBoardSearchReply.MnrGrp>();
    public Holder<Session> session = new Holder<Session>();
    //_fareMasterPricerTravelBoardSearch_session1.s
    //_fareMasterPricerTravelBoardSearch_session1.value.setSessionId("030415");
    //_fareMasterPricerTravelBoardSearch_session1.value.setSequenceNumber("1");
    //_fareMasterPricerTravelBoardSearch_session1.value.setSecurityToken("LALALA");
    public Holder<TransactionFlowLinkType> link = new Holder<TransactionFlowLinkType>();
    
    

}
