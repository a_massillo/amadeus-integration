package com.waafi.amadeus.builder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.amadeus.xml.fmptbq_14_3_1a.ArrivalLocalizationType;
import com.amadeus.xml.fmptbq_14_3_1a.ArrivalLocationDetailsType;
import com.amadeus.xml.fmptbq_14_3_1a.ArrivalLocationDetailsType120834C;
import com.amadeus.xml.fmptbq_14_3_1a.DateAndTimeDetailsType254619C;
import com.amadeus.xml.fmptbq_14_3_1a.DateAndTimeDetailsTypeI;
import com.amadeus.xml.fmptbq_14_3_1a.DateAndTimeInformationType181295S;
import com.amadeus.xml.fmptbq_14_3_1a.DepartureLocationType;
import com.amadeus.xml.fmptbq_14_3_1a.FareMasterPricerTravelBoardSearch.FareOptions;
import com.amadeus.xml.fmptbq_14_3_1a.FareMasterPricerTravelBoardSearch.Itinerary;
import com.amadeus.xml.fmptbq_14_3_1a.NumberOfUnitsType;
import com.amadeus.xml.fmptbq_14_3_1a.NumberOfUnitDetailsType260583C;
import com.amadeus.xml.fmptbq_14_3_1a.OriginAndDestinationRequestType;
import com.amadeus.xml.fmptbq_14_3_1a.PricingTicketingDetailsType;
import com.amadeus.xml.fmptbq_14_3_1a.PricingTicketingInformationType;
import com.amadeus.xml.fmptbq_14_3_1a.TravellerDetailsType;
import com.amadeus.xml.fmptbq_14_3_1a.TravellerReferenceInformationType;
import com.amadeus.xml.smpreq_14_2_1a.AirRetrieveSeatMap.Traveler;
import com.waafi.amadeus.dto.PaxReference;
import com.waafi.amadeus.dto.Traveller;
import com.amadeus.xml._2010._06.session_v3.Session;

/**
 * This class attempts to simplify the initialization of a search by using FareMasterPriceTravelBoardSearch soap web service.
 * @author amassillo
 *
 */
public class FareMasterPricerTravelBoardSearchRQBuilder {

	NumberOfUnitsType numberOfUnit = new NumberOfUnitsType();
	List<TravellerReferenceInformationType> paxReferenceList =  new ArrayList<>();
	FareOptions fareOptions = new FareOptions();
	java.util.List<Itinerary> itinerary	= new ArrayList<Itinerary>();
	
	/**
	 * Returns the number of unit
	 * @return
	 */
	public NumberOfUnitsType getNumberOfUnit() {
		return numberOfUnit;
	}

	/**
	 * Returns the list of pax
	 * @return
	 */
	public List<TravellerReferenceInformationType> getPaxReferenceList() {
		return paxReferenceList;
	}

	/**
	 * returns the fare options list
	 * @return
	 */
	public FareOptions getFareOptions() {
		return fareOptions;
	}

	/**
	 * returns the itinerary list;
	 * @return
	 */
	public List<Itinerary> getItinerary() {
		return itinerary;
	}

	/**
	 * returns the session.
	 * @return
	 */
	public Session getSession() {
		return session;
	}

	Session session;

	/**
	 * Add a new Unit
	 * @param numberOfUnits
	 * @param typeOfUnit
	 * @return
	 */
	public FareMasterPricerTravelBoardSearchRQBuilder addUnit(BigInteger numberOfUnits, String typeOfUnit) {
		NumberOfUnitDetailsType260583C unitDetails = new NumberOfUnitDetailsType260583C();
		unitDetails.setNumberOfUnits(numberOfUnits);
		unitDetails.setTypeOfUnit(typeOfUnit);
		numberOfUnit.getUnitNumberDetail().add(unitDetails);
		return this;
	}

	public FareMasterPricerTravelBoardSearchRQBuilder build() {
		return this;
	}
	
	/**
	 * Includes a new tiketing price type.
	 * @param priceType
	 * @return
	 */
	public FareMasterPricerTravelBoardSearchRQBuilder addTicketingPriceType(String priceType) {
		if (fareOptions.getPricingTickInfo() == null) {
			fareOptions.setPricingTickInfo(new PricingTicketingDetailsType());
			fareOptions.getPricingTickInfo().setPricingTicketing(new PricingTicketingInformationType());
		}
		fareOptions.getPricingTickInfo().getPricingTicketing().getPriceType().add(priceType);
		return this;
	}
	
	/**
	 * Inserts a new item into the itinerary list.
	 * @param setSegRef
	 * @param departureCode
	 * @param arrivalCode
	 * @param date
	 * @return
	 */
	public FareMasterPricerTravelBoardSearchRQBuilder addItemToItInerary(
			BigInteger setSegRef,
			String departureCode,
			String arrivalCode,
			String date) {
		Itinerary item = new Itinerary();
		
		OriginAndDestinationRequestType segmentReference = new OriginAndDestinationRequestType();
		segmentReference.setSegRef(setSegRef);
		
		ArrivalLocationDetailsType120834C arrivalDetailsType = new ArrivalLocationDetailsType120834C();
		arrivalDetailsType.setLocationId(departureCode);
		DepartureLocationType departure = new DepartureLocationType();
		departure.setDeparturePoint(arrivalDetailsType);
		
		
		ArrivalLocalizationType arrivalLocalizationType = new ArrivalLocalizationType();
		ArrivalLocationDetailsType arrivalLocationDetails = new ArrivalLocationDetailsType();
		arrivalLocationDetails.setLocationId(arrivalCode);
		arrivalLocalizationType.setArrivalPointDetails(arrivalLocationDetails);
		
		
		DateAndTimeDetailsTypeI dateAndTimeDetailsType=new DateAndTimeDetailsTypeI();
		dateAndTimeDetailsType.setDate(date);	
		DateAndTimeInformationType181295S dateAndTimeInfo =new DateAndTimeInformationType181295S();
		dateAndTimeInfo.setFirstDateTimeDetail(dateAndTimeDetailsType);
		
		//DateAndTimeDetailsType254619C dateForRangeDays = new DateAndTimeDetailsType254619C();
		//dateForRangeDays.setDayInterval(range);
		//dateForRangeDays.setRangeQualifier("701");
		//dateAndTimeInfo.setRangeOfDate(dateForRangeDays);
		
		item.setRequestedSegmentRef(segmentReference);
		item.setDepartureLocalization(departure);
		item.setArrivalLocalization(arrivalLocalizationType);
		item.setTimeDetails(dateAndTimeInfo);
		
		itinerary.add(item);
		
		return this;
	}
	
	
	/**
	 * Add a new traveler into the pax reference list
	 * @return
	 */

	public FareMasterPricerTravelBoardSearchRQBuilder addTravelersInfo(List<PaxReference> paxReferences) {

		for(PaxReference paxReference : paxReferences) {
			TravellerReferenceInformationType travelerInformation = new TravellerReferenceInformationType();
			
			for (Traveller traveller : paxReference.getTravellerList()) {
				TravellerDetailsType travelerDetailsType = new TravellerDetailsType();
				travelerDetailsType.setRef(traveller.getRef());
				travelerInformation.getTraveller().add(travelerDetailsType);			
			}
			travelerInformation.getPtc().add(paxReference.getPct().name());
			paxReferenceList.add(travelerInformation);			
		}
		return this;
	}

}
