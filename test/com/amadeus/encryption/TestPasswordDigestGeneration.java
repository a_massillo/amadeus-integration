package com.amadeus.encryption;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Base64;

import org.junit.Test;

import com.amadeus.utils.SOAPHeader4Util;

public class TestPasswordDigestGeneration {

	@Test
	public void validateGenerationTest() throws IOException {
		String plainTextPassword = "AMADEUS";
		String nonce = "secretnonce10111";
		String timestamp = "2015-09-30T14:12:15Z";

		assertEquals("+LzcaRc+ndGAcZIXmq/N7xGes+k=",
				SOAPHeader4Util.getDigestPassword(nonce, timestamp, plainTextPassword));

	}
	
	@Test
	public void validateGenerationNonceBinaryTest() throws IOException {
		String plainTextPassword = "AMADEUS";
		String nonce = "secretnonce10111";
		String timestamp = "2015-09-30T14:12:15Z";
		assertEquals("+LzcaRc+ndGAcZIXmq/N7xGes+k=",
				SOAPHeader4Util.getDigestPasswordNonceBase64(nonce.getBytes(), timestamp, plainTextPassword));

	}
	
	public static void main(String[] args) {
		String plainTextPassword = "AMADEUS";
		String base64Binary = "Caoapez4mYsmnt8dzeAv9w==";
		byte[] nonceDecoded = Base64.getDecoder().decode(base64Binary);
		String timestamp = "2018-04-03T19:34:01.637Z";
		
		String passwordDigest = SOAPHeader4Util.getDigestPasswordNonceBase64(nonceDecoded, timestamp, plainTextPassword);
		System.out.println(passwordDigest);
	}	
		

}
